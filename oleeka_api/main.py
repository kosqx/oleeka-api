from fastapi import FastAPI, Request, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from jose.exceptions import ExpiredSignatureError  # type: ignore

from oleeka_api import __version__
from oleeka_api.api.middleware import ServerTimingMiddleware
from oleeka_api.logic.base import OleekaException, OleekaObjectMissingException
from oleeka_api.logic.plugin import get_plugin_manager

from oleeka_api.endpoints.aggregates import router as aggregates_router
from oleeka_api.endpoints.backup import router as backup_router
from oleeka_api.endpoints.files import router as files_router
from oleeka_api.endpoints.note import router as note_router
from oleeka_api.endpoints.notes import router as notes_router
from oleeka_api.endpoints.users import router as users_router
from oleeka_api.endpoints.other import router as other_router


app = FastAPI(
    title="Oleeka API",
    description="Oleeka is note taking application with nice features",
    version=__version__,
    openapi_url="/api/openapi.json",
    openapi_tags=[
        {"name": "notes", "description": "Notes' CRUD"},
        {"name": "note", "description": "Note's CRUD"},
        {"name": "files", "description": "Files' CRUD"},
        {"name": "aggregates", "description": "Aggregates data from multiple notes"},
        {"name": "users", "description": "Users' operations, including the **login**"},
        {"name": "other", "description": "Other endpoints"},
    ],
)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(ServerTimingMiddleware)
app.include_router(aggregates_router)
app.include_router(backup_router)
app.include_router(files_router)
app.include_router(note_router)
app.include_router(notes_router)
app.include_router(users_router)
app.include_router(other_router)

for plugin_router in get_plugin_manager().get_routers():
    app.include_router(plugin_router)


@app.exception_handler(OleekaObjectMissingException)
async def oleeka_object_missing_exception_handler(
    request: Request, exc: OleekaObjectMissingException
):
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={"detail": {"message": exc.message, "id": exc.info}},
    )


@app.exception_handler(OleekaException)
async def oleeka_exception_handler(request: Request, exc: OleekaException):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": exc.get_detail()},
    )


@app.exception_handler(ExpiredSignatureError)
async def expired_jwt_handler(request: Request, exc: ExpiredSignatureError):
    return JSONResponse(
        status_code=status.HTTP_401_UNAUTHORIZED,
        content={"detail": str(exc)},
    )
