from typing import cast

import pyparsing as pp  # type: ignore

from oleeka_api.query.base import QueryParseException
from oleeka_api.query.terms import Term, All, Nid, Text, Tag, Attr, Not, And, Or


def handle_attrs(tokens):
    if tokens[0] == "NID":
        if tokens[1] == "=":
            return Nid(tokens[2].split(","))
        else:
            raise QueryParseException("query cannot be parsed")
    else:
        return Attr(*tokens)


not_operator = pp.Keyword("NOT") | pp.Literal("!")
and_operator = pp.Keyword("AND") | pp.oneOf("& &&")
or_operator = pp.Keyword("OR") | pp.oneOf("| ||")
bool_operator = pp.Keyword("NOT") | pp.Keyword("ALL") | pp.Keyword("OR")
identifier = ~bool_operator + pp.Word(pp.pyparsing_unicode.alphanums + ":-_.")
string = pp.QuotedString('"', escChar="\\") | pp.QuotedString("'", escChar="\\")
asterisk = pp.Suppress("*")
attr_name = pp.Combine(pp.Optional(pp.Suppress("$")) + identifier)
attr_operator = pp.oneOf(" ".join(Attr.OPERATORS.keys()))

expr = pp.Forward()

term = (
    pp.Keyword("ALL").setParseAction(lambda tokens: All())
    | pp.Keyword("NONE").setParseAction(lambda tokens: Term())
    | pp.Group(string).setParseAction(lambda tokens: Text(tokens[0][0]))
    | (attr_name + pp.Literal("?")).setParseAction(
        lambda tokens: Attr.exists(tokens[0])
    )
    | (attr_name + attr_operator + (identifier | string)).setParseAction(handle_attrs)
    | pp.Combine(pp.Suppress("$") + identifier).setParseAction(
        lambda tokens: Attr.exists(tokens[0])
    )
    | pp.Combine(asterisk + identifier + asterisk).setParseAction(
        lambda tokens: Tag(tokens[0], mode="infix")
    )
    | pp.Combine(asterisk + identifier).setParseAction(
        lambda tokens: Tag(tokens[0], mode="suffix")
    )
    | pp.Combine(identifier + asterisk).setParseAction(
        lambda tokens: Tag(tokens[0], mode="prefix")
    )
    | pp.Group(identifier).setParseAction(lambda tokens: Tag(tokens[0][0]))
    | (pp.Suppress("(") + expr + pp.Suppress(")"))
)

expr_not = ((not_operator + term) | term).setParseAction(
    lambda tokens: Not(tokens[1]) if tokens[0] in ("NOT", "!") else tokens[0]
)
expr_and = (
    expr_not + pp.ZeroOrMore(pp.Optional(pp.Suppress(and_operator)) + expr_not)
).setParseAction(lambda tokens: And(list(tokens)) if len(tokens) > 1 else tokens[0])
expr_or = (
    expr_and + pp.ZeroOrMore(pp.Suppress(or_operator) + expr_and)
).setParseAction(lambda tokens: Or(list(tokens)) if len(tokens) > 1 else tokens[0])

expr << expr_or


def parse_string(query: str) -> Term:
    if query.strip() == "":
        return All()
    try:
        term = expr.parseString(query, parseAll=True)[0]
        return cast(Term, term)
    except pp.ParseException as exc:
        raise QueryParseException("query cannot be parsed", exc.loc)
