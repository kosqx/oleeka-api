from oleeka_api.logic.base import OleekaException


class QueryParseException(OleekaException):
    def get_detail(self):
        return [
            {
                "loc": ["query", "query"],
                "msg": self.message,
                "type": "parse_error.query",
                "char_index": self.info,
            },
        ]
