import re
import operator as std_operator
from dataclasses import dataclass, astuple
from datetime import datetime
from typing import Any, Dict, Callable, ClassVar, List, Literal, Optional, Tuple, Type
from typing import TYPE_CHECKING


from oleeka_api.logic.base import OleekaException
from oleeka_api.logic.date import oleeka_date_parse
from oleeka_api.logic.note import validate_name, validate_nid


if TYPE_CHECKING:
    from oleeka_api.models import Note


class StructParseException(OleekaException):
    def get_detail(self):
        return [
            {
                "loc": ["query", "query"],
                "msg": self.message,
                "type": "parse_error.query",
                "value": self.info,
            },
        ]


def _contains_word(haystack: str, needle: str) -> bool:
    return re.search(r"\b" + re.escape(needle) + r"\b", haystack) is not None


def _is_less_or_equal(a: str, b: str) -> bool:
    try:
        float_a = float(a)
        float_b = float(b)
    except ValueError:
        return False

    return float_a <= float_b


def _validate_arguments_count(params: List[Any], term: str, expected: int):
    if (len(params) - 1) != expected:
        raise StructParseException(
            "{} takes {} positional arguments but {} were given".format(
                term, expected, len(params) - 1
            )
        )


def _format_exception_message(message: str, klass: Type["Term"]) -> str:
    pattern_old = "__init__()"
    pattern_new = f"{klass.__name__}.__init__()"
    name = klass.__name__.lower()
    return message.replace(pattern_new, name).replace(pattern_old, name)


TermVisitor = Callable[["Term"], Optional["Term"]]


@dataclass(frozen=True)
class Term:
    def match(self, note: "Note") -> bool:
        return False

    def format(self, brackets: bool = False) -> str:
        return "NONE"

    @classmethod
    def get_term_name(cls) -> str:
        if cls.__name__ == "Term":
            return "none"
        return cls.__name__.lower()

    def to_struct(self) -> List[Any]:
        return [self.get_term_name(), *astuple(self)]

    def simplify(self) -> "Term":
        return self

    def visit(self, visitor: TermVisitor) -> Optional["Term"]:
        return visitor(self)

    @classmethod
    def from_struct(cls, struct: List[Any]) -> "Term":
        if not isinstance(struct, list):
            raise StructParseException("struct query must be a list")
        if len(struct) == 0:
            raise StructParseException("struct query must be an nonempty list")

        if struct[0] == "none":
            _validate_arguments_count(struct, "none", 0)
            return Term()
        if struct[0] == "not":
            _validate_arguments_count(struct, "not", 1)
            return Not(cls.from_struct(struct[1]))
        if struct[0] == "and":
            _validate_arguments_count(struct, "and", 1)
            return And([cls.from_struct(i) for i in struct[1]])
        if struct[0] == "or":
            _validate_arguments_count(struct, "or", 1)
            return Or([cls.from_struct(i) for i in struct[1]])
        if struct[0] == "nid":
            _validate_arguments_count(struct, "nid", 1)
            return Nid(struct[1])

        for parameter in struct:
            if not isinstance(parameter, str):
                raise StructParseException(
                    "struct query parameters {!r} is not a string".format(parameter),
                    parameter,
                )

        klasses = {klass.__name__.lower(): klass for klass in cls.__subclasses__()}
        if struct[0] not in klasses:
            raise StructParseException(
                "struct query {!r} is not a valid term".format(struct[0]), struct[0]
            )
        klass = klasses[struct[0]]
        try:
            return klass(*struct[1:])
        except TypeError as exc:
            raise StructParseException(_format_exception_message(str(exc), klass))


@dataclass(frozen=True)
class All(Term):
    def match(self, note: "Note") -> bool:
        return True

    def format(self, brackets: bool = False) -> str:
        return "ALL"


@dataclass(frozen=True)
class Nid(Term):
    nids: List[str]

    def __post_init__(self):
        for nid in self.nids:
            if not validate_nid(nid):
                raise StructParseException("{!r} is not a valid nid".format(nid), nid)

    def match(self, note: "Note") -> bool:
        return note.nid in self.nids

    def format(self, brackets: bool = False) -> str:
        return 'NID="{}"'.format(",".join(self.nids))

    def simplify(self) -> Term:
        if not self.nids:
            return Term()
        return self


@dataclass(frozen=True)
class Tag(Term):
    tag: str
    mode: Literal["exact", "prefix", "suffix", "infix"] = "exact"
    MODES: ClassVar[Dict[str, Tuple[str, str, Callable[[str, str], bool]]]] = {
        "exact": ("", "", str.__eq__),
        "prefix": ("", "*", str.startswith),
        "suffix": ("*", "", str.endswith),
        "infix": ("*", "*", str.__contains__),
    }

    def __post_init__(self):
        if self.mode not in self.MODES:
            raise StructParseException(
                "tag mode {!r} is not a valid one".format(self.mode),
                self.mode,
            )

    def match(self, note: "Note") -> bool:
        _, _, cmp = self.MODES[self.mode]
        return any(cmp(tag, self.tag) for tag in note.tags)

    def format(self, brackets: bool = False) -> str:
        before, after, cmp = self.MODES[self.mode]
        return before + self.tag + after


@dataclass(frozen=True)
class Attr(Term):
    name: str
    # fmt: off
    operator: Literal[
        # check if attr exists
        "?",
        # for convenience most useful operator (insensitive contains) is the shortest
        "=",
        # different substring operators
        "==", "===", "^=", "^==", "$=", "$==", "*=", "*==", "+=", "+==",
        # ordering
        "<=", "<==", ">=", ">==",
        # numeric comparisons
        "<<=", ">>="
    ]
    # fmt: on
    value: str
    # kind: str = "text"

    OPERATORS: ClassVar[Dict[str, Tuple[Callable[[str, str], bool], bool]]] = {
        "?": (str.__ne__, False),
        "=": (str.__contains__, False),
        "==": (str.__eq__, False),
        "===": (str.__eq__, True),
        "^=": (str.startswith, False),
        "^==": (str.startswith, True),
        "$=": (str.endswith, False),
        "$==": (str.endswith, True),
        "*=": (str.__contains__, False),
        "*==": (str.__contains__, True),
        "+=": (_contains_word, False),
        "+==": (_contains_word, True),
        "<=": (str.__le__, False),
        "<==": (str.__le__, True),
        ">=": (str.__ge__, False),
        ">==": (str.__ge__, True),
        "<<=": (lambda note_val, attr_val: _is_less_or_equal(note_val, attr_val), True),
        ">>=": (lambda note_val, attr_val: _is_less_or_equal(attr_val, note_val), True),
    }

    @classmethod
    def exists(cls, name: str) -> "Attr":
        return cls(name, "?", "")

    @classmethod
    def any_of(cls, name: str, values: List[str], operator="===") -> "Or":
        return Or([cls(name, operator, value) for value in values])

    @classmethod
    def is_operator_case_sensitive(cls, operator: str) -> bool:
        _, case_sensitive = cls.OPERATORS[operator]
        return case_sensitive

    def __post_init__(self):
        if self.operator not in self.OPERATORS:
            raise StructParseException(
                "attr operator {!r} is not a valid one".format(self.operator),
                self.operator,
            )

    def match(self, note: "Note") -> bool:
        attr_value = note.attrs.get(self.name)
        if attr_value is None:
            return False
        return self.match_value(attr_value)

    def match_value(self, attr_value: str) -> bool:
        operator_fn, case_sensitive = self.OPERATORS[self.operator]

        if case_sensitive:
            value = self.value
        else:
            value = self.value.lower()
            attr_value = attr_value.lower()

        return operator_fn(attr_value, value)

    def _format_value(self):
        if validate_name(self.value):
            return self.value
        else:
            return '"' + self.value.replace('"', '\\"') + '"'

    def format(self, brackets: bool = False) -> str:
        return "{}{}{}".format(self.name, self.operator, self._format_value())


MarkFeature = Literal[
    "tags_count",
    "attrs_count",
    "files_count",
    # TODO: consider not returning 0 for notes without files
    "files_size",  # total size of files, in bytes
    "modified_after",  # how many seconds after creation note has been modified
    "date_day_of_week",  # weekday, 1..7, Monday = 1, ..., Sunday = 7 (ISO 8601)
    "date_day_of_year",  # day of the year, 1...366
    "date_week_of_year",  # week number, 1..53 (ISO 8601)
]
MarkOperator = Literal["==", "!=", "<", "<=", ">", ">="]
MarkValue = int


def _get_date(note: "Note", key: str = "created") -> datetime:
    return oleeka_date_parse(note.attrs[key])


@dataclass(frozen=True)
class Mark(Term):
    feature: MarkFeature
    operator: MarkOperator
    value: MarkValue

    FEATURES: ClassVar[Dict[MarkFeature, Callable[["Note"], MarkValue]]] = {
        "tags_count": lambda note: len(note.tags),
        "attrs_count": lambda note: len(note.attrs),
        "files_count": lambda note: len(note.files),
        "files_size": lambda note: sum(file.size for file in note.files),
        "modified_after": lambda note: round(
            (_get_date(note, "modified") - _get_date(note, "created")).total_seconds()
        ),
        "date_day_of_week": lambda note: _get_date(note).isoweekday(),
        "date_day_of_year": lambda note: _get_date(note).timetuple().tm_yday,
        "date_week_of_year": lambda note: _get_date(note).isocalendar().week,
    }
    OPERATORS: ClassVar[Dict[MarkOperator, Callable[[MarkValue, MarkValue], bool]]] = {
        "==": std_operator.eq,
        "!=": std_operator.ne,
        "<": std_operator.lt,
        "<=": std_operator.le,
        ">": std_operator.gt,
        ">=": std_operator.ge,
    }

    @classmethod
    def any_of(cls, feature: MarkFeature, values: List[MarkValue]) -> "Or":
        return Or([cls(feature, "==", value) for value in values])

    def match(self, note: "Note") -> bool:
        feature_fn = self.FEATURES[self.feature]
        operator_fn = self.OPERATORS[self.operator]
        return operator_fn(feature_fn(note), self.value)

    def format(self, brackets: bool = False) -> str:
        raise NotImplementedError


@dataclass(frozen=True)
class Text(Term):
    value: str

    def match(self, note: "Note") -> bool:
        return self.value in note.text

    def format(self, brackets: bool = False) -> str:
        return '"{}"'.format(self.value.replace('"', '\\"'))


@dataclass(frozen=True)
class Not(Term):
    condition: Term

    def match(self, note: "Note") -> bool:
        return not self.condition.match(note)

    def format(self, brackets: bool = False) -> str:
        return "!{}".format(self.condition.format(brackets=True))

    def to_struct(self) -> List[Any]:
        return ["not", self.condition.to_struct()]

    def simplify(self) -> Term:
        if isinstance(self.condition, self.__class__):
            return self.condition.condition.simplify()
        return Not(self.condition.simplify())

    def visit(self, visitor: TermVisitor) -> Optional["Term"]:
        condition = self.condition.visit(visitor)
        if condition is None:
            return None
        else:
            return Not(condition)


@dataclass(frozen=True)
class And(Term):
    conditions: List[Term]

    def match(self, note: "Note") -> bool:
        return all(condition.match(note) for condition in self.conditions)

    def format(self, brackets: bool = False) -> str:
        return (
            ("(" * brackets)
            + " AND ".join(
                condition.format(brackets=True) for condition in self.conditions
            )
            + (")" * brackets)
        )

    def to_struct(self) -> List[Any]:
        return ["and", [condition.to_struct() for condition in self.conditions]]

    def simplify(self) -> Term:
        if len(self.conditions) == 0:
            return All()
        if len(self.conditions) == 1:
            return self.conditions[0].simplify()
        return And([cond.simplify() for cond in self.conditions])

    def visit(self, visitor: TermVisitor) -> Optional["Term"]:
        conditions = [cond.visit(visitor) for cond in self.conditions]
        return And([cond for cond in conditions if cond is not None])


@dataclass(frozen=True)
class Or(Term):
    conditions: List[Term]

    def match(self, note: "Note") -> bool:
        return any(condition.match(note) for condition in self.conditions)

    def format(self, brackets: bool = False) -> str:
        return (
            ("(" * brackets)
            + " OR ".join(
                condition.format(brackets=True) for condition in self.conditions
            )
            + (")" * brackets)
        )

    def to_struct(self) -> List[Any]:
        return ["or", [condition.to_struct() for condition in self.conditions]]

    def simplify(self) -> Term:
        if len(self.conditions) == 0:
            return Term()
        if len(self.conditions) == 1:
            return self.conditions[0].simplify()
        return Or([cond.simplify() for cond in self.conditions])

    def visit(self, visitor: TermVisitor) -> Optional["Term"]:
        conditions = [cond.visit(visitor) for cond in self.conditions]
        return Or([cond for cond in conditions if cond is not None])


@dataclass(frozen=True)
class Nested(Term):
    """
    Nested() allows integrating code generated struct queries with user entered ones.
    It only transports data from the parser and cannot be used for querying
    Example:
        ["and", [["attr", "kind", "===", "photo"], ["nested", "user input"]]]
    """

    query: str

    def match(self, note: "Note") -> bool:
        raise NotImplementedError

    def format(self, brackets: bool = False) -> str:
        raise NotImplementedError

    def to_struct(self) -> List[Any]:
        raise NotImplementedError

    def simplify(self) -> Term:
        raise NotImplementedError


ALL_TERM_CLASSES = [Term, All, Nid, Tag, Attr, Mark, Text, Not, And, Or, Nested]
