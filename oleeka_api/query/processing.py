import json
from typing import Optional, TYPE_CHECKING

from oleeka_api.query.terms import All, Term, Tag, Nested, StructParseException
from oleeka_api.query.parser import parse_string


if TYPE_CHECKING:
    from oleeka_api.models import PaginatedQuery


def parse_query(query: Optional[str] = None) -> Term:
    if query is None:
        return All()
    elif query.startswith("["):
        try:
            query_struct = json.loads(query)
        except json.JSONDecodeError:
            raise StructParseException("input is not valid JSON")
        return Term.from_struct(query_struct)
    else:
        return parse_string(query)


class RandomTagFinder:
    def __init__(self):
        self.has_random = False

    def visitor(self, term: Term) -> Optional["Term"]:
        if term == Tag(":RANDOM"):
            self.has_random = True
            return None
        return term


def nested_visitor(term: Term) -> Optional["Term"]:
    if isinstance(term, Nested):
        return parse_query(term.query)
    return term


def postprocess_query(paginated_query: "PaginatedQuery") -> "PaginatedQuery":
    random_tag_finder = RandomTagFinder()

    query_terms_unnested = paginated_query.query_terms.visit(nested_visitor) or Term()
    query_terms = query_terms_unnested.visit(random_tag_finder.visitor)

    return paginated_query.replace(
        query_terms=(All() if query_terms is None else query_terms),
        random=paginated_query.random or random_tag_finder.has_random,
    )
