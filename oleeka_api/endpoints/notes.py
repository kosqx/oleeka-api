from typing import Optional

from fastapi import APIRouter, Depends

from oleeka_api.api.models import NoteModel, NotesResponse, NoteRequest, NoteResponse
from oleeka_api.models import PaginatedQuery, SelectFields, UserAccount, RelatedRequest
from oleeka_api.service import params, prepare_query, save_notes


router = APIRouter()


async def paginated_query_parameters(
    username: Optional[str] = params.username(),
    query: Optional[str] = params.query(),
    select_fields: str = params.select_fields(),
    skip: int = params.offset(),
    limit: int = params.limit(),
    random: bool = False,
):
    return PaginatedQuery(
        username=username,
        query=query,
        skip=skip,
        limit=limit,
        random=random,
        select_fields=SelectFields.parse(select_fields),
    )


@router.get(
    "/api/v2/notes",
    tags=["notes"],
    response_model=NotesResponse,
    response_model_exclude_unset=True,
)
async def api_get_notes(
    db: params.Storage = params.storage(),
    query: PaginatedQuery = Depends(paginated_query_parameters),
    current_user: str = params.current_user(),
    fetch_total: bool = False,
):
    query = prepare_query(query)
    notes = [NoteModel.from_note(note) for note in await db.get_notes(query)]
    if fetch_total:
        if len(notes) < query.limit:
            result = NotesResponse(notes=notes, total=len(notes) + query.skip)
        else:
            related = await db.get_related(RelatedRequest(query, fetch_total=True))
            result = NotesResponse(notes=notes, total=related.total)
    else:
        result = NotesResponse(notes=notes)

    if query.random:
        result.random = True
    return result


@router.post("/api/v2/notes", tags=["notes"], response_model=NoteResponse)
async def api_post_notes(
    note_wrapper: NoteRequest,
    db: params.Storage = params.storage(),
    current_user_account: UserAccount = params.current_user_account(),
):
    notes = await save_notes(db, current_user_account, note_wrapper.all_notes())
    return NoteResponse(note=NoteModel.from_note(notes[0]))
