from enum import Enum
from typing import Optional, get_args

from fastapi import APIRouter, Depends


from oleeka_api.api.models import RelatedStatsResponse, SuggestionsResponse
from oleeka_api.models import PaginatedQuery, FetchDatesOptions, RelatedRequest
from oleeka_api.service import get_suggestions, params, prepare_query


router = APIRouter()


# error: Enum() expects a string, tuple, list or dict literal as the second argument
# see https://github.com/python/typeshed/issues/2305
FetchDatesEnum = Enum(  # type: ignore
    "FetchDatesEnum",
    {value: value for value in get_args(FetchDatesOptions) if value is not None},
)


async def fetch_dates_parameter(
    fetch_dates: Optional[FetchDatesEnum] = None,
) -> FetchDatesOptions:
    return None if fetch_dates is None else fetch_dates.value


async def query_parameters(
    username: Optional[str] = params.username(),
    query: Optional[str] = params.query(),
):
    return PaginatedQuery(username=username, query=query)


@router.get(
    "/api/v2/related",
    tags=["aggregates"],
    response_model=RelatedStatsResponse,
)
async def api_get_related(
    db: params.Storage = params.storage(),
    query: PaginatedQuery = Depends(query_parameters),
    current_user: str = params.current_user(),
    fetch_total: bool = True,
    fetch_tags: bool = True,
    fetch_attrs: bool = False,
    fetch_sizes: bool = False,
    fetch_dates: FetchDatesOptions = Depends(fetch_dates_parameter),
):
    """
    Helps find notes **related** to the given query.

    If `fetch_tags` or `fetch_attrs` or `fetch_dates` are set to true
    then requests behaves as if the `fetch_total` was also set.
    """
    request = RelatedRequest(
        prepare_query(query, allow_random=False),
        fetch_total=fetch_total,
        fetch_tags=fetch_tags,
        fetch_attrs=fetch_attrs,
        fetch_sizes=fetch_sizes,
        fetch_dates=fetch_dates,
    )
    stats = await db.get_related(request)
    return RelatedStatsResponse.from_related_stats(stats)


@router.get(
    "/api/v2/suggestions",
    tags=["aggregates"],
    response_model=SuggestionsResponse,
    response_model_exclude_unset=True,
)
async def api_get_suggestions(
    db: params.Storage = params.storage(),
    query: PaginatedQuery = Depends(query_parameters),
    current_user: str = params.current_user(),
):
    """
    Returns list of suggestions for query or tags input.
    """
    prepared_query = prepare_query(query, allow_random=False)
    return await get_suggestions(db, current_user, prepared_query)
