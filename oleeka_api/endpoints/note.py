from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Response
from fastapi import status

from oleeka_api.api.models import NoteModel, NoteRequest, NoteResponse
from oleeka_api.models import PaginatedQuery, SelectFields, UserAccount
from oleeka_api.query.terms import Nid
from oleeka_api.service import params, save_notes


router = APIRouter()


async def note_query_parameters(
    nid: str = params.nid(),
    username: Optional[str] = params.username(),
    select_fields: str = params.select_fields(),
):
    return PaginatedQuery(
        username=username,
        query_terms=Nid(nids=[nid]),
        limit=1,
        select_fields=SelectFields.parse(select_fields),
    )


@router.get(
    "/api/v2/notes/{nid}",
    tags=["note"],
    response_model=NoteResponse,
    response_model_exclude_unset=True,
)
async def api_get_note(
    db: params.Storage = params.storage(),
    query: PaginatedQuery = Depends(note_query_parameters),
    current_user: str = params.current_user(),
):
    notes = await db.get_notes(query)
    if len(notes) == 1:
        return NoteResponse(note=NoteModel.from_note(notes[0]))
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Note not found",
        )


@router.put(
    "/api/v2/notes/{nid}",
    tags=["note"],
    response_model=NoteResponse,
)
async def api_put_note(
    note_wrapper: NoteRequest,
    nid: str = params.nid(),
    db: params.Storage = params.storage(),
    current_user_account: UserAccount = params.current_user_account(),
):
    # TODO: better error handling
    assert note_wrapper.note.nid in (None, nid)

    notes = await save_notes(db, current_user_account, [note_wrapper.note])
    return NoteResponse(note=NoteModel.from_note(notes[0]))


@router.delete(
    "/api/v2/notes/{nid}",
    tags=["note"],
)
async def api_delete_note(
    nid: str = params.nid(),
    db: params.Storage = params.storage(),
    current_user: str = params.current_user(),
):
    await db.delete_notes(current_user, nids=[nid])
    return Response(status_code=status.HTTP_204_NO_CONTENT)
