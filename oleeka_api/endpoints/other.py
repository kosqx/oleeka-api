import datetime
import base64

from fastapi import APIRouter, Response

from oleeka_api import __version__
from oleeka_api.api.models import ServiceInfoResponse


router = APIRouter()


@router.get(
    "/api/v2",
    tags=["other"],
    summary="Service info",
    response_model=ServiceInfoResponse,
)
async def service_info():
    """
    Returns service info.

    Doesn't require database connection nor auth, so can be used as a **ping** endpoint.
    """
    return {
        "service": "oleeka",
        "version": __version__,
        "isotime": datetime.datetime.utcnow().isoformat(timespec="microseconds") + "Z",
    }


# `favicon.ico` is probably only media file in Oleeka API, so is easier to base64 it
# Content (according to `file favicon.ico`):
# MS Windows icon resource - 2 icons:
#   16x16, 16 colors, 4 bits/pixel
#   32x32, 16 colors, 4 bits/pixel
FAVICON_DATA = """
    AAABAAIAEBAQAAEABAAoAQAAJgAAACAgEAABAAQA6AIAAE4BAAAoAAAAEAAAACAAAAABAAQA
    AAAAAIAAAAAAAAAAAAAAABAAAAAQAAAAAZptABeieQAhpn8AMq2JAEu2lgBevqEAccatAIrP
    ugCn28sAsN/QANLs5AD2+/kAwubbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAnmFAAFohiAZvKtxGbqrgAQwLLRrgRMwFERItot0REA7qsu3i6qroTuS
    KLRrcSqhB7nLgRy5y2ABaZYQAXyFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAACgAAAAgAAAAQAAAAAEABAAAAAAAAAIAAAAAAAAAAAAAEAAAABAAAAAAmm0A
    GqN7ACKmfwAwq4cATLaWAF69oQBzxa0Ajs+7ALvi1gCi2McA+/39ANPs5AAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABNFUxAAAAAAA0VUIAAAAAWKqqq3EAAAA5uq
    qqlAAAGaqqqqqoEAAEuqqqqqpQABiquJmqqpAAO6qrl4qqcAADlyAAS6qkAHqqkwAEdxAAAA
    AAAAWqqQO6qzAAAAAAAAAAAAACuqsUqqkAAAAAAAAnd3d3d6qrFaqrd3d3d2AAWqqqqqqqqi
    WqqqqqqqqxAFqquIiIqqsUqqqIiIuqoQBKqkABA6qoA6qoAAAHqrEAG6pwAAeqpgCaqkAAK6
    pwAAeqqVSaqoEASqq3RYqqQAABiqqqqqswAAaqqqqqpwAAABeqqqqTAAAASKqqq2AAAAAANW
    dTAAAAAABGZkEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==
"""


@router.get("/favicon.ico", tags=["other"], include_in_schema=False)
async def favicon_ico():
    data = base64.b64decode(FAVICON_DATA)
    return Response(content=data, media_type="image/vnd.microsoft.icon")
