from typing import Dict

from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordRequestForm

from oleeka_api.api.auth import (
    check_auth,
    create_access_token,
    check_refresh_token,
    create_refresh_token,
    store_refresh_token,
)
from oleeka_api.api.models import AuthRequest, AuthResponse
from oleeka_api.service import params


REFRESH_TOKEN_KEY_PREFIX = "refresh_token."


router = APIRouter()


@router.post("/api/v2/auth/token", tags=["users"], summary="For SwaggerUI use only")
@router.post("/api/v2/token", tags=["users"], summary="Get Auth Token", deprecated=True)
async def api_auth_token(
    db: params.Storage = params.storage(),
    form_data: OAuth2PasswordRequestForm = Depends(),
):
    user = await db.get_user(form_data.username)
    if user is not None and check_auth(user.auth, form_data.password):
        access_token = create_access_token(data={"sub": form_data.username})
        return {"access_token": access_token, "token_type": "bearer"}
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect username or password",
        )


@router.post(
    "/api/v2/auth/login",
    response_model=AuthResponse,
    tags=["users"],
    summary="Login to the service",
)
async def api_auth_login(
    auth_request: AuthRequest,
    db: params.Storage = params.storage(),
):
    """
    Returns:
     * access token in the response body
       * to be used in `Authorization: Bearer $access_token`
     * refresh token in the HttpOnly cookie
       * cookie names include username, so multiple users can be logged in in the same time
       * to be used in `POST /api/v2/auth/refresh/{username}` endpoint
    """
    user = await db.get_user(auth_request.username)
    if user is not None and check_auth(user.auth, auth_request.password):
        access_token = create_access_token(data={"sub": auth_request.username})
        refresh_token = create_refresh_token()
        await db.save_user(
            user.replace(auth=store_refresh_token(user.auth, refresh_token))
        )
        content = {"access_token": access_token}
        response = JSONResponse(content=content)
        response.set_cookie(
            key=f"{REFRESH_TOKEN_KEY_PREFIX}{auth_request.username}",
            value=refresh_token,
            httponly=True,
            secure=True,
        )
        return response
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect username or password",
        )


def get_refresh_tokens(request: Request) -> Dict[str, str]:
    return {
        name[len(REFRESH_TOKEN_KEY_PREFIX) :]: value
        for name, value in request.cookies.items()
        if name.startswith(REFRESH_TOKEN_KEY_PREFIX)
    }


@router.post(
    "/api/v2/auth/refresh/{username}",
    response_model=AuthResponse,
    tags=["users"],
    summary="Get new access token from refresh token",
)
async def api_auth_refresh(
    username: str = params.username_path(),
    db: params.Storage = params.storage(),
    refresh_tokens: Dict[str, str] = Depends(get_refresh_tokens),
):
    # TODO: validate_username(), validate_token()
    token = refresh_tokens.get(username)
    user = await db.get_user(username)

    if username and user is not None and check_refresh_token(user.auth, token):
        access_token = create_access_token(data={"sub": username})
        return {"access_token": access_token}
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect refresh token",
        )
