import tarfile
import io
from enum import Enum
from typing import AsyncIterator, IO, List, cast

from fastapi import APIRouter
from fastapi.responses import StreamingResponse

from oleeka_api.api.models import NoteModel
from oleeka_api.logic.base import OleekaException
from oleeka_api.logic.date import (
    parse_timedelta,
    oleeka_date_parse,
    datetime_to_timestamp,
)
from oleeka_api.logic.note import generate_datetime_str
from oleeka_api.models import Note, PaginatedQuery, SelectFields
from oleeka_api.query import parse_query
from oleeka_api.query.terms import And, Attr, Nid, Term
from oleeka_api.service import params
from oleeka_api.storage import Storage
from oleeka_api.utils.iterutils import chunks


router = APIRouter()


class BackupFileFormat(str, Enum):
    ndjson = "ndjson"
    tar_gz = "tar.gz"


class BackupDownloader:
    extension = ""
    mime = ""
    CHUNK_LENGTH = 30

    def get_filename(self, age_param: str) -> str:
        if age_param != "all":
            suffix = "_" + "".join(i for i in age_param if i.isalnum())
        else:
            suffix = ""

        backup_date = generate_datetime_str()
        return f"backup_{backup_date}{suffix}{self.extension}"

    async def get_notes(
        self, db: Storage, notes_nids: List[str]
    ) -> AsyncIterator[Note]:
        for nids in chunks(notes_nids, self.CHUNK_LENGTH):
            query = PaginatedQuery(
                query_terms=Nid(nids),
                limit=self.CHUNK_LENGTH,
                select_fields=SelectFields.everything(),
            )
            notes = await db.get_notes(query)
            for note in notes:
                yield note

    async def build(self, db: Storage, notes_nids: List[str]):
        raise NotImplementedError


class NdjsonBackupDownloader(BackupDownloader):
    """
    Exporting backups in `ndjson` (Newline Delimited JSON) format
    See:
      http://ndjson.org/
      https://jsonlines.org/
      https://en.wikipedia.org/wiki/JSON_streaming
    """

    extension = ".ndjson"
    mime = "application/x-ndjson"

    async def build(self, db: Storage, notes_nids: List[str]):
        async for note in self.get_notes(db, notes_nids):
            model = NoteModel.from_note(note)
            yield model.json(ensure_ascii=False).encode("utf-8") + b"\n"


class PartialFile:
    def __init__(self):
        self._buffer: List[bytes] = []

    def write(self, data: bytes):
        if data:
            self._buffer.append(data)

    def get_data(self) -> bytes:
        result, self._buffer = self._buffer, []
        return b"".join(result)

    def has_data(self) -> bool:
        return any(self._buffer)

    def size(self) -> int:
        return sum(map(len, self._buffer))


class TargzBackupDownloader(BackupDownloader):
    extension = ".tar.gz"
    mime = "application/gzip"

    def _build_tar_info(self, note: Note, data: bytes) -> tarfile.TarInfo:
        name = note.attrs["created"][:7] + "/" + note.nid
        mtime = int(datetime_to_timestamp(oleeka_date_parse(note.attrs["modified"])))

        info = tarfile.TarInfo(name=name)
        info.size = len(data)
        info.mtime = mtime
        info.mode = 0o640

        return info

    async def build(self, db: Storage, notes_nids: List[str]):
        partial_file = PartialFile()
        tar = tarfile.open(
            fileobj=cast(IO[bytes], partial_file),
            mode="w:gz",
            compresslevel=5,
        )

        async for note in self.get_notes(db, notes_nids):
            model = NoteModel.from_note(note)
            data = model.json(indent=4, ensure_ascii=False).encode("utf-8")
            stream = io.BytesIO(data)

            tar.addfile(tarinfo=self._build_tar_info(note, data), fileobj=stream)

            if partial_file.size() >= 4 * 1024:
                yield partial_file.get_data()

        tar.close()

        if partial_file.has_data():
            yield partial_file.get_data()


def build_backup_downloader(file_format: BackupFileFormat) -> BackupDownloader:
    if file_format == BackupFileFormat.ndjson:
        return NdjsonBackupDownloader()
    elif file_format == BackupFileFormat.tar_gz:
        return TargzBackupDownloader()
    else:
        raise OleekaException(f"Backup format {file_format} is not supported")


def build_query_terms(query: str, age: str = "all") -> Term:
    query_terms = parse_query(query)

    if age == "all":
        return query_terms

    age_seconds = parse_timedelta(age)
    modified_term = Attr("modified", ">==", generate_datetime_str(-age_seconds))
    return And([modified_term, query_terms])


@router.get("/api/v2/backup/download", tags=["other"], summary="Download a backup file")
async def api_backup_download(
    db: params.Storage = params.storage(),
    current_user: str = params.current_user(),
    file_format: BackupFileFormat = BackupFileFormat.tar_gz,
    query: str = params.query(),
    age: str = "all",
):
    """
    Generate and instantaneously start downloading single file containing
    all notes matching criteria
    """

    downloader = build_backup_downloader(file_format)
    filename = downloader.get_filename(age)

    paginated_query = PaginatedQuery(
        username=current_user,
        query=query,
        query_terms=build_query_terms(query, age),
        limit=1000_000,
        select_fields=SelectFields.only_nid(),
    )

    notes_nids = [note.nid for note in await db.get_notes(paginated_query)]

    return StreamingResponse(
        downloader.build(db, notes_nids),
        media_type=downloader.mime,
        headers={
            "Content-Disposition": f"attachment;filename={filename}",
            "X-Oleeka-Notes-Count": str(len(notes_nids)),
        },
    )
