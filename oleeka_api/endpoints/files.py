from fastapi import APIRouter, Response

from oleeka_api.api.models import FileModel, FileResponse
from oleeka_api.service import params
from oleeka_api.utils.http import get_download_headers


router = APIRouter()


@router.get(
    "/api/v2/notes/{nid}/files/{fid}",
    tags=["files"],
    response_model=FileResponse,
)
async def api_get_file(
    nid: str = params.nid(),
    fid: str = params.fid(),
    db: params.Storage = params.storage(),
    current_user: str = params.current_user(),
    fetch_data: bool = False,
):
    file = await db.get_file(current_user, nid, fid, fetch_data=fetch_data)
    return FileResponse(file=FileModel.from_file(file))


@router.get(
    "/api/v2/notes/{nid}/files/{fid}/data",
    tags=["files"],
)
async def api_get_file_data(
    nid: str = params.nid(),
    fid: str = params.fid(),
    db: params.Storage = params.storage(),
    current_user: str = params.current_user(),
    download: bool = False,
):
    file = await db.get_file(current_user, nid, fid)

    headers = get_download_headers(file.name) if download else None
    return Response(content=file.data, media_type=file.mime, headers=headers)
