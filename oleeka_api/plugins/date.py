from oleeka_api.logic.date import parse_timedelta
from oleeka_api.logic.note import generate_datetime_str
from oleeka_api.logic.plugin import Plugin
from oleeka_api.models import Note, UserAccount
from oleeka_api.query.terms import Attr, Tag, Mark


plugin = Plugin("date")

for year in range(1970, 2030 + 1):
    plugin.replace_query_term(
        search=Tag(f":y{year}"),
        replace=Attr("created", "^==", f"{year}-"),
    )

for month in range(1, 12 + 1):
    plugin.replace_query_term(
        search=Tag(f":m{month:02}"),
        replace=Attr("created", "*==", f"-{month:02}-"),
    )

for day in range(1, 31 + 1):
    plugin.replace_query_term(
        search=Tag(f":d{day:02}"),
        replace=Attr("created", "*==", f"-{day:02}T"),
    )


for week in range(1, 53 + 1):
    plugin.replace_query_term(
        search=Tag(f":w{week:02}"),
        replace=Mark("date_week_of_year", "==", week),
    )


for day_number, name, short_name in [
    (1, "monday", "mon"),
    (2, "tuesday", "tue"),
    (3, "wednesday", "wed"),
    (4, "thursday", "thu"),
    (5, "friday", "fri"),
    (6, "saturday", "sat"),
    (7, "sunday", "sun"),
]:
    plugin.replace_query_term(
        search=Tag(f":{name}"),
        replace=Mark("date_day_of_week", "==", day_number),
        help=f"Created on {name.title()}",
    )
    plugin.replace_query_term(
        search=Tag(f":{short_name}"),
        replace=Mark("date_day_of_week", "==", day_number),
        # intentionally without `help=` (not shown in suggestions)
    )


# meteorological seasons
for season, values in [
    ("winter", ["-12-", "-01-", "-02-"]),
    ("spring", ["-03-", "-04-", "-05-"]),
    ("summer", ["-06-", "-07-", "-08-"]),
    ("autumn", ["-09-", "-10-", "-11-"]),
]:
    plugin.replace_query_term(
        search=Tag(f":{season}"),
        replace=Attr.any_of("created", values, operator="*=="),
        help=f"Created in {season} (meteorological)",
    )


@plugin.modify_note(
    Attr.exists(":age"),
    help={":age": "Set creation time given time ago (`:age=1d2h34m`)"},
)
def set_age(note: Note, user_account: UserAccount) -> Note:
    age = note.attrs.pop(":age")
    created = generate_datetime_str(-parse_timedelta(age))
    return note.replace(attrs=dict(note.attrs, created=created))
