from typing import List

from oleeka_api.models import PaginatedQuery, UserAccount
from oleeka_api.query.terms import Attr, And
from oleeka_api.service import params

from oleeka_api.plugins.bookmark.link_rules import (
    apply_link_rules,
    get_link_rules,
)
from .models import BookmarkMatchRequest, BookmarkModel


def build_text(match_request: BookmarkMatchRequest) -> str:
    result = []
    if match_request.description:
        result.append(f"<blockquote>{match_request.description}</blockquote>")
    if match_request.selection:
        result.append(f"<blockquote>{match_request.selection}</blockquote>")
    return "\n\n".join(result)


async def match_bookmarks(
    db: params.Storage,
    current_user_account: UserAccount,
    match_request: BookmarkMatchRequest,
) -> List[BookmarkModel]:
    normalized_link = apply_link_rules(
        match_request.link,
        get_link_rules(current_user_account),
    )
    query_terms = And(
        [
            Attr("kind", "===", "bookmark"),
            Attr("href", "===", normalized_link),
        ]
    )
    notes = await db.get_notes(
        PaginatedQuery(
            query_terms=query_terms,
            limit=1,
        )
    )
    if notes:
        note = notes[0]
        result = BookmarkModel(
            kind="exact",
            nid=note.nid,
            link=note.attrs.get("href", normalized_link),
            title=note.attrs.get("title", match_request.title),
            text=note.text,
            input=" ".join(note.tags),
        )
    else:
        result = BookmarkModel(
            kind="new",
            nid=None,
            link=normalized_link,
            title=match_request.title,
            text=build_text(match_request),
            input="",
        )

    return [result]
