from oleeka_api.logic.plugin import Plugin
from oleeka_api.models import UserAccount
from oleeka_api.query.terms import Attr, Tag, Term
from oleeka_api.service import params

from .models import (
    BookmarkMatchRequest,
    BookmarkMatchResponse,
    BookmarkSaveRequest,
    BookmarkSaveResponse,
)
from .match_bookmarks import match_bookmarks
from .save_bookmark import save_bookmark


plugin = Plugin("bookmark")
plugin.replace_query_term(Tag(":bookmark"), Attr("kind", "===", "bookmark"))


@plugin.modify_query_attr("link")
def link_query_term(attr: Attr) -> Term:
    return Attr("href", attr.operator, attr.value)


@plugin.post("/match", response_model=BookmarkMatchResponse)
async def bookmark_match(
    match_request: BookmarkMatchRequest,
    db: params.Storage = params.storage(),
    current_user_account: UserAccount = params.current_user_account(),
):
    matches = await match_bookmarks(db, current_user_account, match_request)
    return BookmarkMatchResponse(matches=matches)


@plugin.post("/save", response_model=BookmarkSaveResponse)
async def bookmark_save(
    save_request: BookmarkSaveRequest,
    db: params.Storage = params.storage(),
    current_user_account: UserAccount = params.current_user_account(),
):
    saved = await save_bookmark(db, current_user_account, save_request.bookmark)
    return BookmarkSaveResponse(bookmark=saved)
