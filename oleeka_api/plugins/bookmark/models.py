from typing import List, Literal, Optional

from pydantic import BaseModel


EXAMPLE_BOOKMARK_MATCH_REQUEST = {
    "link": "https:/oleeka.com/foo#bar",
    "title": "Oleeka Notes",
    "description": "description from meta",
    "selection": "selected text",
}


class BookmarkMatchRequest(BaseModel):
    link: str
    title: Optional[str]
    description: Optional[str]
    selection: Optional[str]

    class Config:
        schema_extra = {"example": EXAMPLE_BOOKMARK_MATCH_REQUEST}


EXAMPLE_BOOKMARK_MODEL = {
    "kind": "new",
    "nid": None,
    "link": "https:/oleeka.com/foo#bar",
    "title": "Oleeka Notes",
    "text": (
        "<blockquote>description from meta</blockquote>"
        "\n\n"
        "<blockquote>selected text</blockquote>"
    ),
    "input": "",
}


class BookmarkModel(BaseModel):
    kind: Optional[Literal["new", "exact", "saved"]]
    nid: Optional[str]
    link: str
    title: str
    text: str
    input: str

    class Config:
        schema_extra = {"example": EXAMPLE_BOOKMARK_MODEL}


EXAMPLE_BOOKMARK_MATCH_RESPONSE = {"matches": [EXAMPLE_BOOKMARK_MODEL]}


class BookmarkMatchResponse(BaseModel):
    matches: List[BookmarkModel]

    class Config:
        schema_extra = {"example": EXAMPLE_BOOKMARK_MATCH_RESPONSE}


EXAMPLE_BOOKMARK_SAVE_RESPONSE = {"bookmark": EXAMPLE_BOOKMARK_MODEL}


class BookmarkSaveResponse(BaseModel):
    bookmark: BookmarkModel

    class Config:
        schema_extra = {"example": EXAMPLE_BOOKMARK_SAVE_RESPONSE}


BookmarkSaveRequest = BookmarkSaveResponse
