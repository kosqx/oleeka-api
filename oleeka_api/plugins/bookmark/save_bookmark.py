from oleeka_api.api.models import NoteInputModel
from oleeka_api.models import PaginatedQuery, UserAccount
from oleeka_api.query.terms import Nid
from oleeka_api.service import params, save_notes

from .models import BookmarkModel


async def save_bookmark(
    db: params.Storage,
    current_user_account: UserAccount,
    bookmark: BookmarkModel,
) -> BookmarkModel:
    new_attrs = {"kind": "bookmark", "href": bookmark.link, "title": bookmark.title}
    if bookmark.nid:
        query = PaginatedQuery(query_terms=Nid([bookmark.nid]), limit=1)
        notes = await db.get_notes(query)
        # TODO: error if no note found
        note = notes[0]
        # TODO: CAS for `attrs.modified`
        note_input = NoteInputModel(
            nid=note.nid,
            text=bookmark.text,
            attrs={**note.attrs, **new_attrs},
            input=bookmark.input,
        )
    else:
        note_input = NoteInputModel(
            text=bookmark.text,
            attrs=new_attrs,
            input=bookmark.input,
        )

    new_notes = await save_notes(db, current_user_account, [note_input])
    new_note = new_notes[0]
    return BookmarkModel(
        kind="saved",
        nid=new_note.nid,
        link=new_note.attrs.get("href"),
        title=new_note.attrs.get("title"),
        text=new_note.text,
        input=" ".join(new_note.tags),
    )
