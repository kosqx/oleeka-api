import fnmatch
from dataclasses import dataclass, field
from urllib.parse import urlparse, ParseResult as ParsedURL
from typing import List


from oleeka_api.models import UserAccount


@dataclass(frozen=True)
class LinkRule:
    domains: List[str]
    remove_params: List[str]


@dataclass(frozen=True)
class LinkRuleSettings:
    use_default: bool = True
    rules: List[LinkRule] = field(default_factory=list)


DEFAULT_LINK_RULES = [
    LinkRule(
        domains=["*"],
        remove_params=[
            # https://en.wikipedia.org/wiki/UTM_parameters
            "utm_*",
            # Facebook
            "fb_action_ids",
            "fb_action_types",
            "fb_ref",
            "fb_source",
            "fbclid",
            # Google Analytics
            "ga_*",
            "gclid",
            "gclsrc",
            # Matomo/Piwik
            "mtm_*",
            "pk_*",
        ],
    )
]


def get_link_rules(current_user_account: UserAccount) -> List[LinkRule]:
    link_rules_settings = current_user_account.get_setting(
        LinkRuleSettings, "link_rules"
    )
    if link_rules_settings is None:
        return DEFAULT_LINK_RULES
    if link_rules_settings.use_default:
        return DEFAULT_LINK_RULES + link_rules_settings.rules
    return link_rules_settings.rules


def domain_matches(url: ParsedURL, domain_expr: str) -> bool:
    return fnmatch.fnmatch(url.hostname or "", domain_expr)


def matches_link_rule(url: ParsedURL, link_rule: LinkRule) -> bool:
    return any(domain_matches(url, domain) for domain in link_rule.domains)


def remove_params(url: ParsedURL, params_expr: List[str]) -> ParsedURL:
    params = url.query.split("&")
    params = [
        param
        for param in params
        if not any(
            fnmatch.fnmatch(param.split("=", 1)[0], param_expr)
            for param_expr in params_expr
        )
    ]
    return url._replace(query="&".join(params))


def apply_link_rules(link: str, link_rules: List[LinkRule]) -> str:
    url = urlparse(link)
    for link_rule in link_rules:
        if matches_link_rule(url, link_rule):
            url = remove_params(url, link_rule.remove_params)
    return url.geturl()


def normalize_link(link: str) -> str:
    return apply_link_rules(link, DEFAULT_LINK_RULES)
