import datetime
from dataclasses import dataclass
from typing import List, Tuple

from oleeka_api.models import Note, PaginatedQuery
from oleeka_api.query.terms import Tag, Attr, And
from oleeka_api.service import params
from oleeka_api.logic.plugin import Plugin
from oleeka_api.logic.date import oleeka_date_format, oleeka_date_parse


plugin = Plugin("uptime")
plugin.replace_query_term(Tag(":uptime"), Attr("kind", "===", "uptime"))


@dataclass(order=True, frozen=True)
class UptimeRange:
    host: str
    start: datetime.datetime
    stop: datetime.datetime

    @classmethod
    def from_parts(cls, parts: List[str]) -> "UptimeRange":
        return cls(
            host=parts[0],
            start=oleeka_date_parse(parts[1]),
            stop=oleeka_date_parse(parts[2]),
        )

    def format(self):
        return "{:20}  {}  {}".format(
            self.host, oleeka_date_format(self.start), oleeka_date_format(self.stop)
        )


def parse_uptime(text: str) -> List[UptimeRange]:
    lines_parts = [line.split() for line in text.splitlines()]
    result = [UptimeRange.from_parts(parts) for parts in lines_parts if len(parts) == 3]
    return sorted(result)


def format_uptime(data: List[UptimeRange]) -> str:
    return "\n".join([uptime_info.format() for uptime_info in data])


def update_uptime(
    ranges: List[UptimeRange], host: str, now: datetime.datetime, system_uptime: int = 0
) -> Tuple[List[UptimeRange], UptimeRange]:
    interval = datetime.timedelta(minutes=5)
    data = list(ranges)

    for i in range(len(data) - 1, -1, -1):
        entry = data[i]
        if entry.host == host and now < (entry.stop + interval):
            data[i] = result = UptimeRange(host, entry.start, now)
            break
    else:
        result = UptimeRange(host, now - datetime.timedelta(seconds=system_uptime), now)
        data.append(result)

    return sorted(data), result


def format_timedelta(timedelta: datetime.timedelta):
    seconds = int(timedelta.total_seconds())
    return "%.2d:%.2d:%.2d" % (
        seconds / 3600,
        seconds / 60 % 60,
        seconds % 60,
    )


@plugin.post("/ping/{host}")
async def uptime_ping(
    host: str,
    db: params.Storage = params.storage(),
    current_user: str = params.current_user(),
):
    now = datetime.datetime.utcnow()
    period_start = "%.4d-%.2d-01T00:00:00" % (now.year, now.month)

    query_terms = And(
        [
            Attr("kind", "===", "uptime"),
            Attr("uptime-host", "===", host),
            Attr("created", ">==", period_start),
        ]
    )
    notes = await db.get_notes(
        PaginatedQuery(
            query_terms=query_terms,
            limit=1,
        )
    )
    if notes:
        note = notes[0]
    else:
        note = Note(
            text="foo",
            attrs={
                "kind": "uptime",
                "uptime-host": host,
            },
        )
    data = parse_uptime(note.text)
    data, last = update_uptime(data, host, now)
    note = note.replace(text=format_uptime(data))
    await db.save_note(current_user, note.normalize())

    current_uptime = format_timedelta(last.stop - last.start)

    return {"uptime": current_uptime}
