from oleeka_api.logic.plugin import Plugin
from oleeka_api.models import Note, UserAccount
from oleeka_api.query.terms import Attr, Or, Tag


plugin = Plugin("rating")

plugin.replace_query_term(Tag(":R"), Attr.exists("rating"))

plugin.replace_query_term(Tag(":R1"), Attr.exists("rating"))
plugin.replace_query_term(Tag(":R2"), Attr.any_of("rating", ["2", "3"]))
plugin.replace_query_term(Tag(":R3"), Attr("rating", "===", "3"))

plugin.replace_query_term(Tag(":r1"), Attr("rating", "===", "1"))
plugin.replace_query_term(Tag(":r2"), Attr("rating", "===", "2"))
plugin.replace_query_term(Tag(":r3"), Attr("rating", "===", "3"))


TAG_TO_RATING = {":r1": "1", ":r2": "2", ":r3": "3"}


@plugin.modify_note(Or([Tag(i) for i in TAG_TO_RATING]))
def set_rating(note: Note, user_account: UserAccount) -> Note:
    tags = list(note.tags)
    rating = ""
    for i, tag in enumerate(tags):
        if tag in TAG_TO_RATING:
            rating = TAG_TO_RATING[tag]
            tags[i] = ""
    if rating:
        return note.replace(
            tags=[i for i in tags if i],
            attrs=dict(note.attrs, rating=rating),
        )
    else:
        return note
