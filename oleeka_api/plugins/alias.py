from dataclasses import dataclass
from typing import List, Optional

from oleeka_api.logic.plugin import Plugin
from oleeka_api.models import Note, UserAccount
from oleeka_api.modifier import update_note_with_input
from oleeka_api.query import parse_query


@dataclass(frozen=True)
class NoteAlias:
    query: str
    input: str
    help: Optional[str] = None


plugin = Plugin("alias")


@plugin.modify_note()
def alias_note(note: Note, user_account: UserAccount) -> Note:
    note_aliases = user_account.get_setting(List[NoteAlias], "note_aliases")
    for note_alias in note_aliases or []:
        query_terms = parse_query(note_alias.query)
        if query_terms.match(note):
            note = update_note_with_input(note, note_alias.input)
    return note
