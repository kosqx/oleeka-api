from oleeka_api.logic.plugin import Plugin
from oleeka_api.query.terms import Or, Tag, Term


plugin = Plugin("tags")


@plugin.modify_query_tag()
def tags_query_term_hierarchical(tag: Tag) -> Term:
    if tag.mode == "exact" and tag.tag.endswith(":"):
        return Or([Tag(tag.tag[:-1]), Tag(tag.tag, mode="prefix")])
    return tag


@plugin.modify_query_tag()
def tags_query_term_hidden(tag: Tag) -> Term:
    if tag.mode in ("exact", "prefix") and tag.tag.startswith("."):
        return Or([tag, Tag(tag.tag[1:], mode=tag.mode)])
    return tag
