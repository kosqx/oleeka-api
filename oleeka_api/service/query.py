from oleeka_api.logic.base import OleekaException
from oleeka_api.logic.plugin import get_plugin_manager
from oleeka_api.models import PaginatedQuery
from oleeka_api.query import parse_query, postprocess_query


def prepare_query(
    query: PaginatedQuery,
    *,
    allow_random: bool = True,
) -> PaginatedQuery:
    query = query.replace(query_terms=parse_query(query.query))
    query = postprocess_query(query)
    if not allow_random and query.random:
        raise OleekaException(":RANDOM in query is not allowed here")
    preprocessed_query_terms = get_plugin_manager().preprocess_query(query.query_terms)
    return query.replace(query_terms=preprocessed_query_terms)
