from typing import Dict, List

from oleeka_api.api.models import SuggestionModel, SuggestionsResponse
from oleeka_api.logic.plugin import get_plugin_manager
from oleeka_api.models import Note, Query, PaginatedQuery, SelectFields, RelatedRequest
from oleeka_api.query.terms import Attr
from oleeka_api.storage import Storage


def merge_suggestions(
    related_tags: Dict[str, int],
    query_help: Dict[str, str],
    edit_help: Dict[str, str],
    taginfo_notes: List[Note],
) -> List[SuggestionModel]:
    taginfo = {note.attrs["tag"]: note.attrs["title"] for note in taginfo_notes}
    result = (
        [
            SuggestionModel.init(name=tag, count=count, help=taginfo.get(tag))
            for tag, count in related_tags.items()
        ]
        + [
            SuggestionModel(name=tag, help=message)
            for tag, message in taginfo.items()
            if tag not in related_tags
        ]
        + [
            SuggestionModel(name=tag, only="query", help=message)
            for tag, message in query_help.items()
        ]
        + [
            SuggestionModel(name=f"{tag}=", only="edit", help=message)
            for tag, message in edit_help.items()
        ]
    )
    return sorted(result, key=lambda item: item.name)


async def get_suggestions(
    db: Storage,
    current_user: str,
    query: Query,
) -> SuggestionsResponse:
    plugin_manager = get_plugin_manager()
    related = await db.get_related(RelatedRequest(query, fetch_tags=True))
    taginfo_notes = await db.get_notes(
        PaginatedQuery(
            query_terms=Attr("kind", "===", "taginfo"),
            select_fields=SelectFields.only(attrs={"tag", "title"}),
            limit=1_000,
        )
    )

    suggestions = merge_suggestions(
        related_tags=related.tags or {},
        query_help=plugin_manager.query_help,
        edit_help=plugin_manager.edit_help,
        taginfo_notes=taginfo_notes,
    )

    return SuggestionsResponse(suggestions=suggestions)
