from typing import Dict, List, Optional, Set

from oleeka_api.api.models import NoteInputModel
from oleeka_api.logic.update_note import merge_note
from oleeka_api.logic.plugin import get_plugin_manager
from oleeka_api.models import Note, PaginatedQuery, SelectFields, UserAccount
from oleeka_api.query.terms import Nid
from oleeka_api.storage import Storage


async def _get_notes_dict(
    db: Storage, current_user: str, nids: Set[str]
) -> Dict[Optional[str], Note]:
    if nids:
        query = PaginatedQuery(
            username=current_user,
            query_terms=Nid(nids=list(nids)),
            select_fields=SelectFields.everything(),
        )
        notes_db = await db.get_notes(query)
        return {note.nid: note for note in notes_db}
    else:
        return {}


async def save_notes(
    db: Storage, current_user_account: UserAccount, notes_input: List[NoteInputModel]
) -> List[Note]:
    plugin_manager = get_plugin_manager()
    nids = {note.nid for note in notes_input if note.nid and not note.restore}
    notes_db_dict = await _get_notes_dict(db, current_user_account.username, nids)

    notes = [
        plugin_manager.preprocess_note(
            merge_note(note_db=notes_db_dict.get(note.nid), note_input=note),
            user_account=current_user_account,
        )
        for note in notes_input
    ]
    await db.save_notes(current_user_account.username, notes)
    return notes
