from fastapi import Depends, Path, Query
from oleeka_api.storage import Storage
from oleeka_api.service.auth import get_current_user, get_current_user_account


from oleeka_api.logic.note import VALID_NID_PATTERN, VALID_FID_PATTERN


def nid():
    return Path(
        ...,
        description="Note ID to be fetched",
        min_length=28,
        max_length=28,
        regex=VALID_NID_PATTERN,
    )


def fid():
    return Path(
        ...,
        description="File ID to be fetched",
        min_length=2,
        max_length=29,
        regex=VALID_FID_PATTERN,
    )


def username():
    # TODO: VALID_USERNAME_PATTERN
    return Query(
        None,
        description="Only notes created by given user",
        min_length=2,
        max_length=50,
        regex=r"^[a-z0-9]{2,50}$",
    )


def username_path():
    return Path(
        "",
        min_length=2,
        max_length=50,
        regex=r"^[a-z0-9]{2,50}$",
    )


def query():
    return Query(None, description="Filter notes using Oleeka Query Language")


def select_fields():
    return Query("", description="Return only specified fields")


def offset():
    return Query(0, ge=0, le=1_000_000_000)


def limit():
    return Query(10, ge=0, le=10_000)


def storage():
    return Depends(Storage.get_db)


def current_user():
    return Depends(get_current_user)


def current_user_account():
    return Depends(get_current_user_account)
