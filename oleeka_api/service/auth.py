import binascii
from base64 import b64decode
from typing import Optional, Tuple

from fastapi import Depends, HTTPException, Request, status
from fastapi.security import HTTPBasicCredentials, OAuth2PasswordBearer
from fastapi.security.http import get_authorization_scheme_param


from oleeka_api.api.auth import validate_access_token, check_refresh_token
from oleeka_api.models import UserAccount
from oleeka_api.storage import Storage


oauth2_auth = OAuth2PasswordBearer(tokenUrl="/api/v2/auth/token", auto_error=False)


async def basic_auth(request: Request) -> Optional[HTTPBasicCredentials]:
    # based on `fastapi.security.http.HTTPBasic.__init__`
    authorization: str = request.headers.get("Authorization")
    scheme, credentials = get_authorization_scheme_param(authorization)
    if authorization and scheme.lower() == "basic":
        try:
            data = b64decode(credentials).decode("ascii")
        except (ValueError, UnicodeDecodeError, binascii.Error):
            return None
        username, separator, password = data.partition(":")
        if not separator:
            return None
        return HTTPBasicCredentials(username=username, password=password)
    return None


async def _get_current_user(
    db: Storage,
    oauth2_token: Optional[str],
    basic_credentials: Optional[HTTPBasicCredentials],
) -> Tuple[Optional[str], Optional[UserAccount]]:
    if oauth2_token is not None:
        return validate_access_token(oauth2_token).get("sub"), None
    elif basic_credentials is not None:
        user_account = await db.get_user(basic_credentials.username)
        if user_account is None:
            return None, None
        if check_refresh_token(user_account.auth, basic_credentials.password):
            return basic_credentials.username, user_account
    return None, None


async def get_current_user(
    db: Storage = Depends(Storage.get_db),
    oauth2_token: Optional[str] = Depends(oauth2_auth),
    basic_credentials: Optional[HTTPBasicCredentials] = Depends(basic_auth),
) -> Optional[str]:
    username, user_account = await _get_current_user(
        db, oauth2_token, basic_credentials
    )

    if username is not None:
        return username
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )


async def get_current_user_account(
    db: Storage = Depends(Storage.get_db),
    oauth2_token: Optional[str] = Depends(oauth2_auth),
    basic_credentials: Optional[HTTPBasicCredentials] = Depends(basic_auth),
) -> Optional[UserAccount]:
    username, user_account = await _get_current_user(
        db, oauth2_token, basic_credentials
    )

    if username is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )
    elif user_account is None:
        return await db.get_user(username)
    else:
        return user_account
