from dataclasses import dataclass, astuple, fields
from typing import Any, ClassVar, List

from oleeka_api.models import Note
from oleeka_api.modifier.base import InputParseException


@dataclass(frozen=True)
class Action:
    action_name: ClassVar[str] = "noop"

    def modify(self, note: "Note") -> "Note":
        return note

    def to_struct(self) -> List[Any]:
        return [self.action_name, *astuple(self)]

    @classmethod
    def from_struct(cls, struct: List[Any]) -> "Action":
        klasses = {klass.action_name: klass for klass in cls.__subclasses__()}
        klasses[cls.action_name] = cls

        if not (
            isinstance(struct, list)
            and len(struct) >= 1
            and struct[0] in klasses
            and len(struct) == len(fields(klasses[struct[0]])) + 1
            and all(isinstance(i, str) for i in struct)
        ):
            raise InputParseException("invalid input struct", struct)

        return klasses[struct[0]](*struct[1:])


@dataclass(frozen=True)
class AddText(Action):
    action_name: ClassVar[str] = "add_text"

    text: str

    @staticmethod
    def smart_concat(text: str, new_text: str) -> str:
        if not text:
            return new_text
        if not text.endswith(("\n", "\r")):
            return text + "\n" + new_text
        return text + new_text

    def modify(self, note: "Note") -> "Note":
        return note.replace(text=AddText.smart_concat(note.text, self.text))


@dataclass(frozen=True)
class AddTag(Action):
    action_name: ClassVar[str] = "add_tag"

    tag: str

    def modify(self, note: "Note") -> "Note":
        tags = [*note.tags, self.tag]
        unique_tags = list({tag: None for tag in tags}.keys())
        return note.replace(tags=unique_tags)


@dataclass(frozen=True)
class RemoveTag(Action):
    action_name: ClassVar[str] = "remove_tag"

    tag: str

    def modify(self, note: "Note") -> "Note":
        return note.replace(tags=[i for i in note.tags if i != self.tag])


@dataclass(frozen=True)
class ResetTags(Action):
    action_name: ClassVar[str] = "reset_tags"

    def modify(self, note: "Note") -> "Note":
        return note.replace(tags=[])


@dataclass(frozen=True)
class SetAttr(Action):
    action_name: ClassVar[str] = "set_attr"

    attr: str
    value: str

    def modify(self, note: "Note") -> "Note":
        if self.value:
            attrs = {**note.attrs, self.attr: self.value}
        else:
            attrs = {key: val for key, val in note.attrs.items() if key != self.attr}
        return note.replace(attrs=attrs)


ListOfActions = List[Action]
