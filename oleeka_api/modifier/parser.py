import json
from typing import cast

import pyparsing as pp  # type: ignore

from oleeka_api.modifier.actions import Action, ListOfActions
from oleeka_api.modifier.actions import AddText, AddTag, RemoveTag, ResetTags, SetAttr
from oleeka_api.modifier.base import InputParseException


# TODO: disallow ("tag", "AND") and other keywords identifiers
identifier = pp.Word(pp.pyparsing_unicode.alphanums + ":-_.")
string = pp.QuotedString('"', escChar="\\") | pp.QuotedString("'", escChar="\\")
attr_name = pp.Combine(pp.Optional(pp.Suppress("$")) + identifier)
reset_tags = pp.Suppress(pp.Regex(r"={1,}")).setParseAction(lambda tokens: ResetTags())

input_term = (
    pp.Group(string).setParseAction(lambda tokens: AddText(tokens[0][0]))
    | (attr_name + pp.Suppress("=") + (identifier | string)).setParseAction(
        lambda tokens: SetAttr(tokens[0], tokens[1])
    )
    | (identifier + pp.Suppress("!")).setParseAction(
        lambda tokens: RemoveTag(tokens[0])
    )
    | pp.Group(identifier).setParseAction(lambda tokens: AddTag(tokens[0][0]))
)

input_expr = pp.Optional(reset_tags) + pp.ZeroOrMore(input_term)


def parse_input_string_freeform(input_string: str) -> ListOfActions:
    try:
        input_terms = input_expr.parseString(input_string, parseAll=True).asList()
        return cast(ListOfActions, input_terms)
    except pp.ParseException as exc:
        raise InputParseException("input cannot be parsed", exc.loc)


def parse_input_string_struct(input_string: str) -> ListOfActions:
    try:
        actions = json.loads(input_string)
    except json.JSONDecodeError:
        raise InputParseException("input is not valid JSON")

    return [Action.from_struct(action) for action in actions]


def parse_input_string(input_string: str) -> ListOfActions:
    if input_string.startswith("["):
        return parse_input_string_struct(input_string)
    else:
        return parse_input_string_freeform(input_string)
