from oleeka_api.logic.base import OleekaException


class InputParseException(OleekaException):
    def get_detail(self):
        return [
            {
                "loc": ["body", "note", "input"],
                "msg": self.message,
                "type": "parse_error.input",
                "char_index": self.info,
            },
        ]
