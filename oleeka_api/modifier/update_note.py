from oleeka_api.models import Note
from oleeka_api.modifier.actions import ListOfActions
from oleeka_api.modifier.parser import parse_input_string


def apply_actions(note: Note, actions: ListOfActions) -> Note:
    for action in actions:
        note = action.modify(note)
    return note


def update_note_with_input(note: Note, input_string: str) -> Note:
    return apply_actions(note, parse_input_string(input_string))
