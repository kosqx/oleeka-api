import pkgutil
import importlib
from typing import Callable, Dict, List, Optional, Tuple, Type, cast

from fastapi import APIRouter

from oleeka_api.query.terms import All, Attr, Tag, Term
from oleeka_api.models import Note, UserAccount


PLUGIN_PATH_PREFIX = "/api/v2/plugin/"


QueryTermModifier = Callable[[Term], Term]
QueryTagModifier = Callable[[Tag], Term]
QueryAttrModifier = Callable[[Attr], Term]
ModifierStruct = Tuple[Type[Term], Optional[str], QueryTermModifier]
NoteModifier = Callable[[Note, UserAccount], Note]
NoteModifierStruct = Tuple[Term, NoteModifier]


class Plugin:
    def __init__(self, name: str):
        self.name = name
        self.router: Optional[APIRouter] = None
        self.query_term_replacements: Dict[Term, Term] = {}
        self.query_term_modifiers: List[ModifierStruct] = []
        self.note_modifiers: List[NoteModifierStruct] = []
        self.query_help: Dict[str, str] = {}
        self.edit_help: Dict[str, str] = {}

    #################################################################
    # Routing - adding custom endpoints to the API

    def _ensure_router(self) -> APIRouter:
        if self.router is None:
            self.router = APIRouter()
        return self.router

    def api_route(self, method: str, path: str, extra_args):
        return self._ensure_router().api_route(
            f"{PLUGIN_PATH_PREFIX}{self.name}{path}",
            tags=[f"plugin_{self.name}"],
            methods=[method.upper()],
            **extra_args,
        )

    def get(self, path: str, **kwargs):
        return self.api_route("GET", path, kwargs)

    def put(self, path: str, **kwargs):
        return self.api_route("PUT", path, kwargs)

    def post(self, path: str, **kwargs):
        return self.api_route("POST", path, kwargs)

    def delete(self, path: str, **kwargs):
        return self.api_route("DELETE", path, kwargs)

    def patch(self, path: str, **kwargs):
        return self.api_route("PATCH", path, kwargs)

    #################################################################
    # Query preprocessing - editing query before is sent to Storage

    def replace_query_term(
        self, search: Term, replace: Term, help: Optional[str] = None
    ):
        self.query_term_replacements[search] = replace
        if isinstance(search, Tag) and help is not None:
            self.query_help[search.tag] = help

    def modify_query_term(
        self, search: Type[Term]
    ) -> Callable[[QueryTermModifier], QueryTermModifier]:
        def wrapper(modifier: QueryTermModifier):
            self.query_term_modifiers.append((search, None, modifier))
            return modifier

        return wrapper

    def modify_query_tag(
        self, tag_value: Optional[str] = None
    ) -> Callable[[QueryTagModifier], QueryTagModifier]:
        def wrapper(modifier: QueryTagModifier):
            self.query_term_modifiers.append(
                (cast(Type[Term], Tag), tag_value, cast(QueryTermModifier, modifier))
            )
            return modifier

        return wrapper

    def modify_query_attr(
        self, attr_value: Optional[str] = None
    ) -> Callable[[QueryAttrModifier], QueryAttrModifier]:
        def wrapper(modifier: QueryAttrModifier):
            self.query_term_modifiers.append(
                (cast(Type[Term], Attr), attr_value, cast(QueryTermModifier, modifier))
            )
            return modifier

        return wrapper

    def modify_note(
        self, query: Term = All(), help: Optional[Dict[str, str]] = None
    ) -> Callable[[NoteModifier], NoteModifier]:
        if help is not None:
            self.edit_help.update(help)

        def wrapper(modifier: NoteModifier):
            self.note_modifiers.append((query, modifier))
            return modifier

        return wrapper


class PluginManager:
    def __init__(self, plugins: List[Plugin]):
        self.plugins = plugins
        self.query_term_replacements: Dict[Term, Term] = {}
        self.query_term_modifiers: List[ModifierStruct] = []
        self.note_modifiers: List[NoteModifierStruct] = []
        self.query_help: Dict[str, str] = {}
        self.edit_help: Dict[str, str] = {}
        for plugin in self.plugins:
            self.query_term_replacements.update(plugin.query_term_replacements)
            self.query_term_modifiers.extend(plugin.query_term_modifiers)
            self.note_modifiers.extend(plugin.note_modifiers)
            self.query_help.update(plugin.query_help)
            self.edit_help.update(plugin.edit_help)

    def get_routers(self) -> List[APIRouter]:
        return [plugin.router for plugin in self.plugins if plugin.router is not None]

    def _preprocess_query_term(self, term: Term) -> Term:
        try:
            hash(term)
        except TypeError:
            return term
        return self.query_term_replacements.get(term, term)

    def preprocess_query(self, query: Term) -> Term:
        query = query.visit(self._preprocess_query_term) or Term()
        for ttype, value, modifier in self.query_term_modifiers:

            def visitor(term: Term):
                if isinstance(term, ttype) and value is None:
                    return modifier(term) or term
                elif ttype is Tag and isinstance(term, Tag) and value == term.tag:
                    return modifier(term) or term
                elif ttype is Attr and isinstance(term, Attr) and value == term.name:
                    return modifier(term) or term
                else:
                    return term

            query = query.visit(visitor) or query
        return query

    def preprocess_note(self, note: Note, user_account: UserAccount) -> Note:
        for query, modifier in self.note_modifiers:
            if query.match(note):
                note = modifier(note, user_account)
        return note


def import_plugins(plugin_package: str) -> List[Plugin]:
    result = []

    base_package = importlib.import_module(plugin_package)
    base_path = base_package.__path__  # type: ignore  # mypy issue 1422
    for module_info in pkgutil.iter_modules(base_path):
        module = importlib.import_module(f"{plugin_package}.{module_info.name}")
        result.extend(
            [value for value in vars(module).values() if isinstance(value, Plugin)]
        )

    return sorted(result, key=lambda plugin: plugin.name)


_plugin_manager: Optional[PluginManager] = None


def get_plugin_manager() -> PluginManager:
    global _plugin_manager
    if _plugin_manager is None:
        _plugin_manager = PluginManager(import_plugins("oleeka_api.plugins"))
    return _plugin_manager
