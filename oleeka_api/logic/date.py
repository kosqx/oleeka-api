import re
import time
from datetime import datetime


OLEEKA_DATE_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def oleeka_date_format(dt: datetime) -> str:
    return dt.strftime(OLEEKA_DATE_FORMAT)


def oleeka_date_parse(text: str) -> datetime:
    return datetime.strptime(text, OLEEKA_DATE_FORMAT)


PARSE_TIMEDELTA_SUFIXES = dict(
    [
        ("s", 1),
        ("m", 60),
        ("h", 60 * 60),
        ("d", 60 * 60 * 24),
        ("w", 60 * 60 * 24 * 7),
        ("M", 60 * 60 * 24 * 30),
        ("y", 60 * 60 * 24 * 365),
        ("Y", 60 * 60 * 24 * 365),
    ]
)
PARSE_TIMEDELTA_REGEXP = re.compile(r"([smhdwMYy])")


def parse_timedelta(text: str) -> float:
    if text.startswith(("-", "+")):
        sign = -1 if text[0] == "-" else +1
        text = text[1:]
    else:
        sign = 1

    parts = PARSE_TIMEDELTA_REGEXP.split(text)
    if parts[-1] == "":
        parts.pop()
    else:
        parts.append("s")

    result = 0.0
    for val, mul in zip(parts[::2], parts[1::2]):
        result += float(val or 1) * PARSE_TIMEDELTA_SUFIXES.get(mul, 1)

    return result * sign


def datetime_to_timestamp(dt: datetime) -> float:
    return time.mktime(dt.timetuple())


def timestamp_to_datetime(ts: float) -> datetime:
    return datetime.fromtimestamp(ts)
