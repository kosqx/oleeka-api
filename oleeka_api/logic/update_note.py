from typing import Dict, List, Optional, cast

from oleeka_api.logic.base import OleekaConflictException
from oleeka_api.logic.note import generate_nid, validate_nid

from oleeka_api.logic.note import generate_datetime_str
from oleeka_api.models import File, Note
from oleeka_api.modifier import update_note_with_input
from oleeka_api.api.models import FileInputModel, NoteInputModel


def _build_nid(note_db: Optional[Note], note_input: NoteInputModel) -> str:
    if note_db is not None and note_db.nid:
        return note_db.nid
    elif validate_nid(note_input.nid):
        return cast(str, note_input.nid)
    else:
        return generate_nid()


def _build_text(note_db: Optional[Note], note_input: NoteInputModel) -> str:
    if "text" in note_input.__fields_set__:
        if isinstance(note_input.text, list):
            return "".join(note_input.text)
        else:
            return note_input.text
    elif note_db is not None:
        return note_db.text
    else:
        return ""


def _build_tags(note_db: Optional[Note], note_input: NoteInputModel) -> List[str]:
    if "tags" in note_input.__fields_set__:
        return note_input.tags or []
    elif note_db is not None:
        return note_db.tags
    else:
        return []


def _build_attrs(note_db: Optional[Note], note_input: NoteInputModel) -> Dict[str, str]:
    if note_db is None:
        attrs = note_input.attrs.copy() if isinstance(note_input.attrs, dict) else {}

        if "created" not in attrs:
            attrs["created"] = generate_datetime_str()
        if not note_input.restore:
            attrs["modified"] = generate_datetime_str()

        return attrs

    if "attrs" in note_input.__fields_set__ and isinstance(note_input.attrs, dict):
        attrs = note_input.attrs.copy()
    else:
        attrs = note_db.attrs.copy()

    if "created" not in attrs:
        if "created" in note_db.attrs:
            attrs["created"] = note_db.attrs["created"]
        else:
            attrs["created"] = generate_datetime_str()

    if "modified" not in (note_input.attrs or {}):
        attrs["modified"] = generate_datetime_str()
    else:
        # when `modified` is specified in input then client want to use
        # Compare-and-swap (CAS) to ensure that parallel edit is not overwritten
        if (
            "modified" in note_db.attrs
            and note_db.attrs["modified"] != attrs["modified"]
        ):
            raise OleekaConflictException(
                "note has been updated in meantime",
                {"request": attrs["modified"], "storage": note_db.attrs["modified"]},
            )
        else:
            attrs["modified"] = generate_datetime_str()

    return attrs


def _update_existing_file(file_cmd: FileInputModel, files: Dict[str, File]):
    assert file_cmd.fid
    if file_cmd.delete:
        files.pop(file_cmd.fid, None)
    else:
        file = files[file_cmd.fid]
        if "name" in file_cmd.__fields_set__:
            file = file.replace(name=file_cmd.name)
        if "mime" in file_cmd.__fields_set__:
            file = file.replace(mime=file_cmd.mime)
        if "meta" in file_cmd.__fields_set__:
            file = file.replace(meta=file_cmd.meta)
        if "data" in file_cmd.__fields_set__:
            data = file_cmd.get_decoded_data()
            file = file.replace_data(data)

        files[file.fid] = file


def _add_new_file(file_cmd: FileInputModel, files: Dict[str, File]):
    assert not file_cmd.delete
    new_file = file_cmd.to_file()
    files[new_file.fid] = new_file


def _build_files(note_db: Optional[Note], note_input: NoteInputModel) -> List[File]:
    if note_db is None:
        return [file_input.to_file() for file_input in (note_input.files or [])]
    else:
        files = {file.fid: file for file in note_db.files}
        for file_cmd in note_input.files or []:
            if file_cmd.fid and file_cmd.fid in files:
                _update_existing_file(file_cmd, files)
            else:
                _add_new_file(file_cmd, files)

        return sorted(files.values(), key=lambda f: f.name)


def merge_note(note_db: Optional[Note], note_input: NoteInputModel) -> Note:
    note_db = None if note_input.restore else note_db
    note = Note(
        nid=_build_nid(note_db, note_input),
        text=_build_text(note_db, note_input),
        tags=_build_tags(note_db, note_input),
        attrs=_build_attrs(note_db, note_input),
        files=_build_files(note_db, note_input),
    )
    return update_note_with_input(note, note_input.input)
