from typing import Any


class OleekaException(Exception):
    def __init__(self, message: str, info: Any = None):
        super().__init__(message, info)
        self.message = message
        self.info = info

    def get_detail(self):
        return self.message


class OleekaObjectMissingException(OleekaException):
    pass


class OleekaConflictException(OleekaException):
    pass
