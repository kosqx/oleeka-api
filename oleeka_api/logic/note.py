import re
import random
from datetime import datetime, timedelta
from typing import Any, Optional

import pyparsing  # type: ignore

from oleeka_api.logic.date import oleeka_date_format


VALID_NAME_CHARACTERS = pyparsing.pyparsing_unicode.alphanums + ":-_."
VALID_NAME_CHARACTERS_SET = frozenset(VALID_NAME_CHARACTERS)
VALID_NID_PATTERN = r"^\d{8}T\d{12}R\d{6}$"
VALID_FID_PATTERN = r"^F[0-9TR]+$"


def validate_name(name: Any) -> bool:
    return isinstance(name, str) and set(name) < VALID_NAME_CHARACTERS_SET


def generate_nid(now: Optional[datetime] = None, rand: Optional[int] = None) -> str:
    rand_limit = 10**6

    if now is None:
        now = datetime.utcnow()

    if rand is None:
        rand = random.randint(0, rand_limit)
    else:
        rand = rand % rand_limit

    return "%sR%.6d" % (now.strftime("%Y%m%dT%H%M%S%f"), rand)


def validate_nid(nid: Any) -> bool:
    if not isinstance(nid, str):
        return False
    return re.match(VALID_NID_PATTERN, nid) is not None


def generate_fid(now: Optional[datetime] = None, rand: Optional[int] = None) -> str:
    return "F" + generate_nid(now=now, rand=rand)


def validate_fid(fid: Any) -> bool:
    if not isinstance(fid, str):
        return False
    return re.match(VALID_FID_PATTERN, fid) is not None


def generate_datetime_str(delta=0) -> str:
    return oleeka_date_format(datetime.utcnow() + timedelta(seconds=delta))


def validate_datetime_str(dt: Any) -> bool:
    if not isinstance(dt, str):
        return False
    return re.match(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$", dt) is not None
