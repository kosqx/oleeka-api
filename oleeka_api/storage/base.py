import os
from typing import List, Optional, Union, Set

from oleeka_api.models import (
    File,
    Note,
    UserAccount,
    PaginatedQuery,
    RelatedRequest,
    RelatedStats,
)


RecordType = Union[Note, UserAccount]


class StorageBackend:
    URL_ENVVAR = "OLEEKA_STORAGE_BACKEND"

    @classmethod
    def get_backend_url(cls):
        return os.environ.get(cls.URL_ENVVAR, "")

    @classmethod
    async def testing_deinit(cls):
        pass

    async def testing_clear(self):
        raise NotImplementedError

    async def testing_insert(self, *entities: RecordType, clear=True):
        if clear:
            await self.testing_clear()

        username: Optional[str] = None

        for entity in entities:
            if isinstance(entity, Note):
                await self.save_notes(username or "", [entity])
            elif isinstance(entity, UserAccount):
                username = entity.username
                await self.save_user(entity)

    async def get_notes(self, query: PaginatedQuery) -> List[Note]:
        raise NotImplementedError

    async def save_notes(self, username: str, note: List[Note]):
        raise NotImplementedError

    async def delete_notes(self, username: str, nids: List[str]) -> Set[str]:
        raise NotImplementedError

    async def get_file(
        self, username: str, nid: str, fid: str, *, fetch_data: bool = True
    ) -> File:
        raise NotImplementedError

    async def get_related(self, request: RelatedRequest) -> RelatedStats:
        raise NotImplementedError

    async def get_user(self, username: str) -> Optional[UserAccount]:
        raise NotImplementedError

    async def save_user(self, user: UserAccount):
        raise NotImplementedError
