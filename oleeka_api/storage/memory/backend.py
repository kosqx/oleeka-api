import random
from collections import Counter
from typing import Dict, List, Optional, Set, Counter as CounterType, cast

from oleeka_api.logic.base import OleekaObjectMissingException

from oleeka_api.models import File, Note, UserAccount, Query, PaginatedQuery
from oleeka_api.models import RelatedRequest, RelatedStats, SizeStats
from oleeka_api.models import FETCH_DATES_OFFSET
from oleeka_api.storage.base import StorageBackend, RecordType


def note_key(username: str, nid: str) -> str:
    return "note:{}:{}".format(username or "", nid)


def user_key(username: str) -> str:
    return "user:{}".format(username or "")


class MemoryStorageBackend(StorageBackend):
    shared_data: Dict[str, RecordType] = {}

    def __init__(self):
        self._db = self.shared_data

    def _iter_by_kind(self, kind: str, category: Optional[str] = None):
        for key, value in self._db.items():
            parts = key.split(":")
            if parts[0] == kind and (not category or parts[1] == category):
                yield value

    def _get_filtered(self, query: Query) -> List[Note]:
        return [
            note
            for note in self._iter_by_kind("note", query.username)
            if query.query_terms.match(note)
        ]

    async def testing_clear(self):
        self._db.clear()

    async def get_notes(self, query: PaginatedQuery) -> List[Note]:
        notes_filtered = self._get_filtered(query)

        if query.random:
            random.shuffle(notes_filtered)
            notes_filtered = notes_filtered[: query.limit]
            offset = 0
        else:
            offset = query.skip

        notes_ordered = sorted(
            notes_filtered, key=lambda note: note.attrs.get("created", ""), reverse=True
        )[offset : offset + query.limit]
        return [note.select_fields(query.select_fields) for note in notes_ordered]

    async def save_notes(self, username: str, notes: List[Note]):
        for note in notes:
            self._db[note_key(username, note.nid)] = note

    async def delete_notes(self, username: str, nids: List[str]) -> Set[str]:
        existing_nids = [nid for nid in nids if note_key(username, nid) in self._db]
        for nid in existing_nids:
            del self._db[note_key(username, nid)]

        return set(existing_nids)

    async def get_file(
        self, username: str, nid: str, fid: str, *, fetch_data: bool = True
    ) -> File:
        for note in self._iter_by_kind("note", username):
            if note.nid != nid:
                continue
            try:
                file = note.get_file(fid=fid)
            except IndexError:
                break
            result = file if fetch_data else file.replace(data=None)
            return cast(File, result)  # this shouldn't be required...

        raise OleekaObjectMissingException("file not found", fid)

    async def get_related(self, request: RelatedRequest) -> RelatedStats:
        filtered = self._get_filtered(request.query)
        tags: CounterType[str] = Counter()
        tags_empty = 0
        attrs: CounterType[str] = Counter()
        sizes = SizeStats()
        dates: CounterType[str] = Counter()
        for note in filtered:
            tags.update(note.tags)
            tags_empty += int(note.tags == [])
            attrs.update(note.attrs.keys())
            sizes = sizes + SizeStats.from_note(note)
            offset = FETCH_DATES_OFFSET.get(request.fetch_dates, 0)
            dates.update([note.attrs["created"][:offset]])

        return RelatedStats(
            total=len(filtered) if request.should_fetch_total() else None,
            tags_empty=tags_empty if request.fetch_tags else None,
            tags=dict(tags) if request.fetch_tags else None,
            attrs=dict(attrs) if request.fetch_attrs else None,
            sizes=sizes if request.fetch_sizes else None,
            dates=dict(dates) if request.fetch_dates else None,
        )

    async def get_user(self, username: str) -> Optional[UserAccount]:
        user_account = self._db.get(user_key(username))
        return cast(Optional[UserAccount], user_account)

    async def save_user(self, user: UserAccount):
        self._db[user_key(user.username)] = user
