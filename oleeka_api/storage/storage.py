from typing import List, Optional, Set

from oleeka_api.models import (
    Note,
    UserAccount,
    PaginatedQuery,
    RelatedRequest,
    RelatedStats,
)
from oleeka_api.storage.base import StorageBackend, RecordType
from oleeka_api.storage.memory.backend import MemoryStorageBackend
from oleeka_api.storage.mongodb.backend import MongodbStorageBackend
from oleeka_api.storage.postgresql.backend import PostgresqlStorageBackend
from oleeka_api.storage.sqlite.backend import SqliteStorageBackend


class Storage:
    def __init__(self, backend: StorageBackend):
        self._backend = backend

    @classmethod
    async def get_db(cls):
        backend_url = StorageBackend.get_backend_url()
        if backend_url.startswith("memory://"):
            yield cls(MemoryStorageBackend())
        if backend_url.startswith("mongodb://"):
            backend = await MongodbStorageBackend.acquire_backend()
            yield cls(backend)
        elif backend_url.startswith("postgresql://"):
            async with PostgresqlStorageBackend.acquire_backend() as backend:
                yield cls(backend)
        elif backend_url.startswith("sqlite://"):
            yield cls(SqliteStorageBackend())

    def __repr__(self):
        return f"Storage({self._backend.__class__.__name__}())"

    async def get_notes(self, query: PaginatedQuery) -> List[Note]:
        return await self._backend.get_notes(query)

    async def save_note(self, username: str, note: Note):
        return await self._backend.save_notes(username, [note])

    async def save_notes(self, username: str, notes: List[Note]):
        return await self._backend.save_notes(username, notes)

    async def delete_notes(self, username: str, nids: List[str]) -> Set[str]:
        return await self._backend.delete_notes(username, nids)

    async def get_file(
        self, username: str, nid: str, fid: str, *, fetch_data: bool = True
    ):
        return await self._backend.get_file(username, nid, fid, fetch_data=fetch_data)

    async def get_related(self, request: RelatedRequest) -> RelatedStats:
        return await self._backend.get_related(request)

    async def get_user(self, username: str) -> Optional[UserAccount]:
        return await self._backend.get_user(username)

    async def save_user(self, user: UserAccount):
        return await self._backend.save_user(user)

    async def testing_insert(self, *entities: RecordType, clear=True):
        return await self._backend.testing_insert(*entities, clear=clear)
