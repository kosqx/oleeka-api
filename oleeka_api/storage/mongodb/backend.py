from urllib.parse import urlparse
from typing import Dict, List, Optional, Set, Tuple, cast

from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase  # type: ignore

from oleeka_api.logic.base import OleekaObjectMissingException
from oleeka_api.models import File, Note, UserAccount
from oleeka_api.models import PaginatedQuery, RelatedRequest, RelatedStats
from oleeka_api.storage.base import StorageBackend
from oleeka_api.storage.mongodb import build_query
from oleeka_api.storage.mongodb import encoding
from oleeka_api.storage.mongodb.initialize import initialize_db


QUERY_TIMEOUT_MS = int(10.0 * 1000)


def parse_mongodb_url(backend_url: str) -> Tuple[str, str]:
    url = urlparse(backend_url)
    return url._replace(path="").geturl(), url.path.strip("/")


class MongodbStorageBackend(StorageBackend):
    initialized = False

    @classmethod
    async def acquire_backend(cls) -> "MongodbStorageBackend":
        # TODO: research connection pooling
        backend_url = cls.get_backend_url()
        connection_str, database_name = parse_mongodb_url(backend_url)
        client = AsyncIOMotorClient(connection_str)
        if cls.initialized:
            db = client[database_name]
        else:
            db = await initialize_db(client, database_name)
            cls.initialized = True
        return cls(db)

    def __init__(self, db: AsyncIOMotorDatabase):
        self.db = db

    async def _upsert(self, collection_name: str, record: encoding.MongoDict) -> None:
        await self.db[collection_name].replace_one(
            filter={"_id": record["_id"]},
            replacement=record,
            upsert=True,
        )

    async def testing_clear(self) -> None:
        await self.db.notes.delete_many({})
        await self.db.files.delete_many({})
        await self.db.users.delete_many({})

    async def _get_files_data_mapping(
        self, records: List[encoding.MongoDict], query: PaginatedQuery
    ) -> Dict[str, bytes]:
        if not query.select_fields.files_data:
            return {}
        file_hashes = {
            file_record["fid"]
            for record in records
            for file_record in record.get("files", [])
        }
        if not file_hashes:
            return {}
        file_records = await self.db.files.find(
            filter={"_id": {"$in": list(file_hashes)}},
            projection={"_id": 1, "data": 1},
            max_time_ms=QUERY_TIMEOUT_MS,
        ).to_list(length=len(file_hashes))
        return {record["_id"]: record["data"] for record in file_records}

    async def get_notes(self, query: PaginatedQuery) -> List[Note]:
        pipeline = build_query.get_notes(query)
        cursor = self.db.notes.aggregate(pipeline, maxTimeMS=QUERY_TIMEOUT_MS)
        records = await cursor.to_list(length=query.limit)
        files_data_mapping = await self._get_files_data_mapping(records, query)
        return [encoding.decode_note(record, files_data_mapping) for record in records]

    async def save_notes(self, username: str, notes: List[Note]) -> None:
        for note in notes:
            await self._upsert("notes", encoding.encode_note(note, username))
            for file in note.files:
                record = encoding.encode_file_standalone(
                    file, username=username, nid=note.nid
                )
                await self._upsert("files", record)

    async def delete_notes(self, username: str, nids: List[str]) -> Set[str]:
        deleted = set()
        for nid in nids:
            result = await self.db.notes.delete_one({"username": username, "_id": nid})
            if result.deleted_count:
                deleted.add(nid)
        return deleted

    async def get_file(
        self, username: str, nid: str, fid: str, *, fetch_data: bool = True
    ) -> File:
        condition = {"_id": fid, "username": username, "nid": nid}
        projection = None if fetch_data else {"data": 0}
        record = await self.db.files.find_one(filter=condition, projection=projection)
        if record:
            return encoding.decode_file_standalone(record)

        raise OleekaObjectMissingException("file not found", fid)

    async def get_related(self, request: RelatedRequest) -> RelatedStats:
        pipeline = build_query.get_related(request)
        if pipeline is None:
            return RelatedStats()
        else:
            records = await self.db.notes.aggregate(
                pipeline, maxTimeMS=QUERY_TIMEOUT_MS
            ).to_list(length=1)
            return encoding.decode_related_stats(records[0])

    async def get_user(self, username: str) -> Optional[UserAccount]:
        record = await self.db.users.find_one({"_id": username})
        user_account = encoding.decode_user(record) if record is not None else None
        return cast(Optional[UserAccount], user_account)

    async def save_user(self, user: UserAccount) -> None:
        await self._upsert("users", encoding.encode_user(user))
