import re
from functools import lru_cache, partial
from typing import cast, Any, Callable, ClassVar, Dict, Type

from oleeka_api.query.terms import Term, All, Nid, Text, Tag, Attr, Mark, Not, And, Or
from oleeka_api.storage.mongodb.encoding import encode_key
from oleeka_api.utils.text import cast_number


Condition = Dict[str, Any]
AttrFunction = Callable[[str, str, str], Condition]


class TermQuery(Term):
    def to_condition(self) -> Condition:
        return {"_id": ""}

    @classmethod
    @lru_cache(maxsize=None)
    def resolve(cls) -> Dict[Type[Term], Type["TermQuery"]]:
        def get_term_class(klass: Type["TermQuery"]) -> Type[Term]:
            return [
                cast(Type["TermQuery"], subclass)
                for subclass in klass.mro()
                if TermQuery.__module__ != subclass.__module__
            ][0]

        result = {get_term_class(klass): klass for klass in cls.__subclasses__()}
        result[Term] = cls
        return result

    @classmethod
    def run_to_condition(cls, term: Term) -> Condition:
        mapping = TermQuery.resolve()
        return mapping[term.__class__].to_condition(cast(TermQuery, term))


class AllQuery(All, TermQuery):
    def to_condition(self) -> Condition:
        return {}


class TextQuery(Text, TermQuery):
    def to_condition(self) -> Condition:
        return {"text": re.compile(self.value)}


class NidQuery(Nid, TermQuery):
    def to_condition(self) -> Condition:
        return {"_id": {"$in": self.nids}}


class TagQuery(Tag, TermQuery):
    def to_condition(self) -> Condition:
        if self.mode == "exact":
            return {"tags": self.tag}
        before, after, _ = self.MODES[self.mode]
        parts = ["^", ".*" * bool(before), re.escape(self.tag), ".*" * bool(after), "$"]
        return {"tags": re.compile("".join(parts))}


def _build_regex(pattern: str, operator: str, field: str, value: str) -> Condition:
    insensitive = not Attr.is_operator_case_sensitive(operator)
    regex = re.compile(
        pattern.format(re.escape(value)),
        re.IGNORECASE * insensitive,
    )
    return {field: regex}


def _build_order_comparison(operator: str, field: str, value: str) -> Condition:
    action = "$lte" if operator.startswith("<") else "$gte"
    insensitive = "==" not in operator
    if insensitive:
        # `{"$toUpper": undefined} == ""` so we need to first check if attr exists
        exists = {"$eq": [{"$type": f"${field}"}, "string"]}
        cmp = {action: [{"$toUpper": f"${field}"}, value.upper()]}
        return {"$expr": {"$and": [exists, cmp]}}
    else:
        return {field: {action: value}}


def _build_number_comparison(operator: str, field: str, value: str) -> Condition:
    number = cast_number(value)
    if number is not None:
        action = "$lte" if operator == "<<=" else "$gte"
        return {f"{field}|num": {action: float(value)}}
    else:
        return {"_id": ""}


class AttrQuery(Attr, TermQuery):
    OPERATOR_TO_CONDITION: ClassVar[Dict[str, AttrFunction]] = {
        "?": lambda _operator, field, _value: {field: {"$exists": True}},
        "=": partial(_build_regex, "{}"),
        "==": partial(_build_regex, "^{}$"),
        "===": lambda _operator, field, value: {field: value},
        "^=": partial(_build_regex, "^{}"),
        "^==": partial(_build_regex, "^{}"),
        "$=": partial(_build_regex, "{}$"),
        "$==": partial(_build_regex, "{}$"),
        "*=": partial(_build_regex, "{}"),
        "*==": partial(_build_regex, "{}"),
        "+=": partial(_build_regex, "\\b{}\\b"),
        "+==": partial(_build_regex, "\\b{}\\b"),
        "<=": _build_order_comparison,
        "<==": _build_order_comparison,
        ">=": _build_order_comparison,
        ">==": _build_order_comparison,
        "<<=": _build_number_comparison,
        ">>=": _build_number_comparison,
    }

    def to_condition(self) -> Condition:
        condition = AttrQuery.OPERATOR_TO_CONDITION[self.operator]
        return condition(self.operator, f"attrs.{encode_key(self.name)}", self.value)


class MarkQuery(Mark, TermQuery):
    FEATURE_TO_CONDITION: ClassVar[Dict[str, Any]] = {
        # TODO: consider using values stored in `$_sizes`
        "tags_count": {"$size": "$tags"},
        "attrs_count": {"$size": {"$objectToArray": "$attrs"}},
        "files_count": {"$size": "$files"},
        "files_size": {"$sum": "$files.size"},
        "modified_after": {
            "$divide": [
                {
                    "$subtract": [
                        {"$toDate": "$attrs.modified"},
                        {"$toDate": "$attrs.created"},
                    ]
                },
                1000,
            ]
        },
        "date_day_of_week": {"$isoDayOfWeek": {"date": {"$toDate": "$attrs.created"}}},
        "date_day_of_year": {"$dayOfYear": {"date": {"$toDate": "$attrs.created"}}},
        "date_week_of_year": {"$isoWeek": {"date": {"$toDate": "$attrs.created"}}},
    }
    OPERATOR_TO_CONDITION: ClassVar[Dict[str, str]] = {
        "==": "$eq",
        "!=": "$ne",
        "<": "$lt",
        "<=": "$lte",
        ">": "$gt",
        ">=": "$gte",
    }

    def to_condition(self) -> Condition:
        field = MarkQuery.FEATURE_TO_CONDITION[self.feature]
        op = MarkQuery.OPERATOR_TO_CONDITION[self.operator]
        return {"$expr": {op: [field, int(self.value)]}}


class NotQuery(Not, TermQuery):
    def to_condition(self) -> Condition:
        return {"$nor": [TermQuery.run_to_condition(self.condition)]}


class AndQuery(And, TermQuery):
    def to_condition(self) -> Condition:
        parts = [TermQuery.run_to_condition(cond) for cond in self.conditions]
        return {"$and": parts}


class OrQuery(Or, TermQuery):
    def to_condition(self) -> Condition:
        parts = [TermQuery.run_to_condition(cond) for cond in self.conditions]
        return {"$or": parts}


def to_condition(term: Term) -> Condition:
    return TermQuery.run_to_condition(term)
