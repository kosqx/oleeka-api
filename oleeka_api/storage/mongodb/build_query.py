from typing import List, Optional

from pymongo import DESCENDING

from oleeka_api.models import Query, PaginatedQuery, SelectFields, RelatedRequest
from oleeka_api.models import FETCH_DATES_OFFSET
from oleeka_api.storage.mongodb.encoding import MongoDict, encode_key
from oleeka_api.storage.mongodb.query_terms import to_condition


def _build_match(query: Query) -> MongoDict:
    condition = to_condition(query.query_terms.simplify())
    if query.username is not None:
        user_filter = {"username": query.username}
        condition = {"$and": [condition, user_filter]}
    return {"$match": condition}


def _build_note_projection(select: SelectFields) -> List[MongoDict]:
    fields = {
        "_id": True,
        "text": select.text,
        "tags": select.tags,
        "files": select.files,
    }

    if isinstance(select.attrs, set):
        for attr in sorted(select.attrs):
            fields[f"attrs.{encode_key(attr)}"] = True
    else:
        fields["attrs"] = bool(select.attrs)

    include = {name: 1 for name, value in fields.items() if value}
    result = [{"$project": include}]
    if select.files and not select.files_data:
        result.append({"$project": {"files.data": 0}})
    return result


def get_notes(query: PaginatedQuery) -> List[MongoDict]:
    if query.random:
        return [
            _build_match(query),
            {"$sample": {"size": query.limit}},
            {"$sort": {"created": DESCENDING}},
            *_build_note_projection(query.select_fields),
        ]
    else:
        return [
            _build_match(query),
            {"$sort": {"created": DESCENDING}},
            {"$skip": query.skip},
            {"$limit": query.limit},
            *_build_note_projection(query.select_fields),
        ]


def get_related(request: RelatedRequest) -> Optional[List[MongoDict]]:
    facet: MongoDict = {}

    if request.should_fetch_total():
        facet["total"] = [
            {"$count": "count"},
        ]
    if request.fetch_tags:
        facet["tags"] = [
            {"$unwind": {"path": "$tags", "preserveNullAndEmptyArrays": True}},
            {"$sortByCount": "$tags"},
        ]
    if request.fetch_attrs:
        facet["attrs"] = [
            {"$project": {"fields": {"$objectToArray": "$attrs"}}},
            {"$unwind": "$fields"},
            {"$sortByCount": "$fields.k"},
        ]

    if request.fetch_sizes:
        facet["sizes"] = [
            {
                "$group": {
                    "_id": None,
                    "tags_counts": {"$sum": "$_sizes.tags_counts"},
                    "attrs_counts": {"$sum": "$_sizes.attrs_counts"},
                    "files_counts": {"$sum": "$_sizes.files_counts"},
                    "files_size": {"$sum": "$_sizes.files_size"},
                }
            },
        ]

    if request.fetch_dates:
        offset = FETCH_DATES_OFFSET.get(request.fetch_dates, 0)
        facet["dates"] = [
            {"$project": {"date": {"$substrBytes": ["$attrs.created", 0, offset]}}},
            {"$sortByCount": "$date"},
        ]

    if facet:
        return [
            _build_match(request.query),
            {"$facet": facet},
        ]
    else:
        return None
