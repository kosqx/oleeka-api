from typing import cast, Any, Dict, Optional, Tuple

from oleeka_api.logic.date import oleeka_date_parse
from oleeka_api.models import File, Note, UserAccount, RelatedStats, SizeStats
from oleeka_api.utils.text import cast_number


MongoDict = Dict[str, Any]


def encode_key(key: str) -> str:
    return key.replace("~", "~7e~").replace(".", "~2e~")


def decode_key(key: str) -> str:
    return key.replace("~2e~", ".").replace("~7e~", "~")


def encode_dict(dict_: MongoDict) -> MongoDict:
    return {encode_key(key): value for key, value in dict_.items()}


def decode_dict(dict_: MongoDict) -> MongoDict:
    return {decode_key(key): value for key, value in dict_.items()}


def encode_attrs(attrs: MongoDict) -> MongoDict:
    fields = [(key, cast_number(value)) for key, value in attrs.items()]
    numeric = {f"{key}|num": value for key, value in fields if value is not None}
    return encode_dict({**attrs, **numeric})


def decode_attrs(attrs: MongoDict) -> MongoDict:
    return encode_dict({key: value for key, value in attrs.items() if "|" not in key})


def encode_note(note: Note, username: str) -> MongoDict:
    return {
        "username": username,
        "created": oleeka_date_parse(note.attrs["created"]),
        "_id": note.nid,
        "text": note.text,
        "tags": note.tags,
        "attrs": encode_attrs(note.attrs),
        "files": [encode_file_nested(file) for file in note.files],
        "_sizes": {
            "text_size": len(note.text),
            "tags_counts": len(note.tags),
            "attrs_counts": len(note.attrs),
            "files_counts": len(note.files),
            "files_size": sum(file.size for file in note.files),
        },
    }


def decode_note(record: MongoDict, files_data_mapping: Dict[str, bytes]) -> Note:
    return Note(
        nid=record["_id"],
        text=record["text"] if "text" in record.keys() else "",
        tags=record["tags"] if "tags" in record.keys() else [],
        attrs=decode_attrs(record["attrs"]) if "attrs" in record.keys() else {},
        files=[
            decode_file_nested(file, files_data_mapping)
            for file in record.get("files", [])
        ],
    )


def _encode_file(file: File) -> MongoDict:
    return {
        "size": file.size,
        "hash": file.hash,
        "name": file.name,
        "mime": file.mime,
        "meta": encode_dict(file.meta),
        "data": file.data,
    }


def encode_file_nested(file: File) -> MongoDict:
    return {**_encode_file(file), "fid": file.fid, "data": None}


def encode_file_standalone(file: File, username: str, nid: str) -> MongoDict:
    return {**_encode_file(file), "_id": file.fid, "username": username, "nid": nid}


def _decode_file(record: MongoDict, fid: str, data: Optional[bytes]) -> File:
    return File(
        fid=fid,
        name=record["name"],
        hash=record["hash"],
        mime=record["mime"],
        size=record["size"],
        meta=decode_dict(record["meta"]),
        data=data,
    )


def decode_file_nested(record: MongoDict, files_data_mapping: Dict[str, bytes]) -> File:
    data = files_data_mapping.get(record["fid"])
    return _decode_file(record, fid=record["fid"], data=data)


def decode_file_standalone(record: MongoDict) -> File:
    data = record["data"] if "data" in record.keys() else None
    return _decode_file(record, fid=record["_id"], data=data)


def encode_user(user: UserAccount) -> MongoDict:
    return {
        "_id": user.username,
        "email": user.email,
        "auth": user.auth,
        "settings": user.settings,
        "cache": user.cache,
    }


def decode_user(record: MongoDict) -> UserAccount:
    return UserAccount(
        username=record["_id"],
        email=record["email"],
        auth=record["auth"],
        settings=record["settings"],
        cache=record["cache"],
    )


def _decode_related_count(record: MongoDict, key: str) -> int:
    value = record.get(key, [])
    if len(value) == 0:
        return 0
    else:
        return cast(int, value[0]["count"])


def _decode_related_count_nullable(record: MongoDict, key: str) -> Optional[int]:
    value = record.get(key)
    if value is None:
        return None
    elif len(value) == 0:
        return 0
    else:
        return cast(int, value[0]["count"])


def _decode_related_count_dict(
    record: MongoDict, key: str, *, decode: bool = False
) -> Optional[Dict[str, int]]:
    if key in record:
        result = {i["_id"]: i["count"] for i in record[key]}
        return decode_attrs(result) if decode else result
    else:
        return None


def _decode_tags_stats(
    record: MongoDict,
) -> Tuple[Optional[Dict[str, int]], Optional[int]]:
    tags = _decode_related_count_dict(record, "tags")
    if tags is None:
        return None, None
    else:
        tags_empty = tags.pop(None, 0)  # type: ignore
        return tags, tags_empty


def _decode_size_stats(record: MongoDict) -> Optional[SizeStats]:
    if "sizes" in record:
        sizes = (record["sizes"] or [{}])[0]
        return SizeStats.from_dict(sizes)
    else:
        return None


def decode_related_stats(record: MongoDict) -> RelatedStats:
    tags, tags_empty = _decode_tags_stats(record)
    return RelatedStats(
        total=_decode_related_count_nullable(record, "total"),
        tags=tags,
        tags_empty=tags_empty,
        attrs=_decode_related_count_dict(record, "attrs", decode=True),
        dates=_decode_related_count_dict(record, "dates"),
        sizes=_decode_size_stats(record),
    )
