from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorCollection, AsyncIOMotorDatabase  # type: ignore
from pymongo import IndexModel, ASCENDING, DESCENDING  # type: ignore


async def safe_create_indexes(
    collection: AsyncIOMotorCollection, *indexes: IndexModel
) -> None:
    existing_names = {idx.to_dict()["name"] async for idx in collection.list_indexes()}
    new_indexes = [
        index for index in indexes if index.document["name"] not in existing_names
    ]
    if new_indexes:
        await collection.create_indexes(new_indexes)


async def initialize_db(
    client: AsyncIOMotorClient, database_name: str
) -> AsyncIOMotorDatabase:
    db = client[database_name]

    await safe_create_indexes(
        db.notes,
        IndexModel([("tags", ASCENDING)], name="notes_tags_idx"),
        # The `$**` is called index-wildcard
        IndexModel([("attrs.$**", ASCENDING)], name="notes_attrs_idx"),
        IndexModel([("created", DESCENDING)], name="notes_created_idx"),
    )
    await safe_create_indexes(
        db.users,
        IndexModel([("email", ASCENDING)], unique=True, name="users_email_idx"),
    )

    return db
