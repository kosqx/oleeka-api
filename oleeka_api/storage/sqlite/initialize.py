import sqlite3

CURRENT_USER_VERSION = 2
SQLITE_APPLICATION_ID = int("oleeka", base=36)
assert SQLITE_APPLICATION_ID == 0x58A3D63A


def initialize_sqlite(conn: sqlite3.Connection):
    cur = conn.cursor()

    cur.execute(f"PRAGMA application_id = {SQLITE_APPLICATION_ID}")
    cur.execute(f"PRAGMA user_version = {CURRENT_USER_VERSION}")

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS users (
            username TEXT PRIMARY KEY,
            email TEXT NOT NULL,
            auth TEXT NOT NULL,
            settings TEXT NOT NULL,
            cache TEXT NOT NULL
        )
        """
    )
    cur.execute("CREATE UNIQUE INDEX IF NOT EXISTS idx_users_name ON users(username)")

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS notes (
            id INTEGER PRIMARY KEY,
            username TEXT,
            created TEXT,
            nid TEXT NOT NULL UNIQUE,
            text TEXT NOT NULL,
            tags TEXT NOT NULL,
            attrs TEXT NOT NULL
        )
        """
    )
    cur.execute("CREATE UNIQUE INDEX IF NOT EXISTS idx_notes_id ON notes(id)")
    cur.execute("CREATE UNIQUE INDEX IF NOT EXISTS idx_notes_nid ON notes(nid)")
    cur.execute("CREATE INDEX IF NOT EXISTS idx_notes_created ON notes(created)")
    cur.execute(
        "CREATE INDEX IF NOT EXISTS idx_notes_username_created ON notes(username, created)"
    )

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS files (
            id INTEGER PRIMARY KEY,
            note_id INTEGER NOT NULL,
            fid TEXT NOT NULL UNIQUE,
            size INTEGER NOT NULL,
            hash TEXT NOT NULL,
            name TEXT NOT NULL,
            mime TEXT NOT NULL,
            meta TEXT NOT NULL,
            data BLOB NOT NULL,
            FOREIGN KEY (note_id)
                REFERENCES notes (id)
                    ON DELETE CASCADE
                    ON UPDATE NO ACTION
        )
        """
    )
    cur.execute("CREATE INDEX IF NOT EXISTS idx_files_id ON files(id)")
    cur.execute("CREATE INDEX IF NOT EXISTS idx_files_noteid ON files(note_id)")

    conn.commit()
    cur.close()
