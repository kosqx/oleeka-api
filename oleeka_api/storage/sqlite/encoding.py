import json
from typing import Any, Dict, List

from oleeka_api.models import File, Note, UserAccount


def sqlquote(value) -> str:
    assert isinstance(value, str)
    return "'" + value.replace("'", "''") + "'"


def encode_attrs(attrs: Dict[str, str]) -> str:
    return json.dumps(attrs, ensure_ascii=False)


def decode_attrs(attrs_field: str) -> Dict[str, str]:
    attrs = json.loads(attrs_field)
    return {key: val for key, val in attrs.items() if val is not None}


def encode_note(note: Note, username: str) -> Dict[str, str]:
    return {
        "username": username,
        "created": note.attrs["created"].rstrip("Z").replace("T", " "),
        "nid": note.nid,
        "text": note.text,
        "tags": json.dumps(note.tags, ensure_ascii=False),
        "attrs": encode_attrs(note.attrs),
    }


def decode_note(record: Dict[str, Any], files_mapping: Dict[int, List[File]]) -> Note:
    return Note(
        nid=record["nid"],
        text=record["text"] if "text" in record.keys() else "",
        tags=json.loads(record["tags"]) if "tags" in record.keys() else [],
        attrs=decode_attrs(record["attrs"]) if "attrs" in record.keys() else {},
        files=files_mapping.get(record["id"], []),
    )


def encode_file(file: File, note_id: int) -> Dict[str, Any]:
    return {
        "note_id": note_id,
        "fid": file.fid,
        "size": file.size,
        "hash": file.hash,
        "name": file.name,
        "mime": file.mime,
        "meta": json.dumps(file.meta),
        "data": file.data,
    }


def decode_file(record: Dict[str, Any]) -> File:
    return File(
        fid=record["fid"],
        name=record["name"],
        hash=record["hash"],
        mime=record["mime"],
        size=record["size"],
        meta=json.loads(record["meta"]),
        data=record["data"] if "data" in record.keys() else None,
    )


def encode_user(user: UserAccount) -> Dict[str, str]:
    return {
        "username": user.username,
        "email": user.email,
        "auth": json.dumps(user.auth),
        "settings": json.dumps(user.settings),
        "cache": json.dumps(user.cache),
    }


def decode_user(record: Dict[str, Any]) -> UserAccount:
    return UserAccount(
        username=record["username"],
        email=record["email"],
        auth=json.loads(record["auth"]),
        settings=json.loads(record["settings"]),
        cache=json.loads(record["cache"]),
    )
