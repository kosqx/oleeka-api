from typing import Any, Dict, List, Optional, Tuple

from oleeka_api.models import Query, PaginatedQuery, SelectFields
from oleeka_api.models import FetchDatesOptions, FETCH_DATES_OFFSET
from oleeka_api.query.terms import Nid
from oleeka_api.storage.sqlite.encoding import sqlquote
from oleeka_api.storage.sqlite.query_terms import to_sql


def insert(table: str, fields: Dict[str, Any]) -> Tuple[str, Tuple[Any, ...]]:
    names = ", ".join(fields.keys())
    values = ", ".join("?" for _ in fields)
    query = f"INSERT INTO {table} ({names}) VALUES ({values})"
    return query, tuple(fields.values())


def upsert(
    table: str,
    fields: Dict[str, Any],
    conflict_target: List[str],
    returning: Optional[str],
) -> Tuple[str, Tuple[Any, ...]]:
    # based on https://www.sqlite.org/lang_UPSERT.html
    query, values = insert(table, fields)
    target = ", ".join(conflict_target)
    assignments_columns = [name for name in fields if name not in conflict_target]
    assignments = ", ".join(f"{name}=excluded.{name}" for name in assignments_columns)
    rs = f" RETURNING {returning}" if returning else ""
    return f"{query} ON CONFLICT({target}) DO UPDATE SET {assignments}{rs}", values


def _build_file_projection(select: SelectFields) -> str:
    base_fields = ["id", "note_id", "fid", "size", "hash", "name", "mime", "meta"]
    fields = base_fields + ["data"] if select.files_data else base_fields
    return ", ".join(fields)


def get_files(note_ids: List[int], select: SelectFields) -> str:
    assert note_ids, "at least one note_id must be given"
    joined_ids = ",".join(map(str, note_ids))
    projection = _build_file_projection(select)
    return f"SELECT {projection} FROM files WHERE note_id IN ({joined_ids})"


def get_file(username: str, nid: str, fid: str, fetch_data: bool) -> str:
    check_note = f"""
        EXISTS(
            SELECT 1
            FROM notes
            WHERE
                id=files.note_id AND
                notes.nid = {sqlquote(nid)} AND
                notes.username = {sqlquote(username)}
        )
        """

    if fetch_data:
        data_query = "data"
    else:
        data_query = "NULL AS data"

    return f"""
        SELECT id, note_id, fid, size, hash, name, mime, meta, {data_query}
        FROM files
        WHERE fid = {sqlquote(fid)} AND {check_note}
    """


def _build_attrs_projection(select: SelectFields) -> Optional[str]:
    if isinstance(select.attrs, set):
        pairs = ", ".join(
            f"'{attr}', json_extract(attrs, '$.\"{attr}\"')"
            for attr in sorted(select.attrs)
        )
        return f"json_object({pairs}) AS attrs"
    else:
        return "attrs" if select.attrs else None


def _build_note_projection(select: SelectFields) -> str:
    fields = [
        "id",
        "nid",
        "text" if select.text else None,
        "tags" if select.tags else None,
        _build_attrs_projection(select),
    ]
    return ", ".join([field for field in fields if field])


def _build_where(query: Query) -> str:
    result = [to_sql(query.query_terms.simplify())]
    if query.username is not None:
        result.append("(notes.username = {})".format(sqlquote(query.username)))
    return "WHERE {}".format(" AND ".join(result))


def get_notes(query: PaginatedQuery) -> str:
    projection = _build_note_projection(query.select_fields)
    where = _build_where(query)
    if query.random:
        subquery = f"SELECT id FROM notes {where} ORDER BY RANDOM() LIMIT {query.limit}"
        where_query = f"WHERE id IN ({subquery})"
        offset = 0
    else:
        where_query = where
        offset = query.skip

    return f"""
        SELECT {projection}
        FROM notes
        {where_query}
        ORDER BY created DESC
        LIMIT {query.limit} OFFSET {offset}
    """


def delete_notes(username: str, nids: List[str]) -> str:
    where = _build_where(Query(username=username, query_terms=Nid(nids=nids)))
    return f"DELETE FROM notes {where} RETURNING nid"


def get_related_total(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        SELECT count(*) AS cnt
        FROM notes
        {where_query}
    """


def get_related_tags(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        WITH
            filtered_notes(id, tags) AS (
                SELECT id, tags
                FROM notes
                {where_query}
            )
        SELECT t2.value AS name, count(*) AS cnt
        FROM
            filtered_notes AS t1,
            json_each((
                SELECT
                    CASE tags
                    WHEN '[]' THEN '["#ALL#", "#EMPTY#"]'
                    ELSE json_insert(tags,'$[#]', '#ALL#')
                    END
                FROM notes
                WHERE id = t1.id
            )) AS t2
        GROUP BY t2.value
    """


def get_related_sizes(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        WITH filtered AS (
            SELECT id AS note_id FROM notes {where_query}
        ), note_stats AS (
            SELECT
                count(*) AS notes_count,
                -- sum(length(notes.text)) AS text_size,
                0 AS text_size,
                sum(json_array_length(notes.tags)) AS tags_counts
            FROM notes JOIN filtered ON (filtered.note_id = notes.id)
        ), attrs_stats AS (
            SELECT count(*) AS attrs_counts
            FROM
                filtered
                JOIN notes ON (filtered.note_id = notes.id)
                JOIN json_each(attrs) AS t2
        ), file_stats AS (
            SELECT
                count(*) AS files_counts,
                sum(files.size) AS files_size
            FROM files JOIN filtered ON (filtered.note_id = files.note_id)
        )
        SELECT * FROM note_stats CROSS JOIN attrs_stats CROSS JOIN file_stats
    """


def get_related_attrs(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        WITH
            filtered_notes(id, attrs) AS (
                SELECT id, attrs
                FROM notes
                {where_query}
            )
        SELECT t2.key AS name, count(*) AS cnt
        FROM
            filtered_notes AS t1,
            json_each(attrs) AS t2
        GROUP BY t2.key
    """


def get_related_dates(query: Query, date_type: FetchDatesOptions) -> str:
    where_query = _build_where(query)
    offset = FETCH_DATES_OFFSET[date_type]
    return f"""
        SELECT
            substr(json_extract(attrs, '$."created"'), 1, {offset}) AS name,
            count(*) AS cnt
        FROM notes
        {where_query}
        GROUP BY name
    """
