from typing import Any, Dict, List, Optional, Set
import sqlite3

from oleeka_api.logic.base import OleekaObjectMissingException
from oleeka_api.models import File, Note, UserAccount, PaginatedQuery, SelectFields
from oleeka_api.models import RelatedRequest, RelatedStats, SizeStats
from oleeka_api.storage.base import StorageBackend
from oleeka_api.storage.sqlite import build_query
from oleeka_api.storage.sqlite import encoding
from oleeka_api.storage.sqlite.initialize import initialize_sqlite
from oleeka_api.storage.sqlite.query_terms import attr_match, mark_date_extract
from oleeka_api.utils.iterutils import build_dict_of_lists


class SqliteStorageBackend(StorageBackend):
    def __init__(self):
        backend_url = self.get_backend_url()
        self._conn = sqlite3.connect(backend_url.split("://", 1)[-1])
        self._conn.row_factory = sqlite3.Row
        self._conn.create_function("attr_match", 4, attr_match, deterministic=True)
        self._conn.create_function(
            "mark_date_extract", 2, mark_date_extract, deterministic=True
        )

        initialize_sqlite(self._conn)

        self._cur = self._conn.cursor()

    def _upsert(
        self,
        table: str,
        fields: Dict[str, Any],
        target: List[str],
        returning: Optional[str] = None,
    ):
        self._cur.execute(*build_query.upsert(table, fields, target, returning))
        if returning:
            record = self._cur.fetchone()
            return record[returning]
        else:
            return self._cur.lastrowid

    async def testing_clear(self):
        self._cur.execute("DELETE FROM files")
        self._cur.execute("DELETE FROM notes")
        self._cur.execute("DELETE FROM users")
        self._conn.commit()

    def _fetch_files(
        self, note_ids: List[int], select: SelectFields
    ) -> Dict[int, List[File]]:
        if not note_ids or not select.files:
            return {}

        self._cur.execute(build_query.get_files(note_ids, select))
        records = self._cur.fetchall()

        return build_dict_of_lists(
            (record["note_id"], encoding.decode_file(record)) for record in records
        )

    async def get_notes(self, query: PaginatedQuery) -> List[Note]:
        self._cur.execute(build_query.get_notes(query))
        records = self._cur.fetchall()
        files_mapping = self._fetch_files(
            [record["id"] for record in records], query.select_fields
        )
        return [encoding.decode_note(record, files_mapping) for record in records]

    async def save_notes(self, username: str, notes: List[Note]):
        for note in notes:
            encoded_note = encoding.encode_note(note, username)
            note_id = self._upsert("notes", encoded_note, ["nid"], "id")
            for file in note.files:
                self._upsert("files", encoding.encode_file(file, note_id), ["fid"])

        self._conn.commit()

    async def delete_notes(self, username: str, nids: List[str]) -> Set[str]:
        self._cur.execute(build_query.delete_notes(username, nids))
        return {record["nid"] for record in self._cur.fetchall()}

    async def get_file(
        self, username: str, nid: str, fid: str, *, fetch_data: bool = True
    ) -> File:
        self._cur.execute(build_query.get_file(username, nid, fid, fetch_data))
        records = self._cur.fetchall()

        if len(records) == 1:
            return encoding.decode_file(records[0])
        else:
            raise OleekaObjectMissingException("file not found", fid)

    def _fetch_counts(self, sql: str) -> Dict[str, int]:
        self._cur.execute(sql)
        return {record["name"]: record["cnt"] for record in self._cur.fetchall()}

    async def get_related(self, request: RelatedRequest) -> RelatedStats:
        total = None
        tags = None
        empty = None
        attrs = None
        sizes = None
        dates = None

        if request.fetch_tags:
            tags = self._fetch_counts(build_query.get_related_tags(request.query))
            total = tags.pop("#ALL#", 0)
            empty = tags.pop("#EMPTY#", 0)

        if request.fetch_attrs:
            attrs = self._fetch_counts(build_query.get_related_attrs(request.query))
            total = attrs.get("created", 0)

        if request.fetch_sizes:
            self._cur.execute(build_query.get_related_sizes(request.query))
            record = self._cur.fetchall()[0]
            sizes = SizeStats.from_dict(dict(record))
            total = record["notes_count"] or 0

        if request.fetch_dates:
            dates = self._fetch_counts(
                build_query.get_related_dates(request.query, request.fetch_dates)
            )
            total = sum(dates.values())

        if request.fetch_total and total is None:
            self._cur.execute(build_query.get_related_total(request.query))
            record = self._cur.fetchone()
            total = record["cnt"]

        return RelatedStats(
            total=total,
            tags_empty=empty,
            tags=tags,
            attrs=attrs,
            sizes=sizes,
            dates=dates,
        )

    async def get_user(self, username: str) -> Optional[UserAccount]:
        self._cur.execute("SELECT * FROM users WHERE username = ?", (username,))
        record = self._cur.fetchone()
        if record is not None:
            return encoding.decode_user(record)
        else:
            return None

    async def save_user(self, user: UserAccount):
        self._upsert("users", encoding.encode_user(user), ["username"])
        self._conn.commit()
