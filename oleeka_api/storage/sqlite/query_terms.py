import functools
from typing import cast, Dict, Type, ClassVar

from oleeka_api.logic.date import oleeka_date_parse
from oleeka_api.query.terms import Term, All, Nid, Text, Tag, Attr, Mark, Not, And, Or
from oleeka_api.storage.sqlite.encoding import decode_attrs, sqlquote


class TermQuery(Term):
    def to_sql(self) -> str:
        return "(0 = 1)"

    @classmethod
    @functools.lru_cache(maxsize=None)
    def resolve(cls) -> Dict[Type[Term], Type["TermQuery"]]:
        def get_term_class(klass: Type["TermQuery"]) -> Type[Term]:
            return [
                cast(Type["TermQuery"], subclass)
                for subclass in klass.mro()
                if TermQuery.__module__ != subclass.__module__
            ][0]

        result = {get_term_class(klass): klass for klass in cls.__subclasses__()}
        result[Term] = cls
        return result

    @classmethod
    def run_to_sql(cls, term: Term) -> str:
        mapping = TermQuery.resolve()
        return mapping[term.__class__].to_sql(cast(TermQuery, term))


class AllQuery(All, TermQuery):
    def to_sql(self) -> str:
        return "(1 = 1)"


class TextQuery(Text, TermQuery):
    def to_sql(self) -> str:
        return "(notes.text LIKE {})".format(sqlquote("%" + self.value + "%"))


class NidQuery(Nid, TermQuery):
    def to_sql(self) -> str:
        if self.nids:
            nids = ", ".join([sqlquote(nid) for nid in self.nids])
            return "(notes.nid IN ({}))".format(nids)
        else:
            return "(1 = 0)"


class TagQuery(Tag, TermQuery):
    def to_sql(self) -> str:
        before, after, _ = self.MODES[self.mode]
        pattern = "".join(["%", '"' * (not before), self.tag, '"' * (not after), "%"])

        return "(notes.tags LIKE {})".format(sqlquote(pattern))


def attr_match(db_field, name, operator, value):
    try:
        attrs = decode_attrs(db_field)
        if name not in attrs:
            return False
        else:
            return Attr(name, operator, value).match_value(attrs[name])
    except Exception:
        return False


class AttrQuery(Attr, TermQuery):
    def to_sql(self) -> str:
        # TODO: `attr_match` is slow way to find notes, prefilter them first
        return "attr_match(notes.attrs, {}, {}, {})".format(
            sqlquote(self.name), sqlquote(self.operator), sqlquote(self.value)
        )


def mark_date_extract(db_field, feature):
    try:
        created = oleeka_date_parse(decode_attrs(db_field)["created"])
        if feature == "week_of_year":
            return created.isocalendar().week
    except Exception:
        return -1


class MarkQuery(Mark, TermQuery):
    FEATURE_TO_SQL: ClassVar[Dict[str, str]] = {
        "tags_count": "json_array_length(notes.tags)",
        "attrs_count": "(SELECT count(*) FROM json_each(notes.attrs))",
        "files_count": "(SELECT count(*) FROM files WHERE notes.id = note_id)",
        "files_size": "(SELECT coalesce(sum(size), 0) FROM files WHERE notes.id = note_id)",
        "modified_after": (
            "("
            "strftime('%s', json_extract(notes.attrs, '$.modified'))"
            " - "
            "strftime('%s', json_extract(notes.attrs, '$.created'))"
            ")"
        ),
        # we want to have ISO day of week (Monday = 1, ..., Sunday = 7)
        # but SQLite could only return non-ISO one (Sunday = 0, ..., Saturday = 6)
        # converting could be done via: `iso_dow = 7 if dow == 0 else dow`
        # but following is more concise:
        # `assert [(7 - (7 - dow) % 7) for dow in range(7)] == [7, 1, 2, 3, 4, 5, 6]`
        "date_day_of_week": "(7 - (7 - strftime('%w', json_extract(notes.attrs, '$.created'))) % 7)",
        "date_day_of_year": "(strftime('%j', json_extract(notes.attrs, '$.created')) * 1)",
        "date_week_of_year": "mark_date_extract(notes.attrs, 'week_of_year')",
    }

    def to_sql(self) -> str:
        field = MarkQuery.FEATURE_TO_SQL[self.feature]
        return f"({field} {self.operator} {int(self.value)})"


class NotQuery(Not, TermQuery):
    def to_sql(self) -> str:
        sql = TermQuery.run_to_sql(self.condition)
        return "(NOT {})".format(sql)


class AndQuery(And, TermQuery):
    def to_sql(self) -> str:
        parts = [TermQuery.run_to_sql(cond) for cond in self.conditions]
        return "({})".format(" AND ".join(parts))


class OrQuery(Or, TermQuery):
    def to_sql(self) -> str:
        parts = [TermQuery.run_to_sql(cond) for cond in self.conditions]
        return "({})".format(" OR ".join(parts))


def to_sql(term: Term) -> str:
    return TermQuery.run_to_sql(term)
