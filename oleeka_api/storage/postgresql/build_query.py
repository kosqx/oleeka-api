from typing import Any, Dict, List, Optional, Tuple

from asyncpg.utils import _quote_literal as quote_literal  # type: ignore

from oleeka_api.models import Query, PaginatedQuery, SelectFields
from oleeka_api.models import FetchDatesOptions, FETCH_DATES_OFFSET
from oleeka_api.query.terms import Nid
from oleeka_api.storage.postgresql.query_terms import to_sql


def insert(
    table: str, fields: Dict[str, Any], returning: Optional[str] = None
) -> Tuple[str, List[Any]]:
    def cast_value(value):
        return "::hstore" if isinstance(value, dict) else ""

    names = ", ".join(fields.keys())
    values = ", ".join(
        f"${i}{cast_value(value)}" for i, value in enumerate(fields.values(), 1)
    )
    returning_clause = f" RETURNING {returning}" if returning is not None else ""
    query = f"INSERT INTO {table} ({names}) VALUES ({values}){returning_clause}"
    return query, list(fields.values())


def get_files_data(file_hashes: List[str]) -> str:
    hashes = ",".join(map(quote_literal, file_hashes))
    return f"SELECT hash, data FROM files_data WHERE hash IN ({hashes})"


def get_files(note_ids: List[int]) -> str:
    ids = ",".join(map(str, note_ids))
    return f"SELECT * FROM files WHERE note_id IN ({ids})"


def get_existing_file_hashes(file_hashes: List[str]):
    hashes = ",".join(map(quote_literal, file_hashes))
    return f"SELECT hash FROM files_data WHERE hash IN ({hashes})"


def get_file(username: str, nid: str, fid: str, fetch_data: bool) -> str:
    check_note = f"""
        EXISTS(
            SELECT 1
            FROM notes
            WHERE
                id=files.note_id AND
                notes.nid = {quote_literal(nid)} AND
                notes.username = {quote_literal(username)}
        )
        """

    if fetch_data:
        data_query = "(SELECT data FROM files_data WHERE files_data.hash = files.hash)"
    else:
        data_query = "NULL"

    return f"""
        SELECT files.*, {data_query} AS data
        FROM files
        WHERE fid = {quote_literal(fid)} AND {check_note}
    """


def _build_where(query: Query) -> str:
    result = [to_sql(query.query_terms.simplify())]
    if query.username is not None:
        result.append("(notes.username = {})".format(quote_literal(query.username)))
    return "WHERE {}".format(" AND ".join(result))


def _build_attrs_projection(select: SelectFields) -> Optional[str]:
    if isinstance(select.attrs, set):
        keys = ", ".join(map(quote_literal, sorted(select.attrs)))
        return f"slice(attrs, ARRAY[{keys}]) AS attrs"
    else:
        return "attrs" if select.attrs else None


def _build_note_projection(select: SelectFields) -> str:
    fields = [
        "id",
        "nid",
        "text" if select.text else None,
        "tags" if select.tags else None,
        _build_attrs_projection(select),
    ]
    return ", ".join([field for field in fields if field])


def get_notes(query: PaginatedQuery) -> str:
    projection = _build_note_projection(query.select_fields)
    where = _build_where(query)

    if query.random:
        subquery = f"SELECT id FROM notes {where} ORDER BY RANDOM() LIMIT {query.limit}"
        where_query = f"WHERE id IN ({subquery})"
        offset = 0
    else:
        where_query = where
        offset = query.skip

    return f"""
        SELECT {projection}
        FROM notes
        {where_query}
        ORDER BY notes.attrs->'created' DESC
        LIMIT {query.limit} OFFSET {offset}
    """


def delete_notes(username: str, nids: List[str]) -> str:
    where = _build_where(Query(username=username, query_terms=Nid(nids=nids)))
    return f"DELETE FROM notes {where} RETURNING nid"


def get_related_total(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        SELECT count(*) AS cnt
        FROM notes
        {where_query}
    """


def get_related_tags(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        SELECT
            unnest(
                CASE tags
                WHEN ARRAY[]::text[] THEN ARRAY['#ALL#', '#EMPTY#']
                ELSE array_append(tags, '#ALL#')
                END
            ) AS name,
            count(*) AS cnt
        FROM notes
        {where_query}
        GROUP BY name
    """


def get_related_sizes(query: Query) -> str:
    # FIXME: fetching of the text_size consumes almost 90% of this query runtime
    where_query = _build_where(query)
    return f"""
        WITH filtered AS (
            SELECT id AS note_id FROM notes {where_query}
        ), note_stats AS (
            SELECT
                count(*) AS notes_count,
                -- sum(length(notes.text)) AS text_size,
                0 AS text_size,
                sum(array_length(notes.tags, 1)) AS tags_counts,
                sum(array_length(akeys(notes.attrs), 1)) AS attrs_counts
            FROM notes JOIN filtered ON (filtered.note_id = notes.id)
        ), file_stats AS (
            SELECT
                count(*) AS files_counts,
                sum(files.size) AS files_size
            FROM files JOIN filtered ON (filtered.note_id = files.note_id)
        )
        SELECT * FROM note_stats CROSS JOIN file_stats
    """


def get_related_attrs(query: Query) -> str:
    where_query = _build_where(query)
    return f"""
        SELECT attr_name AS name, sum(attr_cnt) AS cnt
        FROM (
            SELECT skeys(attrs) AS attr_name, 1 AS attr_cnt
            FROM notes
            {where_query}
        ) AS foo
        GROUP BY name
    """


def get_related_dates(query: Query, date_type: FetchDatesOptions) -> str:
    where_query = _build_where(query)
    offset = FETCH_DATES_OFFSET[date_type]
    return f"""
        SELECT
            substr(notes.attrs->'created', 1, {offset}) AS name,
            count(*) AS cnt
        FROM notes
        {where_query}
        GROUP BY name
    """
