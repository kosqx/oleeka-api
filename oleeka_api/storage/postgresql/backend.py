import asyncio
from contextlib import asynccontextmanager
from urllib.parse import urlparse
from typing import Any, Dict, List, Optional, Set, Union

import asyncpg  # type: ignore

from oleeka_api.logic.base import OleekaObjectMissingException
from oleeka_api.models import File, Note, UserAccount, RelatedStats, SizeStats
from oleeka_api.models import PaginatedQuery, SelectFields, RelatedRequest
from oleeka_api.storage.base import StorageBackend
from oleeka_api.storage.postgresql import build_query
from oleeka_api.storage.postgresql import encoding
from oleeka_api.utils.iterutils import build_dict_of_lists


def parse_postgresql_url(backend_url: str) -> Dict[str, Union[str, int]]:
    parsed = urlparse(backend_url)
    return {
        "host": parsed.hostname or "localhost",
        "port": parsed.port or 5432,
        "user": parsed.username or "",
        "password": parsed.password or "",
        "database": parsed.path.lstrip("/"),
    }


class PostgresqlStorageBackend(StorageBackend):
    connection_pool: Optional[asyncpg.pool.Pool] = None
    connection_pool_lock = asyncio.Lock()

    def __init__(self, connection):
        self._conn = connection

    @classmethod
    async def _init_connection_pool(cls):
        async with cls.connection_pool_lock:
            if cls.connection_pool is None:
                backend_url = cls.get_backend_url()
                cls.connection_pool = await asyncpg.create_pool(
                    **parse_postgresql_url(backend_url),
                    server_settings={"application_name": "oleeka_api"},
                    command_timeout=30.0,
                    min_size=2,
                    max_size=10,
                )

    @classmethod
    @asynccontextmanager
    async def acquire_backend(cls):
        await cls._init_connection_pool()

        async with cls.connection_pool.acquire() as conn:
            await conn.set_builtin_type_codec(
                typename="hstore", codec_name="pg_contrib.hstore"
            )
            yield cls(conn)

    @classmethod
    async def testing_deinit(cls):
        cls.connection_pool.terminate()
        cls.connection_pool = None

    async def testing_clear(self):
        await self._conn.execute("DELETE FROM files")
        await self._conn.execute("DELETE FROM files_data")
        await self._conn.execute("DELETE FROM notes")
        await self._conn.execute("DELETE FROM users")

    async def _insert(
        self, table: str, fields: Dict[str, Any], *, returning: Optional[str] = None
    ):
        query, values = build_query.insert(table, fields, returning)
        row = await self._conn.fetchrow(query, *values)
        return row[returning] if returning is not None else None

    async def _fetch_files_data(self, file_hashes: List[str]) -> Dict[str, bytes]:
        records = await self._conn.fetch(build_query.get_files_data(file_hashes))
        return {record["hash"]: record["data"] for record in records}

    async def _fetch_files(
        self, note_ids: List[int], select: SelectFields
    ) -> Dict[int, List[File]]:
        if not note_ids or not select.files:
            return {}

        records = await self._conn.fetch(build_query.get_files(note_ids))
        if not records:
            return {}

        if select.files_data:
            files_data = await self._fetch_files_data([r["hash"] for r in records])
        else:
            files_data = {}

        return build_dict_of_lists(
            (record["note_id"], encoding.decode_file(record, files_data))
            for record in records
        )

    async def get_notes(self, query: PaginatedQuery) -> List[Note]:
        values = await self._conn.fetch(build_query.get_notes(query))
        files_mapping = await self._fetch_files(
            [record["id"] for record in values], query.select_fields
        )
        return [encoding.decode_note(record, files_mapping) for record in values]

    async def _save_note(self, username: str, note: Note):
        record = encoding.encode_note(note, username)
        await self._conn.fetchrow("DELETE FROM notes WHERE nid = $1", note.nid)
        note_id = await self._insert("notes", record, returning="id")
        await self._save_note_files(note, note_id=note_id)

    async def save_notes(self, username: str, notes: List[Note]):
        for note in notes:
            await self._save_note(username, note)

    async def _save_note_files(self, note: Note, note_id: int):
        if not note.files:
            return

        sql = build_query.get_existing_file_hashes([file.hash for file in note.files])
        existing_file_data = await self._conn.fetch(sql)
        existing_file_hashes = {file["hash"] for file in existing_file_data}

        for file in note.files:
            if file.hash not in existing_file_hashes:
                await self._insert("files_data", dict(hash=file.hash, data=file.data))
            await self._insert("files", encoding.encode_file(file, note_id))

    async def delete_notes(self, username: str, nids: List[str]) -> Set[str]:
        records = await self._conn.fetch(build_query.delete_notes(username, nids))
        return {record["nid"] for record in records}

    async def get_file(
        self, username: str, nid: str, fid: str, *, fetch_data: bool = True
    ) -> File:
        query = build_query.get_file(username, nid, fid, fetch_data)
        records = await self._conn.fetch(query)

        if len(records) == 1:
            return encoding.decode_file(records[0], {})
        else:
            raise OleekaObjectMissingException("file not found", fid)

    async def _fetch_counts(self, sql: str) -> Dict[str, int]:
        records = await self._conn.fetch(sql)
        return {record["name"]: record["cnt"] for record in records}

    async def get_related(self, request: RelatedRequest) -> RelatedStats:
        total = None
        tags = None
        empty = None
        attrs = None
        sizes = None
        dates = None

        if request.fetch_tags:
            tags = await self._fetch_counts(build_query.get_related_tags(request.query))
            total = tags.pop("#ALL#", 0)
            empty = tags.pop("#EMPTY#", 0)

        if request.fetch_attrs:
            attrs = await self._fetch_counts(
                build_query.get_related_attrs(request.query)
            )
            total = attrs.get("created", 0)

        if request.fetch_sizes:
            record = await self._conn.fetchrow(
                build_query.get_related_sizes(request.query)
            )
            sizes = SizeStats.from_dict(record)
            total = record.get("notes_count") or 0

        if request.fetch_dates:
            dates = await self._fetch_counts(
                build_query.get_related_dates(request.query, request.fetch_dates)
            )
            total = sum(dates.values())

        if request.fetch_total and total is None:
            record = await self._conn.fetchrow(
                build_query.get_related_total(request.query)
            )
            total = record.get("cnt", 0)

        return RelatedStats(
            total=total,
            tags_empty=empty,
            tags=tags,
            attrs=attrs,
            sizes=sizes,
            dates=dates,
        )

    async def get_user(self, username: str) -> Optional[UserAccount]:
        sql = "SELECT * FROM users WHERE username = $1"
        for record in await self._conn.fetch(sql, username):
            return encoding.decode_user(record)
        return None

    async def save_user(self, user: UserAccount):
        await self._conn.fetchrow(
            "DELETE FROM users WHERE username = $1", user.username
        )
        await self._insert("users", encoding.encode_user(user))
