import json
from typing import Any, Dict, List

from oleeka_api.models import File, Note, UserAccount


def encode_note(note: Note, username: str) -> Dict[str, Any]:
    return {
        "username": username,
        "nid": note.nid,
        "text": note.text,
        "tags": note.tags,
        "attrs": note.attrs,
    }


def decode_note(record: Dict[str, Any], files_mapping: Dict[int, List[File]]) -> Note:
    return Note(
        nid=record["nid"],
        text=record.get("text", ""),
        tags=record.get("tags", []),
        attrs=record.get("attrs", {}),
        files=files_mapping.get(record["id"], []),
    )


def encode_file(file: File, note_id: int) -> Dict[str, Any]:
    return {
        "note_id": note_id,
        "fid": file.fid,
        "size": file.size,
        "hash": file.hash,
        "name": file.name,
        "mime": file.mime,
        "meta": json.dumps(file.meta),
    }


def decode_file(record: Dict[str, Any], files_data: Dict[str, bytes]) -> File:
    return File(
        fid=record["fid"],
        name=record["name"],
        hash=record["hash"],
        mime=record["mime"],
        size=record["size"],
        meta=json.loads(record["meta"]),
        data=record["data"] if "data" in record else files_data.get(record["hash"]),
    )


def encode_user(user: UserAccount) -> Dict[str, str]:
    return {
        "username": user.username,
        "email": user.email,
        "auth": json.dumps(user.auth),
        "settings": json.dumps(user.settings),
        "cache": json.dumps(user.cache),
    }


def decode_user(record: Dict[str, Any]) -> UserAccount:
    return UserAccount(
        username=record["username"],
        email=record["email"],
        auth=json.loads(record["auth"]),
        settings=json.loads(record["settings"]),
        cache=json.loads(record["cache"]),
    )
