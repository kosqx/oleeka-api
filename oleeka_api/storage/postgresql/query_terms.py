import functools
from typing import cast, ClassVar, Dict, Type

from asyncpg.utils import _quote_literal  # type: ignore

from oleeka_api.query.terms import Term, All, Nid, Text, Tag, Attr, Mark, Not, And, Or
from oleeka_api.utils.text import is_number


def sqlquote(value) -> str:
    return cast(str, _quote_literal(value))


def sqlquote_like(value, prefix: bool, suffix: bool) -> str:
    return sqlquote("%" * prefix + value + "%" * suffix)


class TermQuery(Term):
    def to_sql(self) -> str:
        return "(0 = 1)"

    @classmethod
    @functools.lru_cache(maxsize=None)
    def resolve(cls) -> Dict[Type[Term], Type["TermQuery"]]:
        def get_term_class(klass: Type["TermQuery"]) -> Type[Term]:
            return [
                cast(Type["TermQuery"], subclass)
                for subclass in klass.mro()
                if TermQuery.__module__ != subclass.__module__
            ][0]

        result = {get_term_class(klass): klass for klass in cls.__subclasses__()}
        result[Term] = cls
        return result

    @classmethod
    def run_to_sql(cls, term: Term) -> str:
        mapping = TermQuery.resolve()
        return mapping[term.__class__].to_sql(cast(TermQuery, term))


class AllQuery(All, TermQuery):
    def to_sql(self) -> str:
        return "(1 = 1)"


class TextQuery(Text, TermQuery):
    def to_sql(self) -> str:
        return "(notes.text LIKE {})".format(sqlquote_like(self.value, True, True))


class NidQuery(Nid, TermQuery):
    def to_sql(self) -> str:
        if self.nids:
            nids = ", ".join([sqlquote(nid) for nid in self.nids])
            return "(notes.nid IN ({}))".format(nids)
        else:
            return "(1 = 0)"


class TagQuery(Tag, TermQuery):
    # FIXME: Check behaviour on new version of PostgreSQL
    #   On PostgreSQL 9.6 the query
    #     EXISTS (SELECT 1 FROM (SELECT unnest(notes.tags)) x(tag) WHERE x.tag LIKE 'db:%')
    #   runs 20% faster than
    #     EXISTS (SELECT 1 FROM unnest(notes.tags) AS tag WHERE tag LIKE 'db:%')

    def to_sql(self) -> str:
        if self.mode == "exact":
            return "(notes.tags @> ARRAY[{}]::text[])".format(sqlquote(self.tag))
        else:
            prefix, suffix, _ = self.MODES[self.mode]
            return "EXISTS (SELECT 1 FROM (SELECT unnest(notes.tags)) x(tag) WHERE x.tag LIKE {})".format(
                sqlquote_like(self.tag, bool(prefix), bool(suffix))
            )


class AttrQuery(Attr, TermQuery):
    OPERATOR_TO_SQL: ClassVar[Dict[str, str]] = {
        "?": "({attr} IS NOT NULL)",
        "=": "({attr} ILIKE '%{value}%')",
        "==": "({attr} ILIKE '{value}')",
        "===": "({attr} LIKE '{value}')",
        "^=": "({attr} ILIKE '{value}%')",
        "^==": "({attr} LIKE '{value}%')",
        "$=": "({attr} ILIKE '%{value}')",
        "$==": "({attr} LIKE '%{value}')",
        "*=": "({attr} ILIKE '%{value}%')",
        "*==": "({attr} LIKE '%{value}%')",
        "+=": "({attr} ~* '.*\\y{value}\\y.*')",
        "+==": "({attr} ~ '.*\\y{value}\\y.*')",
        "<=": "(UPPER({attr}) COLLATE \"C\" <= UPPER('{value}'))",
        "<==": "({attr} COLLATE \"C\" <= '{value}')",
        ">=": "(UPPER({attr}) COLLATE \"C\" >= UPPER('{value}'))",
        ">==": "({attr} COLLATE \"C\" >= '{value}')",
        "<<=": "COALESCE(try_cast_float({attr}) <= '{value}'::float8, FALSE)",
        ">>=": "COALESCE(try_cast_float({attr}) >= '{value}'::float8, FALSE)",
    }

    def to_sql(self) -> str:
        if self.operator in ("<<=", ">>=") and not is_number(self.value):
            return "(1 = 2)"
        return AttrQuery.OPERATOR_TO_SQL[self.operator].format(
            attr="notes.attrs->{}".format(sqlquote(self.name)),
            value=sqlquote(self.value)[1:-1],
        )


class MarkQuery(Mark, TermQuery):
    FEATURE_TO_SQL: ClassVar[Dict[str, str]] = {
        "tags_count": "cardinality(notes.tags)",
        "attrs_count": "cardinality(akeys(notes.attrs))",
        "files_count": "(SELECT count(*) FROM files WHERE notes.id = note_id)",
        "files_size": "(SELECT coalesce(sum(size), 0) FROM files WHERE notes.id = note_id)",
        "modified_after": (
            "extract(epoch FROM "
            "(notes.attrs->'modified')::timestamp - (notes.attrs->'created')::timestamp"
            ")"
        ),
        "date_day_of_week": "extract(isodow FROM (notes.attrs->'created')::timestamp)",
        "date_day_of_year": "extract(doy FROM (notes.attrs->'created')::timestamp)",
        "date_week_of_year": "extract(week FROM (notes.attrs->'created')::timestamp)",
    }

    def to_sql(self) -> str:
        field = MarkQuery.FEATURE_TO_SQL[self.feature]
        op = {"==": "="}.get(self.operator, self.operator)
        return f"({field} {op} {int(self.value)})"


class NotQuery(Not, TermQuery):
    def to_sql(self) -> str:
        sql = TermQuery.run_to_sql(self.condition)
        return "(NOT {})".format(sql)


class AndQuery(And, TermQuery):
    def to_sql(self) -> str:
        parts = [TermQuery.run_to_sql(cond) for cond in self.conditions]
        return "({})".format(" AND ".join(parts))


class OrQuery(Or, TermQuery):
    def to_sql(self) -> str:
        parts = [TermQuery.run_to_sql(cond) for cond in self.conditions]
        return "({})".format(" OR ".join(parts))


def to_sql(term: Term) -> str:
    return TermQuery.run_to_sql(term)
