import hashlib
from dataclasses import dataclass, asdict, field, replace
from typing import Any, Dict, List, Literal, Optional, Union, Set, Type, TypeVar

from oleeka_api.logic.note import generate_fid, generate_nid, generate_datetime_str
from oleeka_api.query.terms import Term
from oleeka_api.utils.mime import guess_mime
from oleeka_api.utils.typed_struct import TypedStruct


@dataclass(frozen=True)
class SelectFields:
    text: bool = True
    tags: bool = True
    attrs: Union[bool, Set[str]] = True
    files: bool = True
    files_data: bool = False

    @classmethod
    def only(
        cls,
        text: bool = False,
        tags: bool = False,
        attrs: Union[bool, Set[str]] = False,
        files: bool = False,
        files_data: bool = False,
    ):
        return cls(
            text=text, tags=tags, attrs=attrs, files=files, files_data=files_data
        )

    @classmethod
    def only_nid(cls):
        return cls(text=False, tags=False, attrs=False, files=False, files_data=False)

    @classmethod
    def no_files(cls):
        return cls(text=True, tags=True, attrs=True, files=False, files_data=False)

    @classmethod
    def everything(cls):
        return cls(text=True, tags=True, attrs=True, files=True, files_data=True)

    def is_everything(self) -> bool:
        return all([self.text, self.tags, self.attrs, self.files, self.files_data])

    def _format_attrs(self) -> str:
        if isinstance(self.attrs, set):
            return ",".join(f"attrs.{attr}" for attr in sorted(self.attrs))
        else:
            return "attrs" if self.attrs else ""

    def format(self) -> str:
        fields = [
            "nid",
            "text" if self.text else "",
            "tags" if self.tags else "",
            self._format_attrs(),
            "files" if self.files else "",
            "files_data" if self.files_data else "",
        ]
        return ",".join(filter(bool, fields))

    @classmethod
    def parse(cls, spec: str):
        if spec.strip() == "":
            return cls()

        fields, attrs = set(), set()
        for item in map(str.strip, spec.split(",")):
            if item.startswith("attrs."):
                attrs.add(item[6:])
            else:
                fields.add(item)
        return cls(
            text=("text" in fields),
            tags=("tags" in fields),
            attrs=(attrs if attrs else "attrs" in fields),
            files=("files" in fields),
            files_data=("files_data" in fields),
        )


@dataclass(frozen=True)
class Query:
    username: Optional[str] = None
    query: Optional[str] = None
    query_terms: Term = field(default_factory=Term)

    def replace(self, **changes) -> "Query":
        return replace(self, **changes)


@dataclass(frozen=True)
class PaginatedQuery(Query):
    skip: int = 0
    limit: int = 10
    random: bool = False
    select_fields: SelectFields = field(default_factory=SelectFields)

    def replace(self, **changes) -> "PaginatedQuery":
        return replace(self, **changes)


@dataclass(frozen=True)
class File:
    fid: str
    name: str
    hash: str
    mime: str
    size: int
    meta: Dict[str, str]
    data: Optional[bytes] = None

    def to_dict(self):
        return asdict(self)

    def replace(self, **changes) -> "File":
        return replace(self, **changes)

    def replace_data(self, new_data: Optional[bytes]):
        if new_data is not None:
            return replace(
                self,
                hash=hashlib.sha1(new_data).hexdigest(),
                size=len(new_data),
                data=new_data,
            )

    def select_fields(self, select: SelectFields) -> "File":
        return self if select.files_data else self.replace(data=None)

    @classmethod
    def from_data(
        cls,
        name: str,
        data: Union[str, bytes],
        *,
        fid: Optional[str] = None,
        mime: Optional[str] = None,
        meta: Optional[Dict[str, str]] = None,
    ) -> "File":
        binary_data = data if isinstance(data, bytes) else data.encode("utf8")
        return cls(
            fid=fid or generate_fid(),
            name=name,
            hash=hashlib.sha1(binary_data).hexdigest(),
            mime=guess_mime(filename=name, filedata=data) if mime is None else mime,
            size=len(binary_data),
            meta={} if meta is None else meta,
            data=binary_data,
        )


@dataclass(frozen=True)
class Note:
    nid: str = ""
    text: str = ""
    tags: List[str] = field(default_factory=list)
    attrs: Dict[str, str] = field(default_factory=dict)
    files: List[File] = field(default_factory=list)

    def to_dict(self):
        return asdict(self)

    def replace(self, **changes) -> "Note":
        return replace(self, **changes)

    def get_file(self, fid: Optional[str] = None, name: Optional[str] = None) -> File:
        files = [
            file
            for file in self.files
            if (fid is None or file.fid == fid) and (name is None or file.name == name)
        ]
        return files[0]

    def _select_attrs(self, select: SelectFields) -> Dict[str, str]:
        if isinstance(select.attrs, set):
            return {key: self.attrs[key] for key in select.attrs if key in self.attrs}
        else:
            return self.attrs if select.attrs else {}

    def select_fields(self, select: SelectFields) -> "Note":
        if select.is_everything():
            return self
        return Note(
            nid=self.nid,
            text=self.text if select.text else "",
            tags=self.tags if select.tags else [],
            attrs=self._select_attrs(select),
            files=[f.select_fields(select) for f in self.files] if select.files else [],
        )

    def normalize(self) -> "Note":
        now = generate_datetime_str()
        return Note(
            nid=self.nid or generate_nid(),
            text=self.text,
            tags=self.tags,
            attrs={
                "created": now,
                **self.attrs,
                "modified": now,
            },
            files=self.files,
        )


T = TypeVar("T")


@dataclass(frozen=True)
class UserAccount:
    username: str
    email: str = ""
    auth: Dict[str, Any] = field(default_factory=dict)
    settings: Dict[str, Any] = field(default_factory=dict)
    cache: Dict[str, Any] = field(default_factory=dict)

    def replace(self, **changes):
        return replace(self, **changes)

    def get_setting(self, typ: Type[T], key: str) -> Optional[T]:
        if key not in self.settings:
            return None
        try:
            return TypedStruct(typ).load(self.settings[key])
        except (TypeError, ValueError):
            return None


FetchDatesOptions = Literal[None, "year", "month", "day", "hour"]
FETCH_DATES_OFFSET: Dict[FetchDatesOptions, int] = {
    "year": 4,
    "month": 7,
    "day": 10,
    "hour": 13,
}


@dataclass(frozen=True)
class RelatedRequest:
    query: Query
    fetch_total: bool = False
    fetch_tags: bool = False
    fetch_attrs: bool = False
    fetch_sizes: bool = False
    fetch_dates: FetchDatesOptions = None

    def should_fetch_total(self) -> bool:
        """
        Because total can be computed from any other stat,
        then we are giving it for free, even if not requested
        """
        return (
            self.fetch_total
            or self.fetch_tags
            or self.fetch_attrs
            or self.fetch_sizes
            or self.fetch_dates is not None
        )


@dataclass(frozen=True)
class SizeStats:
    text_size: int = 0
    tags_counts: int = 0
    attrs_counts: int = 0
    files_counts: int = 0
    files_size: int = 0

    @classmethod
    def from_dict(cls, sizes: Dict) -> "SizeStats":
        return SizeStats(
            text_size=sizes.get("text_size") or 0,
            tags_counts=sizes.get("tags_counts") or 0,
            attrs_counts=sizes.get("attrs_counts") or 0,
            files_counts=sizes.get("files_counts") or 0,
            files_size=sizes.get("files_size") or 0,
        )

    @classmethod
    def from_note(cls, note: Note) -> "SizeStats":
        return SizeStats(
            # text_size=len(note.text),
            text_size=0,
            tags_counts=len(note.tags),
            attrs_counts=len(note.attrs),
            files_counts=len(note.files),
            files_size=sum(file.size for file in note.files),
        )

    def __add__(self, other: "SizeStats") -> "SizeStats":
        return SizeStats(
            text_size=self.text_size + other.text_size,
            tags_counts=self.tags_counts + other.tags_counts,
            attrs_counts=self.attrs_counts + other.attrs_counts,
            files_counts=self.files_counts + other.files_counts,
            files_size=self.files_size + other.files_size,
        )


@dataclass(frozen=True)
class RelatedStats:
    total: Optional[int] = None
    tags_empty: Optional[int] = None
    tags: Optional[Dict[str, int]] = None
    attrs: Optional[Dict[str, int]] = None
    dates: Optional[Dict[str, int]] = None
    sizes: Optional[SizeStats] = None
