from typing import Dict, Iterable, List, Tuple, TypeVar


KT = TypeVar("KT")
VT = TypeVar("VT")


def build_dict_of_lists(items: Iterable[Tuple[KT, VT]]) -> Dict[KT, List[VT]]:
    """
    >>> build_dict_of_lists([(1, 2), (3, 4), (1, 6)])
    {1: [2, 6], 3: [4]}
    """
    result: Dict[KT, List[VT]] = {}
    for key, value in items:
        if key not in result:
            result[key] = []
        result[key].append(value)
    return result


def chunks(iterator: Iterable, size: int) -> Iterable[List]:
    chunk = []
    for i in iterator:
        chunk.append(i)
        if len(chunk) >= size:
            yield chunk
            chunk = []
    if chunk:
        yield chunk
