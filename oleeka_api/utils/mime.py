import mimetypes
from typing import Union


EXTENSIONS = {
    "jsx": "application/javascript",
    "markdown": "text/markdown",
    "md": "text/markdown",
    "mdown": "text/markdown",
    "ts": "application/x-typescript",
    "tsx": "application/x-typescript",
}


def is_text(data: bytes) -> bool:
    if any(ch < ord(" ") and ch not in list(b"\n\r\t\b") for ch in data):
        return False
    try:
        data.decode("utf-8")
        return True
    except UnicodeDecodeError:
        return False


def guess_mime(filename: str, filedata: Union[None, str, bytes] = None) -> str:
    extension = filename.rsplit(".")[-1]
    if extension in EXTENSIONS:
        return EXTENSIONS[extension]

    mime = mimetypes.guess_type(filename, strict=False)[0]
    if mime is not None:
        return mime

    if filedata is None:
        return "application/octet-stream"
    elif isinstance(filedata, str):
        return "text/plain"
    elif is_text(filedata):
        return "text/plain"
    else:
        return "application/octet-stream"
