import dataclasses
import datetime
import enum
import typing
from inspect import isclass


NoneType = type(None)


def _typed_load(typ, data):  # noqa: C901
    if typ == NoneType and data is None:
        return None
    if typ == bool and isinstance(data, (bool, int)):
        return bool(data)
    if typ == int and isinstance(data, (int, float)):
        return int(data)
    if typ == float and isinstance(data, (int, float)):
        return float(data)
    if typ == str and isinstance(data, str):
        return str(data)
    if typ == datetime.datetime and isinstance(data, str):
        return datetime.datetime.fromisoformat(data.rstrip("Z"))

    if dataclasses.is_dataclass(typ) and isinstance(data, dict):
        dataclass_type, dataclass_data = typ, data
        fields = {
            field.name: _typed_load(field.type, dataclass_data[field.name])
            for field in dataclasses.fields(dataclass_type)
            if field.name in dataclass_data
        }
        return dataclass_type(**fields)

    if isclass(typ) and issubclass(typ, enum.Enum):
        return typ(data)

    origin = typing.get_origin(typ)
    args = typing.get_args(typ)
    if origin == list and isinstance(data, list):
        return [_typed_load(args[0], i) for i in data]

    if origin == dict and isinstance(data, dict):
        return {
            _typed_load(args[0], key): _typed_load(args[0], val)
            for key, val in data.items()
        }

    if origin == tuple and isinstance(data, (list, tuple)):
        return tuple(
            _typed_load(item_type, item) for item_type, item in zip(args, data)
        )

    if origin == typing.Union:
        for arg in args:
            try:
                return _typed_load(arg, data)
            except TypeError:
                pass
        raise TypeError("no type in Union matched")

    if origin == typing.Literal:
        if data in args:
            return data

    raise TypeError("")


T = typing.TypeVar("T")


class TypedStruct(typing.Generic[T]):
    def __init__(self, typ: typing.Type[T]):
        self.typ = typ

    def load(self, data: typing.Any) -> T:
        return typing.cast(T, _typed_load(self.typ, data))
