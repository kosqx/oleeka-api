import random
import string
from typing import cast, Union


HEXDIGITS = "0123456789abcdef"


def get_random_string(
    length: int, *, digits: bool = True, lowercase: bool = True, uppercase: bool = True
) -> str:
    chars = "".join(
        [
            string.digits * digits,
            string.ascii_lowercase * lowercase,
            string.ascii_uppercase * uppercase,
        ]
    )
    return "".join(random.choice(chars) for _ in range(length))


def get_random_hex(length: int) -> str:
    return "".join(random.choice(HEXDIGITS) for _ in range(length))


def cast_number(value: str) -> Union[None, int, float]:
    for cast_type in (int, float):
        try:
            return cast(Union[int, float], cast_type(value))
        except ValueError:
            pass
    return None


def is_number(value: str) -> bool:
    return cast_number(value) is not None
