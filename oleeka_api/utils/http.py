from typing import Dict
from dataclasses import dataclass
from string import ascii_letters, digits


def get_download_headers(filename: str) -> Dict[str, str]:
    return {"Content-Disposition": f'attachment; filename="{filename}"'}


# RFC7230: https://httpwg.org/specs/rfc7230.html#rule.token.separators
HTTP_TOKEN_CHARS = frozenset("!#$%&'*+-.^_`|~" + digits + ascii_letters)


def is_http_token(text: str) -> bool:
    return isinstance(text, str) and len(text) > 0 and set(text) <= HTTP_TOKEN_CHARS


@dataclass(frozen=True)
class ServerTimingMetric:
    name: str
    duration: float  # in seconds
    description: str = ""

    def format(self) -> str:
        assert is_http_token(self.name)
        assert isinstance(self.duration, (int, float)) and self.duration >= 0
        duration_ms = self.duration * 1000.0
        parts = [self.name, f"dur={duration_ms:.3f}"]
        if self.description:
            if is_http_token(self.description):
                parts.append(f"desc={self.description}")
            else:
                # FIXME: assumes that description don't contain problematic characters
                parts.append(f'desc="{self.description}"')

        return ";".join(parts)


def format_server_timing(*server_timings: ServerTimingMetric) -> str:
    return ", ".join(timing.format() for timing in server_timings)
