import time
from typing import cast

from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import Response
from starlette.requests import Request

from oleeka_api.utils.http import ServerTimingMetric, format_server_timing


class ServerTimingMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next) -> Response:
        wall_start = time.time()
        cpu_start = time.process_time()
        response = await call_next(request)
        cpu_end = time.process_time()
        wall_end = time.time()

        response.headers["Server-Timing"] = format_server_timing(
            ServerTimingMetric("cpu", cpu_end - cpu_start),
            ServerTimingMetric("wall", wall_end - wall_start),
        )
        return cast(Response, response)
