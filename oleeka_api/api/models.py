import base64
from typing import cast, Dict, List, Literal, Optional, Tuple, Union

from pydantic import BaseModel

from oleeka_api.logic.note import generate_fid, validate_fid
from oleeka_api.models import File, Note, RelatedStats, SizeStats
from oleeka_api.utils.mime import is_text


EXAMPLE_NOTE_MODEL = {
    "nid": "20201020T123456987654R144144",
    "text": ["first line\n", "second line"],
    "tags": ["foo", "bar"],
    "attrs": {"created": "2020-10-20T12:34:56Z", "modified": "2020-10-20T12:34:56Z"},
    "files": [
        {
            "fid": "F20210127T003321222210R330394",
            "name": "content.md",
            "hash": "cef873906c12e04cbdf8b94ce13d735db03d97fe",
            "mime": "text/markdown",
            "size": 20,
            "meta": {},
            "data": [
                "line one\r\n",
                "line two\r\n",
            ],
            "encoding": "text",
        }
    ],
}

EXAMPLE_NOTE_INPUT_MODEL = {
    "text": "first line\nsecond line",
    "tags": ["foo", "bar"],
    "attrs": {},
    "files": [
        {
            "name": "content.md",
            "data": "line one\r\nline two\r\n",
        }
    ],
}


def split_chunks(text: str, length: int = 120) -> List[str]:
    result = []
    for i in range((len(text) + length - 1) // length):
        result.append(text[i * length : (i + 1) * length])
    return result


def split_lines(text: str, length: int = 120) -> List[str]:
    result = []
    for line in text.splitlines(True):
        result.extend(split_chunks(line, length))
    return result


def encode_data(data: Optional[bytes]) -> Tuple[str, Optional[List[str]]]:
    if data is None:
        return "omitted", None
    elif is_text(data):
        data_str = data.decode("utf-8") if isinstance(data, bytes) else data
        return "text", split_lines(data_str)
    else:
        return "base64", split_chunks(base64.b64encode(data).decode("ascii"))


def decode_data(encoding: str, data: Union[None, str, List[str]]) -> Optional[bytes]:
    if encoding == "omitted" or data is None:
        return None

    data_str = data if isinstance(data, str) else "".join(data)
    if encoding == "text":
        return data_str.encode("utf-8")
    if encoding == "base64":
        return base64.b64decode(data_str)

    return None


class FileModel(BaseModel):
    fid: str
    name: str
    hash: str
    mime: str
    size: int
    meta: Dict[str, str]
    data: Optional[List[str]] = None
    encoding: str

    class Config:
        title = "File"

    @classmethod
    def from_file(cls, file: File):
        encoding, data = encode_data(file.data)
        return cls(
            fid=file.fid,
            name=file.name,
            hash=file.hash,
            mime=file.mime,
            size=file.size,
            meta=file.meta,
            data=data,
            encoding=encoding,
        )


class NoteModel(BaseModel):
    nid: str
    text: List[str]
    tags: List[str]
    attrs: Dict[str, str]
    files: List[FileModel]

    class Config:
        title = "Note"
        schema_extra = {"example": EXAMPLE_NOTE_MODEL}

    @classmethod
    def from_note(cls, note: Note):
        return cls(
            nid=note.nid,
            text=split_lines(note.text),
            tags=note.tags,
            attrs=note.attrs,
            files=[FileModel.from_file(file) for file in note.files],
        )


# Input models (used only when saving a note)


class FileInputModel(BaseModel):
    fid: Optional[str]
    name: Optional[str]
    data: Union[None, str, List[str]]
    encoding: str = "text"
    mime: Optional[str] = None
    meta: Optional[Dict[str, str]] = None
    # following fields will be overwritten
    hash: Optional[str] = None
    size: Optional[int] = None
    # special operations
    delete: bool = False

    class Config:
        title = "FileInput"

    def get_decoded_data(self) -> Optional[bytes]:
        return decode_data(self.encoding, self.data)

    def to_file(self) -> File:
        assert self.name
        return File.from_data(
            fid=self.fid if validate_fid(self.fid) else generate_fid(),
            name=self.name,
            data=cast(bytes, self.get_decoded_data()),
            mime=self.mime,
            meta=self.meta,
        )


class NoteInputModel(BaseModel):
    nid: Optional[str] = None
    text: Union[str, List[str]] = ""
    tags: Optional[List[str]] = None
    attrs: Optional[Dict[str, str]] = None
    files: Optional[List[FileInputModel]] = None
    input: str = ""
    restore: bool = False

    class Config:
        title = "NoteInput"
        schema_extra = {"example": EXAMPLE_NOTE_INPUT_MODEL}


class NotesResponse(BaseModel):
    notes: List[NoteModel]
    total: Optional[int] = None
    random: Optional[bool] = None


class NoteRequest(BaseModel):
    note: NoteInputModel
    notes: List[NoteInputModel] = []

    def all_notes(self) -> List[NoteInputModel]:
        return [self.note] + self.notes


class NoteResponse(BaseModel):
    note: NoteModel


class FileResponse(BaseModel):
    file: FileModel


class AuthRequest(BaseModel):
    username: str
    password: str


class AuthResponse(BaseModel):
    access_token: str


EXAMPLE_RELATED_STATS_RESPONSE = {
    "total": 3,
    "tags_empty": 1,
    "tags": {"hello": 2, "world": 1},
    "attrs": {"created": 3, "modified": 3, "name": 2, "link": 1},
    "sizes": {
        "text_size": 31,
        "tags_counts": 3,
        "attrs_counts": 10,
        "files_counts": 1,
        "files_size": 19,
    },
    "dates": {"2021-10": 2, "2021-11": 1},
}


class SizeStatsResponse(BaseModel):
    text_size: int
    tags_counts: int
    attrs_counts: int
    files_counts: int
    files_size: int

    class Config:
        schema_extra = {"example": EXAMPLE_RELATED_STATS_RESPONSE}

    @classmethod
    def from_related_sizes(cls, sizes: Optional[SizeStats]):
        if sizes is None:
            return None
        return cls(
            text_size=sizes.text_size,
            tags_counts=sizes.tags_counts,
            attrs_counts=sizes.attrs_counts,
            files_counts=sizes.files_counts,
            files_size=sizes.files_size,
        )


class RelatedStatsResponse(BaseModel):
    total: Optional[int]
    tags_empty: Optional[int]
    tags: Optional[Dict[str, int]]
    attrs: Optional[Dict[str, int]]
    sizes: Optional[SizeStatsResponse]
    dates: Optional[Dict[str, int]]

    class Config:
        schema_extra = {"example": EXAMPLE_RELATED_STATS_RESPONSE}

    @classmethod
    def from_related_stats(cls, related: RelatedStats):
        return cls(
            total=related.total,
            tags_empty=related.tags_empty,
            tags=related.tags,
            attrs=related.attrs,
            sizes=SizeStatsResponse.from_related_sizes(related.sizes),
            dates=related.dates,
        )


EXAMPLE_SUGGESTIONS_RESPONSE = {
    "suggestions": [
        {"name": "foo", "count": 12},
        {"name": ":summer", "only": "query", "help": "Created in summer"},
    ]
}


class SuggestionModel(BaseModel):
    name: str
    only: Optional[Literal[None, "edit", "query"]]
    count: Optional[int]
    help: Optional[str]

    @classmethod
    def init(
        cls,
        name: str,
        only: Optional[Literal["edit", "query"]] = None,
        count: Optional[int] = None,
        help: Optional[str] = None,
    ):
        """Creates the instance but omits fields set to None"""
        items = [("name", name), ("only", only), ("count", count), ("help", help)]
        return cls(**{key: value for key, value in items if value is not None})


class SuggestionsResponse(BaseModel):
    suggestions: List[SuggestionModel] = []

    class Config:
        schema_extra = {"example": EXAMPLE_SUGGESTIONS_RESPONSE}


class ServiceInfoResponse(BaseModel):
    service: str
    version: str
    isotime: str
