import hashlib
import os
from datetime import datetime, timedelta
from typing import Any, Dict, Optional, cast

from jose import jwt  # type: ignore

from oleeka_api.utils.text import get_random_string, get_random_hex


# if envvar will be missing then ephemeral key will be generated (lasting till restart)
JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY", get_random_hex(length=64))
JWT_ALGORITHM = "HS256"


class Sha1Password:
    @classmethod
    def generate(cls, password: str) -> str:
        salt = get_random_hex(length=8)
        digest = hashlib.sha1((salt + password).encode("utf-8")).hexdigest()
        return "$".join(["sha1", salt, digest])

    @classmethod
    def check(cls, password_hash: str, password: str) -> bool:
        if not password_hash.startswith("sha1$"):
            return False
        parts = password_hash.split("$")
        if len(parts) != 3:
            return False
        alg, salt, digest = parts
        return digest == hashlib.sha1((salt + password).encode("utf-8")).hexdigest()


def check_auth(user_auth, password):
    password_checker = Sha1Password
    for i in user_auth.get("auths", []):
        if password_checker.check(i.get("password", ""), password):
            return True
    return False


def create_access_token(data: Dict[str, Any], duration: int = 3600):
    to_encode = data.copy()
    to_encode.update({"exp": datetime.utcnow() + timedelta(seconds=duration)})
    return jwt.encode(to_encode, JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)


def validate_access_token(token: str) -> Dict[str, Any]:
    token_dict = jwt.decode(token, JWT_SECRET_KEY, algorithms=[JWT_ALGORITHM])
    return cast(Dict[str, Any], token_dict)


def check_refresh_token(user_auth, token: Optional[str]):
    if not token:
        return False
    for i in user_auth.get("auths", []):
        if i.get("token", "") == token:
            return True
    return False


def create_refresh_token() -> str:
    return get_random_string(16)


def store_refresh_token(user_auth, token: str):
    if "auths" not in user_auth:
        user_auth["auths"] = []
    user_auth["auths"].append({"token": token})
    return user_auth
