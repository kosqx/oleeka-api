from oleeka_api.models import File, Note, UserAccount


FILE_A = File.from_data("content.md", "# Title\n\nParagraph\n", fid="F1")
FILE_A_NO_DATA = FILE_A.replace(data=None)
USER_1 = UserAccount(username="testuser")
NOTE_1 = Note(
    nid="20201113T175543290880R684769",
    text="first note",
    tags=["hello"],
    attrs={
        "created": "2020-11-13T17:55:43Z",
        "modified": "2020-11-13T17:55:43Z",
        "name": "abc foo123 xyz",
        "rating": "2",
    },
)
NOTE_2 = Note(
    nid="20201113T175554625048R173053",
    text="second note",
    tags=["hello", "world"],
    attrs={
        "created": "2020-11-13T17:55:54Z",
        "modified": "2020-11-13T17:55:54Z",
        "name": "ABC FOO123 XYZ",
    },
    files=[FILE_A],
)
NOTE_2_NO_DATA = NOTE_2.replace(files=[FILE_A_NO_DATA])
NOTE_3 = Note(
    nid="20201113T182454852122R334524",
    text="third note",
    tags=[],
    attrs={
        "created": "2022-01-02T07:24:54Z",
        "modified": "2022-01-02T09:24:54Z",
        "link": "https://www.oleeka.com/join",
    },
)
NOTE_1_JSON = {
    "nid": "20201113T175543290880R684769",
    "text": ["first note"],
    "tags": ["hello"],
    "attrs": {
        "created": "2020-11-13T17:55:43Z",
        "modified": "2020-11-13T17:55:43Z",
        "name": "abc foo123 xyz",
        "rating": "2",
    },
    "files": [],
}
NOTE_2_JSON = {
    "nid": "20201113T175554625048R173053",
    "text": ["second note"],
    "tags": ["hello", "world"],
    "attrs": {
        "created": "2020-11-13T17:55:54Z",
        "modified": "2020-11-13T17:55:54Z",
        "name": "ABC FOO123 XYZ",
    },
    "files": [
        {
            "fid": "F1",
            "name": "content.md",
            "hash": "d78e0e124680dd28828f96c508ae755e82060436",
            "mime": "text/markdown",
            "size": 19,
            "meta": {},
            "data": ["# Title\n", "\n", "Paragraph\n"],
            "encoding": "text",
        }
    ],
}
NOTE_2_JSON_NO_DATA = {
    **NOTE_2_JSON,  # type: ignore
    "files": [
        {
            **NOTE_2_JSON["files"][0],  # type: ignore
            "data": None,
            "encoding": "omitted",
        }
    ],
}
NOTE_3_JSON = {
    "nid": "20201113T182454852122R334524",
    "text": ["third note"],
    "tags": [],
    "attrs": {
        "created": "2022-01-02T07:24:54Z",
        "modified": "2022-01-02T09:24:54Z",
        "link": "https://www.oleeka.com/join",
    },
    "files": [],
}
