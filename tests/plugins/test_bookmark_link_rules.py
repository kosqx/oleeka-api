import pytest  # type: ignore

from urllib.parse import urlparse

from oleeka_api.plugins.bookmark.link_rules import (
    LinkRule,
    domain_matches,
    apply_link_rules,
)


@pytest.mark.parametrize(
    ("link", "domain_expr", "expected"),
    [
        ("http://oleeka.com", "example.com", False),
        ("http://oleeka.com", "oleeka", False),
        ("http://oleeka.com", "app.oleeka.com", False),
        ("http://oleeka.com", "oleeka.com", True),
        ("http://oleeka.com", "oleeka.*", True),
        ("http://oleeka.com/foo/bar", "oleeka.com", True),
        ("http://oleeka.com?foo=bar", "oleeka.com", True),
        ("http://oleeka.com/?foo=bar", "oleeka.com", True),
        ("http://app.oleeka.com", "oleeka.com", False),
        ("http://app.oleeka.com", "*.oleeka.com", True),
        ("http://app.oleeka.com", "app.oleeka.com", True),
    ],
)
def test_domain_matches(link, domain_expr, expected):
    assert domain_matches(urlparse(link), domain_expr) == expected


GOOGLE_ANALYTICS_RULE = LinkRule(domains=["*"], remove_params=["ga_*", "gclid"])
YOUTUBE_RULE = LinkRule(domains=["youtube.com"], remove_params=["t"])


@pytest.mark.parametrize(
    ("link", "link_rules", "expected"),
    [
        ("http://oleeka.com", [], "http://oleeka.com"),
        ("http://oleeka.com/?abc=def", [], "http://oleeka.com/?abc=def"),
        ("http://oleeka.com/?abc=def#", [], "http://oleeka.com/?abc=def"),
        ("http://oleeka.com/?abc=def#foo", [], "http://oleeka.com/?abc=def#foo"),
        (
            "http://oleeka.com/?abc=def",
            [GOOGLE_ANALYTICS_RULE],
            "http://oleeka.com/?abc=def",
        ),
        (
            "http://oleeka.com/?abc=def&ga_foo=bar",
            [GOOGLE_ANALYTICS_RULE],
            "http://oleeka.com/?abc=def",
        ),
        (
            "http://oleeka.com/?ga_foo=bar&abc=def",
            [GOOGLE_ANALYTICS_RULE],
            "http://oleeka.com/?abc=def",
        ),
        (
            "http://oleeka.com/w?t=123",
            [GOOGLE_ANALYTICS_RULE],
            "http://oleeka.com/w?t=123",
        ),
        (
            "http://oleeka.com/w?t=123",
            [GOOGLE_ANALYTICS_RULE, YOUTUBE_RULE],
            "http://oleeka.com/w?t=123",
        ),
        (
            "http://youtube.com/w?t=123",
            [GOOGLE_ANALYTICS_RULE, YOUTUBE_RULE],
            "http://youtube.com/w",
        ),
    ],
)
def test_apply_link_rules(link, link_rules, expected):
    assert apply_link_rules(link, link_rules) == expected
