import pytest  # type: ignore
from freezegun import freeze_time

from oleeka_api.models import Note
from oleeka_api.query.terms import Tag, Attr, Or

from tests.data import USER_1


@pytest.mark.parametrize(
    ("term", "expected"),
    [
        (
            Tag(":summer"),
            Or(
                [
                    Attr("created", "*==", "-06-"),
                    Attr("created", "*==", "-07-"),
                    Attr("created", "*==", "-08-"),
                ]
            ),
        ),
        (
            Tag(":y2020"),
            Attr("created", "^==", "2020-"),
        ),
    ],
)
def test_date_preprocess_query(plugin_manager, term, expected):
    assert plugin_manager.preprocess_query(term) == expected


@pytest.mark.parametrize(
    ("note", "expected"),
    [
        (
            Note(attrs={":age": "0"}),
            Note(attrs={"created": "2022-02-01T12:34:56Z"}),
        ),
        (
            Note(attrs={":age": "3d4h5m6s"}),
            Note(attrs={"created": "2022-01-29T08:29:50Z"}),
        ),
    ],
)
@freeze_time("2022-02-01T12:34:56")
def test_date_preprocess_note(plugin_manager, note, expected):
    assert plugin_manager.preprocess_note(note, USER_1) == expected
