import pytest  # type: ignore

from oleeka_api.query.terms import Tag, Or


@pytest.mark.parametrize(
    ("term", "expected"),
    [
        (Tag("foo"), Tag("foo")),
        (Tag("foo:"), Or([Tag("foo"), Tag("foo:", mode="prefix")])),
        (Tag(".foo"), Or([Tag(".foo"), Tag("foo")])),
        (
            Tag(".foo", mode="prefix"),
            Or([Tag(".foo", mode="prefix"), Tag("foo", mode="prefix")]),
        ),
        (
            Tag(".foo:"),
            Or(
                [
                    Or([Tag(".foo", mode="exact"), Tag("foo", mode="exact")]),
                    Or([Tag(".foo:", mode="prefix"), Tag("foo:", mode="prefix")]),
                ]
            ),
        ),
    ],
)
def test_tags_preprocess_query(plugin_manager, term, expected):
    assert plugin_manager.preprocess_query(term) == expected
