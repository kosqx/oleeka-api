import pytest  # type: ignore

from oleeka_api.models import Note
from oleeka_api.query.terms import Tag, Attr, Or

from tests.data import USER_1


NID = "20211221T123456012345R987654"


@pytest.mark.parametrize(
    ("term", "expected"),
    [
        (Tag("foo"), Tag("foo")),
        (Tag(":r2"), Attr("rating", "===", "2")),
        (Tag(":R2"), Or([Attr("rating", "===", "2"), Attr("rating", "===", "3")])),
    ],
)
def test_rating_preprocess_query(plugin_manager, term, expected):
    assert plugin_manager.preprocess_query(term) == expected


@pytest.mark.parametrize(
    ("note", "expected"),
    [
        (
            Note(nid=NID, tags=["foo"]),
            Note(nid=NID, tags=["foo"]),
        ),
        (
            Note(nid=NID, tags=["foo", ":r4"]),
            Note(nid=NID, tags=["foo", ":r4"]),
        ),
        (
            Note(nid=NID, tags=["foo", ":r2"]),
            Note(nid=NID, tags=["foo"], attrs={"rating": "2"}),
        ),
        (
            Note(nid=NID, tags=["foo", ":r2"], attrs={"rating": "1"}),
            Note(nid=NID, tags=["foo"], attrs={"rating": "2"}),
        ),
        (
            Note(nid=NID, tags=[":r3", ":r1"]),
            Note(nid=NID, tags=[], attrs={"rating": "1"}),
        ),
    ],
)
def test_rating_preprocess_note(plugin_manager, note, expected):
    assert plugin_manager.preprocess_note(note, USER_1) == expected
