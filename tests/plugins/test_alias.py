import pytest  # type: ignore


from oleeka_api.models import Note

from tests.data import USER_1


NID = "20211221T123456012345R987654"
NOTE = Note(nid=NID, tags=["foo"], attrs={"href": "http://oleeka.com"})


@pytest.mark.parametrize(
    ("settings", "note", "expected"),
    [
        (
            {},
            NOTE,
            NOTE,
        ),
        (
            {"note_aliases": []},
            NOTE,
            NOTE,
        ),
        (
            {"note_aliases": [{"query": "foo", "input": "bar baz"}]},
            NOTE,
            NOTE.replace(tags=["foo", "bar", "baz"]),
        ),
        (
            {"note_aliases": [{"query": "href*=example.com", "input": "rating=1"}]},
            NOTE,
            NOTE,
        ),
        (
            {"note_aliases": [{"query": "href*=oleeka.com", "input": "rating=2"}]},
            NOTE,
            NOTE.replace(attrs={"href": "http://oleeka.com", "rating": "2"}),
        ),
        (
            {
                "note_aliases": [
                    {"query": "foo", "input": "bar baz"},
                    {"query": "href*=oleeka.com", "input": "rating=2"},
                ]
            },
            NOTE,
            NOTE.replace(
                tags=["foo", "bar", "baz"],
                attrs={"href": "http://oleeka.com", "rating": "2"},
            ),
        ),
    ],
)
def test_alias_preprocess_note(plugin_manager, settings, note, expected):
    user = USER_1.replace(settings=settings)
    assert plugin_manager.preprocess_note(note, user) == expected
