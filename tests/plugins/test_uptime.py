import asyncio
from datetime import datetime, timedelta

import pytest  # type: ignore
from freezegun import freeze_time

from oleeka_api.models import Note, PaginatedQuery
from oleeka_api.query.terms import Attr
from oleeka_api.plugins.uptime import (
    UptimeRange,
    parse_uptime,
    format_uptime,
    update_uptime,
    format_timedelta,
)


EXAMPLE_UPTIME_RANGES = [
    UptimeRange(
        host="bar",
        start=datetime(2021, 10, 31, 12, 40, 10),
        stop=datetime(2021, 10, 31, 12, 50, 20),
    ),
    UptimeRange(
        host="foo",
        start=datetime(2021, 10, 31, 12, 34, 56),
        stop=datetime(2021, 10, 31, 12, 34, 56),
    ),
]


def test_parse_uptime():
    assert (
        parse_uptime(
            "foo                   2021-10-31T12:34:56Z  2021-10-31T12:34:56Z\n"
            "\n"
            "bar                   2021-10-31T12:40:10Z  2021-10-31T12:50:20Z\n"
        )
        == EXAMPLE_UPTIME_RANGES
    )


def test_format_uptime():
    assert format_uptime(EXAMPLE_UPTIME_RANGES) == (
        "bar                   2021-10-31T12:40:10Z  2021-10-31T12:50:20Z\n"
        "foo                   2021-10-31T12:34:56Z  2021-10-31T12:34:56Z"
    )


@pytest.mark.parametrize(
    ("host", "now", "expected", "expected_last"),
    [
        pytest.param(
            "bar",
            datetime(2021, 10, 31, 12, 50, 20),
            EXAMPLE_UPTIME_RANGES,
            EXAMPLE_UPTIME_RANGES[0],
            id="no_changes",
        ),
        pytest.param(
            "bar",
            datetime(2021, 10, 31, 12, 51, 20),
            [
                UptimeRange(
                    host="bar",
                    start=datetime(2021, 10, 31, 12, 40, 10),
                    stop=datetime(2021, 10, 31, 12, 51, 20),
                ),
                EXAMPLE_UPTIME_RANGES[1],
            ],
            UptimeRange(
                host="bar",
                start=datetime(2021, 10, 31, 12, 40, 10),
                stop=datetime(2021, 10, 31, 12, 51, 20),
            ),
            id="extend_range",
        ),
        pytest.param(
            "bar",
            datetime(2021, 10, 31, 12, 59, 20),
            [
                EXAMPLE_UPTIME_RANGES[0],
                UptimeRange(
                    host="bar",
                    start=datetime(2021, 10, 31, 12, 59, 20),
                    stop=datetime(2021, 10, 31, 12, 59, 20),
                ),
                EXAMPLE_UPTIME_RANGES[1],
            ],
            UptimeRange(
                host="bar",
                start=datetime(2021, 10, 31, 12, 59, 20),
                stop=datetime(2021, 10, 31, 12, 59, 20),
            ),
            id="new_range",
        ),
    ],
)
def test_update_uptime(host, now, expected, expected_last):
    assert update_uptime(EXAMPLE_UPTIME_RANGES, host, now) == (expected, expected_last)
    assert update_uptime(EXAMPLE_UPTIME_RANGES, host, now) == (expected, expected_last)


@pytest.mark.parametrize(
    ("delta", "expected"),
    [
        (timedelta(), "00:00:00"),
        (timedelta(seconds=12 * 3600 + 34 * 60 + 56), "12:34:56"),
        (timedelta(seconds=1 * 3600 + 2 * 60 + 3), "01:02:03"),
        (timedelta(days=3, seconds=12 * 3600 + 34 * 60 + 56), "84:34:56"),
    ],
)
def test_format_timedelta(delta, expected):
    assert format_timedelta(delta) == expected


def _get_uptime_notes(storage_backend):
    query = PaginatedQuery(query_terms=Attr("kind", "===", "uptime"))
    notes = asyncio.run(storage_backend.get_notes(query))
    return [note.replace(nid="") for note in notes]


def test_uptime_ping_endpoint(test_client_basic_auth, memory_storage_backend):
    test_client = test_client_basic_auth

    with freeze_time("2021-10-31T12:34:56"):
        response = test_client.post("/api/v2/plugin/uptime/ping/h0st")
        assert response.status_code == 200
        assert response.json() == {"uptime": "00:00:00"}

    assert _get_uptime_notes(memory_storage_backend) == [
        Note(
            text="h0st                  2021-10-31T12:34:56Z  2021-10-31T12:34:56Z",
            attrs={
                "created": "2021-10-31T12:34:56Z",
                "kind": "uptime",
                "uptime-host": "h0st",
                "modified": "2021-10-31T12:34:56Z",
            },
        )
    ]

    with freeze_time("2021-10-31T12:37:56"):
        response = test_client.post("/api/v2/plugin/uptime/ping/h0st")
        assert response.status_code == 200
        assert response.json() == {"uptime": "00:03:00"}

    assert _get_uptime_notes(memory_storage_backend) == [
        Note(
            text="h0st                  2021-10-31T12:34:56Z  2021-10-31T12:37:56Z",
            attrs={
                "created": "2021-10-31T12:34:56Z",
                "modified": "2021-10-31T12:37:56Z",
                "kind": "uptime",
                "uptime-host": "h0st",
            },
        )
    ]

    with freeze_time("2021-10-31T12:44:56"):
        response = test_client.post("/api/v2/plugin/uptime/ping/h0st")
        assert response.status_code == 200
        assert response.json() == {"uptime": "00:00:00"}

    assert _get_uptime_notes(memory_storage_backend) == [
        Note(
            text=(
                "h0st                  2021-10-31T12:34:56Z  2021-10-31T12:37:56Z\n"
                "h0st                  2021-10-31T12:44:56Z  2021-10-31T12:44:56Z"
            ),
            attrs={
                "created": "2021-10-31T12:34:56Z",
                "modified": "2021-10-31T12:44:56Z",
                "kind": "uptime",
                "uptime-host": "h0st",
            },
        )
    ]

    with freeze_time("2021-11-01T01:23:45"):
        response = test_client.post("/api/v2/plugin/uptime/ping/h0st")
        assert response.status_code == 200
        assert response.json() == {"uptime": "00:00:00"}

    assert _get_uptime_notes(memory_storage_backend) == [
        Note(
            text="h0st                  2021-11-01T01:23:45Z  2021-11-01T01:23:45Z",
            attrs={
                "created": "2021-11-01T01:23:45Z",
                "kind": "uptime",
                "uptime-host": "h0st",
                "modified": "2021-11-01T01:23:45Z",
            },
        ),
        Note(
            text=(
                "h0st                  2021-10-31T12:34:56Z  2021-10-31T12:37:56Z\n"
                "h0st                  2021-10-31T12:44:56Z  2021-10-31T12:44:56Z"
            ),
            attrs={
                "created": "2021-10-31T12:34:56Z",
                "modified": "2021-10-31T12:44:56Z",
                "kind": "uptime",
                "uptime-host": "h0st",
            },
        ),
    ]
