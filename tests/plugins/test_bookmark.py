import asyncio

import pytest  # type: ignore

from oleeka_api.models import Note, UserAccount
from oleeka_api.query.terms import Attr

from tests.helpers import Matches


USER_1 = UserAccount(username="testuser")


@pytest.mark.parametrize(
    ("term", "expected"),
    [
        (Attr("link", "=", "oleeka.com"), Attr("href", "=", "oleeka.com")),
        (
            Attr("link", "===", "https://oleeka.com/"),
            Attr("href", "===", "https://oleeka.com/"),
        ),
    ],
)
def test_rating_preprocess_query(plugin_manager, term, expected):
    assert plugin_manager.preprocess_query(term) == expected


def test_bookmark_match_endpoint_new(test_client, memory_storage_backend):
    asyncio.run(memory_storage_backend.testing_insert(USER_1))

    body_json = {
        "link": "https://oleeka.com/foo#bar",
        "title": "Oleeka Notes",
        "description": "description from meta",
        "selection": "selected text",
    }
    response = test_client.post("/api/v2/plugin/bookmark/match", json=body_json)
    assert response.status_code == 200
    assert response.json() == {
        "matches": [
            {
                "kind": "new",
                "nid": None,
                "link": "https://oleeka.com/foo#bar",
                "title": "Oleeka Notes",
                "text": (
                    "<blockquote>description from meta</blockquote>\n"
                    "\n"
                    "<blockquote>selected text</blockquote>"
                ),
                "input": "",
            }
        ],
    }

    bookmark = response.json()["matches"][0]
    bookmark = {**bookmark, "input": "foo baz"}
    response_save = test_client.post(
        "/api/v2/plugin/bookmark/save", json={"bookmark": bookmark}
    )
    assert response_save.status_code == 200
    assert response_save.json() == {
        "bookmark": {
            "kind": "saved",
            "nid": Matches.nid(),
            "link": "https://oleeka.com/foo#bar",
            "title": "Oleeka Notes",
            "text": (
                "<blockquote>description from meta</blockquote>\n"
                "\n"
                "<blockquote>selected text</blockquote>"
            ),
            "input": "foo baz",
        },
    }


def test_bookmark_match_endpoint_existing(test_client, memory_storage_backend):
    note = Note(
        nid="20201113T182454852122R334524",
        text="note text",
        tags=["foo", "tag:1"],
        attrs={
            "created": "2020-11-13T18:24:54Z",
            "modified": "2020-11-13T18:24:54Z",
            "kind": "bookmark",
            "href": "https://www.oleeka.com/abc",
            "title": "My Link",
        },
    )
    asyncio.run(memory_storage_backend.testing_insert(note, USER_1))

    body_json = {
        "link": "https://www.oleeka.com/abc",
        "title": "Oleeka Notes",
        "description": "description from meta",
        "selection": "selected text",
    }
    response = test_client.post("/api/v2/plugin/bookmark/match", json=body_json)
    assert response.status_code == 200
    assert response.json() == {
        "matches": [
            {
                "kind": "exact",
                "nid": "20201113T182454852122R334524",
                "link": "https://www.oleeka.com/abc",
                "title": "My Link",
                "text": "note text",
                "input": "foo tag:1",
            }
        ],
    }

    bookmark = response.json()["matches"][0]
    bookmark = {**bookmark, "input": "foo baz tag:2", "title": "Updated Link"}
    response_save = test_client.post(
        "/api/v2/plugin/bookmark/save", json={"bookmark": bookmark}
    )
    assert response_save.status_code == 200
    assert response_save.json() == {
        "bookmark": {
            "kind": "saved",
            "nid": "20201113T182454852122R334524",
            "link": "https://www.oleeka.com/abc",
            "title": "Updated Link",
            "text": "note text",
            "input": "foo baz tag:2",
        },
    }


@pytest.mark.parametrize(
    ("settings", "link_in", "link_out"),
    [
        (
            {},
            "https://oleeka.com/foo?utm_source=ding#bar",
            "https://oleeka.com/foo#bar",
        ),
        (
            {"link_rules": {}},
            "https://oleeka.com/foo?utm_source=ding#bar",
            "https://oleeka.com/foo#bar",
        ),
        (
            {"link_rules": {"use_default": False}},
            "https://oleeka.com/foo?utm_source=ding#bar",
            "https://oleeka.com/foo?utm_source=ding#bar",
        ),
        (
            {"link_rules": {"use_default": False, "rules": "invalid"}},
            "https://oleeka.com/foo?utm_source=ding#bar",
            "https://oleeka.com/foo#bar",
        ),
        (
            {},
            "https://www.youtube.com/watch?v=S_d-gs0WoUw&t=123",
            "https://www.youtube.com/watch?v=S_d-gs0WoUw&t=123",
        ),
        (
            {
                "link_rules": {
                    "rules": [{"domains": ["*.youtube.com"], "remove_params": ["t"]}]
                }
            },
            "https://www.youtube.com/watch?v=S_d-gs0WoUw&t=123",
            "https://www.youtube.com/watch?v=S_d-gs0WoUw",
        ),
    ],
)
def test_bookmark_match_endpoint_with_rules(
    test_client, memory_storage_backend, settings, link_in, link_out
):
    user = UserAccount(username="testuser", settings=settings)
    asyncio.run(memory_storage_backend.testing_insert(user))

    body_json = {
        "link": link_in,
        "title": "Oleeka Notes",
    }
    response = test_client.post("/api/v2/plugin/bookmark/match", json=body_json)
    assert response.status_code == 200
    assert response.json() == {
        "matches": [
            {
                "kind": "new",
                "nid": None,
                "link": link_out,
                "title": "Oleeka Notes",
                "text": "",
                "input": "",
            }
        ],
    }
