import pytest  # type: ignore

from oleeka_api.logic.plugin import get_plugin_manager


@pytest.fixture(scope="function")
def plugin_manager():
    return get_plugin_manager()
