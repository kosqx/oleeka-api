import pytest  # type: ignore
from freezegun import freeze_time

from tests.data import NOTE_1_JSON, NOTE_2_JSON

NID_A = "20210518T194557365509R215338"


API_NOTE_PUT_TESTCASES = [
    pytest.param(
        NID_A,
        {"note": {"nid": NID_A}},
        200,
        {
            "note": {
                "nid": NID_A,
                "text": [],
                "tags": [],
                "attrs": {
                    "created": "2021-10-31T12:34:56Z",
                    "modified": "2021-10-31T12:34:56Z",
                },
                "files": [],
            },
        },
        id="empty",
    ),
    pytest.param(
        NOTE_1_JSON["nid"],
        {"note": {"nid": NOTE_1_JSON["nid"]}},
        200,
        {
            "note": {
                "nid": NOTE_1_JSON["nid"],
                "text": ["first note"],
                "tags": ["hello"],
                "attrs": {
                    "created": NOTE_1_JSON["attrs"]["created"],  # type: ignore
                    "modified": "2021-10-31T12:34:56Z",
                    "name": "abc foo123 xyz",
                    "rating": "2",
                },
                "files": [],
            },
        },
        id="existing_fields_omitted",
    ),
    pytest.param(
        NOTE_1_JSON["nid"],
        {
            "note": {
                "nid": NOTE_1_JSON["nid"],
                "text": "",
                "tags": [],
            }
        },
        200,
        {
            "note": {
                "nid": NOTE_1_JSON["nid"],
                "text": [],
                "tags": [],
                "attrs": {
                    "created": NOTE_1_JSON["attrs"]["created"],  # type: ignore
                    "modified": "2021-10-31T12:34:56Z",
                    "name": "abc foo123 xyz",
                    "rating": "2",
                },
                "files": [],
            },
        },
        id="existing_fields_empty",
    ),
    pytest.param(
        NOTE_1_JSON["nid"],
        {
            "note": {
                "nid": NOTE_1_JSON["nid"],
                "text": "new text",
                "tags": ["new", "tags"],
            }
        },
        200,
        {
            "note": {
                "nid": NOTE_1_JSON["nid"],
                "text": ["new text"],
                "tags": ["new", "tags"],
                "attrs": {
                    "created": NOTE_1_JSON["attrs"]["created"],  # type: ignore
                    "modified": "2021-10-31T12:34:56Z",
                    "name": "abc foo123 xyz",
                    "rating": "2",
                },
                "files": [],
            },
        },
        id="existing_fields_filled",
    ),
    pytest.param(
        NOTE_2_JSON["nid"],
        {
            "note": {
                "nid": NOTE_2_JSON["nid"],
                "input": "new-tag name='--redacted--' speed=24.5 hello! 'second line'",
            }
        },
        200,
        {
            "note": {
                "nid": NOTE_2_JSON["nid"],
                "text": ["second note\n", "second line"],
                "tags": ["world", "new-tag"],
                "attrs": {
                    "created": NOTE_2_JSON["attrs"]["created"],  # type: ignore
                    "modified": "2021-10-31T12:34:56Z",
                    "name": "--redacted--",
                    "speed": "24.5",
                },
                "files": NOTE_2_JSON["files"],
            },
        },
        id="input_modifies_note",
    ),
    pytest.param(
        NOTE_2_JSON["nid"],
        {
            "note": {
                "nid": NOTE_2_JSON["nid"],
                "input": "==new-tag",
            }
        },
        200,
        {
            "note": {
                **NOTE_2_JSON,  # type: ignore
                "tags": ["new-tag"],
                "attrs": {
                    **NOTE_2_JSON["attrs"],  # type: ignore
                    "modified": "2021-10-31T12:34:56Z",
                },
            },
        },
        id="input_with_reset_tag",
    ),
]


@pytest.mark.parametrize(
    ("nid", "body_json", "status", "response_json"),
    API_NOTE_PUT_TESTCASES,
)
@freeze_time("2021-10-31T12:34:56")
def test_api_notes_put(
    test_client, memory_storage_backend, nid, body_json, status, response_json
):
    response = test_client.put(f"/api/v2/notes/{nid}", json=body_json)
    assert response.status_code == status
    assert response.json() == response_json
