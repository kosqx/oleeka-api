from oleeka_api.api.auth import create_access_token

from tests.helpers import to_base64


def test_auth_no_token(test_client_unauthorized, memory_storage_backend):
    response = test_client_unauthorized.get("/api/v2/notes")
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_auth_token_expired(test_client_unauthorized, memory_storage_backend):
    token = create_access_token(data={"sub": "testuser"}, duration=-10)
    test_client_unauthorized.headers.update({"Authorization": f"Bearer {token}"})

    response = test_client_unauthorized.get("/api/v2/notes")
    assert response.status_code == 401
    assert response.json() == {"detail": "Signature has expired."}


def test_auth_token_ok(test_client_unauthorized, memory_storage_backend):
    token = create_access_token(data={"sub": "testuser"}, duration=60)
    test_client_unauthorized.headers.update({"Authorization": f"Bearer {token}"})

    response = test_client_unauthorized.get("/api/v2/notes", params={"limit": 0})
    assert response.status_code == 200
    assert response.json() == {"notes": []}


def test_auth_basic_invalid_user(test_client_unauthorized, memory_storage_backend):
    credentials = to_base64("invalid:pass")
    test_client_unauthorized.headers.update({"Authorization": f"Basic {credentials}"})

    response = test_client_unauthorized.get("/api/v2/notes", params={"limit": 0})
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_auth_basic_invalid_password(test_client_unauthorized, memory_storage_backend):
    credentials = to_base64("testuser:pass")
    test_client_unauthorized.headers.update({"Authorization": f"Basic {credentials}"})

    response = test_client_unauthorized.get("/api/v2/notes", params={"limit": 0})
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_auth_basic_ok(test_client_unauthorized, memory_storage_backend):
    credentials = to_base64("testuser:XYZ123")
    test_client_unauthorized.headers.update({"Authorization": f"Basic {credentials}"})

    response = test_client_unauthorized.get("/api/v2/notes", params={"limit": 0})
    assert response.status_code == 200
    assert response.json() == {"notes": []}
