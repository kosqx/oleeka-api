from typing import Dict, Iterable, Optional

import pytest  # type: ignore

from tests.data import FILE_A, NOTE_1, NOTE_2, NOTE_2_JSON, NOTE_2_JSON_NO_DATA


def headers_subdict(
    mapping: Dict[str, str], keys: Iterable[str]
) -> Dict[str, Optional[str]]:
    return {key: mapping.get(key) for key in keys}


@pytest.mark.parametrize(
    ("params", "headers"),
    [
        pytest.param(
            {},
            {
                "content-length": str(FILE_A.size),
                "content-type": "text/markdown; charset=utf-8",
                "content-disposition": None,
            },
            id="by default just returns data",
        ),
        pytest.param(
            {"download": "yes"},
            {
                "content-length": str(FILE_A.size),
                "content-type": "text/markdown; charset=utf-8",
                "content-disposition": f'attachment; filename="{FILE_A.name}"',
            },
            id="requests data download",
        ),
    ],
)
def test_api_get_file_data(test_client, memory_storage_backend, params, headers):
    url = f"/api/v2/notes/{NOTE_2.nid}/files/{FILE_A.fid}/data"
    response = test_client.get(url, params=params)

    assert response.status_code == 200
    assert response.content == FILE_A.data
    assert headers_subdict(response.headers, headers) == headers


@pytest.mark.parametrize(
    ("url", "params", "status_code", "response_json"),
    [
        pytest.param(
            f"/api/v2/notes/{NOTE_2.nid}/files/{FILE_A.fid}",
            {},
            200,
            {"file": NOTE_2_JSON_NO_DATA["files"][0]},
            id="ok, without data",
        ),
        pytest.param(
            f"/api/v2/notes/{NOTE_2.nid}/files/{FILE_A.fid}",
            {"fetch_data": "yes"},
            200,
            {"file": NOTE_2_JSON["files"][0]},  # type: ignore
            id="ok, with data",
        ),
        pytest.param(
            f"/api/v2/notes/{NOTE_2.nid}/files/F999",
            {},
            404,
            {"detail": {"message": "file not found", "id": "F999"}},
            id="missing fid",
        ),
        pytest.param(
            f"/api/v2/notes/{NOTE_1.nid}/files/{FILE_A.fid}",
            {},
            404,
            {"detail": {"message": "file not found", "id": "F1"}},
            id="note without given file",
        ),
        pytest.param(
            f"/api/v2/notes/000/files/{FILE_A.fid}",
            {},
            422,
            {
                "detail": [
                    {
                        "ctx": {"limit_value": 28},
                        "loc": ["path", "nid"],
                        "msg": "ensure this value has at least 28 characters",
                        "type": "value_error.any_str.min_length",
                    }
                ]
            },
            id="invalid nid",
        ),
        #############################################################
        # /data
        pytest.param(
            f"/api/v2/notes/{NOTE_2.nid}/files/F999/data",
            {},
            404,
            {"detail": {"message": "file not found", "id": "F999"}},
            id="get data: missing fid",
        ),
        pytest.param(
            f"/api/v2/notes/{NOTE_1.nid}/files/{FILE_A.fid}",
            {},
            404,
            {"detail": {"message": "file not found", "id": "F1"}},
            id="get data: note without given file",
        ),
    ],
)
def test_api_get_file(
    test_client, memory_storage_backend, url, params, status_code, response_json
):
    response = test_client.get(url, params=params)

    assert response.status_code == status_code
    assert response.json() == response_json
