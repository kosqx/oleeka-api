import asyncio
import copy
import re

from tests.helpers import Matches


JWT_REGEX = re.compile(r"^[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+$")
COOKIE_REGEX = re.compile(
    r"^refresh_token.testuser=\w+; HttpOnly; Path=/; SameSite=lax; Secure$"
)


def test_api_auth_token_ok(test_client_unauthorized, memory_storage_backend):
    response = test_client_unauthorized.post(
        "/api/v2/auth/token", data={"username": "testuser", "password": "testpass"}
    )
    assert response.status_code == 200
    assert response.json() == {
        "access_token": Matches(JWT_REGEX),
        "token_type": "bearer",
    }


def test_api_auth_token_no_data(test_client_unauthorized, memory_storage_backend):
    response = test_client_unauthorized.post("/api/v2/auth/token", data={})
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "username"],
                "msg": "field required",
                "type": "value_error.missing",
            },
            {
                "loc": ["body", "password"],
                "msg": "field required",
                "type": "value_error.missing",
            },
        ],
    }


def test_api_auth_token_incorrect(test_client_unauthorized, memory_storage_backend):
    response = test_client_unauthorized.post(
        "/api/v2/auth/token", data={"username": "someuser", "password": "somepass"}
    )
    assert response.status_code == 400
    assert response.json() == {
        "detail": "Incorrect username or password",
    }


def test_api_auth_login_and_refresh(test_client_unauthorized, memory_storage_backend):
    user_account = asyncio.run(memory_storage_backend.get_user("testuser"))
    old_auths = copy.deepcopy(user_account.auth["auths"])

    response = test_client_unauthorized.post(
        "/api/v2/auth/login", json={"username": "testuser", "password": "testpass"}
    )

    assert response.headers["set-cookie"] == Matches(COOKIE_REGEX)
    assert response.status_code == 200
    assert response.json() == {
        "access_token": Matches(JWT_REGEX),
    }

    user_account = asyncio.run(memory_storage_backend.get_user("testuser"))
    assert user_account.auth["auths"] == old_auths + [
        {"token": Matches.regex(r"\w+")},
    ]

    response = test_client_unauthorized.post(
        "/api/v2/auth/refresh/testuser",
        cookies=test_client_unauthorized.cookies.get_dict(),
    )
    assert response.status_code == 200
    assert response.json() == {
        "access_token": Matches(JWT_REGEX),
    }


def test_api_auth_refresh_no_cookie(test_client_unauthorized, memory_storage_backend):
    response = test_client_unauthorized.post("/api/v2/auth/refresh/testuser")
    assert response.status_code == 400
    assert response.json() == {"detail": "Incorrect refresh token"}
