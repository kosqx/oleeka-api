from tests.data import NOTE_1_JSON, NOTE_2_JSON, NOTE_3_JSON


def get_nids(test_client):
    response = test_client.get("/api/v2/notes", params={"select_fields": "nid"})
    return [note["nid"] for note in response.json()["notes"]]


def test_api_note_delete(test_client, memory_storage_backend):
    nid_1, nid_2, nid_3 = NOTE_1_JSON["nid"], NOTE_2_JSON["nid"], NOTE_3_JSON["nid"]
    assert get_nids(test_client) == [nid_3, nid_2, nid_1]

    nid = NOTE_2_JSON["nid"]
    response = test_client.delete(f"/api/v2/notes/{nid}")
    assert response.status_code == 204
    assert response.content == b""

    assert get_nids(test_client) == [nid_3, nid_1]
