import pytest  # type: ignore

RELATED_TESTCASES = [
    (
        {},
        200,
        {
            "total": 3,
            "tags_empty": 1,
            "tags": {"hello": 2, "world": 1},
            "attrs": None,
            "sizes": None,
            "dates": None,
        },
    ),
    (
        {"fetch_total": "no", "fetch_tags": "no", "fetch_attrs": "no"},
        200,
        {
            "total": None,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": None,
            "dates": None,
        },
    ),
    (
        # if `fetch_tags` or `fetch_attrs` are set then `total` is also fetched
        {"fetch_total": "no", "fetch_tags": "yes", "fetch_attrs": "yes"},
        200,
        {
            "total": 3,
            "tags_empty": 1,
            "tags": {"hello": 2, "world": 1},
            "attrs": {"created": 3, "link": 1, "modified": 3, "name": 2, "rating": 1},
            "sizes": None,
            "dates": None,
        },
    ),
    (
        {"fetch_tags": "no", "fetch_attrs": "no"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": None,
            "dates": None,
        },
    ),
    (
        {"fetch_tags": "no", "fetch_attrs": "yes"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": {"created": 3, "link": 1, "modified": 3, "name": 2, "rating": 1},
            "sizes": None,
            "dates": None,
        },
    ),
    (
        {"fetch_tags": "yes", "fetch_attrs": "no"},
        200,
        {
            "total": 3,
            "tags_empty": 1,
            "tags": {"hello": 2, "world": 1},
            "attrs": None,
            "sizes": None,
            "dates": None,
        },
    ),
    (
        {"fetch_tags": "yes", "fetch_attrs": "yes"},
        200,
        {
            "total": 3,
            "tags_empty": 1,
            "tags": {"hello": 2, "world": 1},
            "attrs": {"created": 3, "link": 1, "modified": 3, "name": 2, "rating": 1},
            "sizes": None,
            "dates": None,
        },
    ),
    (
        {"query": "world", "fetch_attrs": "yes"},
        200,
        {
            "total": 1,
            "tags_empty": 0,
            "tags": {"hello": 1, "world": 1},
            "attrs": {"created": 1, "modified": 1, "name": 1},
            "sizes": None,
            "dates": None,
        },
    ),
    # fetch_sizes tests
    (
        {"fetch_tags": "no", "fetch_sizes": "yes"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": {
                "text_size": 0,
                "tags_counts": 3,
                "attrs_counts": 10,
                "files_counts": 1,
                "files_size": 19,
            },
            "dates": None,
        },
    ),
    # fetch_dates tests
    (
        {"fetch_tags": "no", "fetch_dates": "year"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": None,
            "dates": {"2020": 2, "2022": 1},
        },
    ),
    (
        {"fetch_tags": "no", "fetch_dates": "month"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": None,
            "dates": {"2020-11": 2, "2022-01": 1},
        },
    ),
    (
        {"fetch_tags": "no", "fetch_dates": "day"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": None,
            "dates": {"2020-11-13": 2, "2022-01-02": 1},
        },
    ),
    (
        {"fetch_tags": "no", "fetch_dates": "hour"},
        200,
        {
            "total": 3,
            "tags_empty": None,
            "tags": None,
            "attrs": None,
            "sizes": None,
            "dates": {"2020-11-13T17": 2, "2022-01-02T07": 1},
        },
    ),
    (
        {"query": "!"},
        422,
        {
            "detail": [
                {
                    "char_index": 1,
                    "loc": ["query", "query"],
                    "msg": "query cannot be parsed",
                    "type": "parse_error.query",
                }
            ]
        },
    ),
    (
        {"query": "world :RANDOM"},
        422,
        # TODO: make error message more specific and standardized
        {"detail": ":RANDOM in query is not allowed here"},
    ),
]


@pytest.mark.parametrize(("params", "status_code", "response_json"), RELATED_TESTCASES)
def test_api_related(
    test_client, memory_storage_backend, params, status_code, response_json
):
    response = test_client.get("/api/v2/related", params=params)
    assert response.status_code == status_code
    assert response.json() == response_json
