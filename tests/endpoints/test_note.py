import pytest  # type: ignore

from tests.data import NOTE_1_JSON, NOTE_2_JSON, NOTE_2_JSON_NO_DATA, NOTE_3_JSON


def build_note_json(*, nid):
    return {"nid": nid, "text": [], "tags": [], "attrs": {}, "files": []}


NOTE_SELECT_FIELDS_TESTCASES = [
    (
        NOTE_1_JSON["nid"],
        {},
        200,
        {"note": NOTE_1_JSON},
    ),
    (
        NOTE_2_JSON["nid"],
        {},
        200,
        {"note": NOTE_2_JSON_NO_DATA},
    ),
    (
        NOTE_3_JSON["nid"],
        {"select_fields": "nid"},
        200,
        {"note": build_note_json(nid=NOTE_3_JSON["nid"])},
    ),
]


@pytest.mark.parametrize(
    ("nid", "params", "status", "response_json"), NOTE_SELECT_FIELDS_TESTCASES
)
def test_api_notes_get(
    test_client, memory_storage_backend, nid, params, status, response_json
):
    response = test_client.get(f"/api/v2/notes/{nid}", params=params)
    assert response.status_code == status
    assert response.json() == response_json
