import re

from tests.helpers import Matches


def test_middleware_server_timing(test_client_unauthorized):
    response = test_client_unauthorized.get("/api/v2")
    assert response.status_code == 200

    server_timing = response.headers["server-timing"]
    match = re.match(
        pattern=r"^cpu;dur=(?P<cpu_dur>[0-9.]+), wall;dur=(?P<wall_dur>[0-9.]+)$",
        string=server_timing,
    )
    assert match is not None
    values = match.groupdict()
    assert float(values["cpu_dur"]) <= 5.0  # ms
    assert float(values["wall_dur"]) <= 5.0  # ms


def test_api_root(test_client_unauthorized):
    response = test_client_unauthorized.get("/api/v2")
    assert response.status_code == 200
    assert response.json() == {
        "service": "oleeka",
        "version": Matches.regex(r"^\d+\.\d+\.\d+$"),
        "isotime": Matches(str),
    }


def test_favicon_ico(test_client_unauthorized):
    response = test_client_unauthorized.get("/favicon.ico")
    assert response.status_code == 200
    assert response.headers["content-length"] == "1078"
    assert response.content.startswith(b"\x00\x00\x01\x00\x02\x00\x10\x10\x10")
