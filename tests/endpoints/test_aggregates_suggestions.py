import pytest  # type: ignore

from tests.helpers import Matches

RELATED_TESTCASES = [
    (
        {},
        200,
        {
            "suggestions": Matches.contains(
                {
                    "name": ":age=",
                    "only": "edit",
                    "help": "Set creation time given time ago (`:age=1d2h34m`)",
                },
                {
                    "name": ":autumn",
                    "only": "query",
                    "help": "Created in autumn (meteorological)",
                },
                {
                    "name": ":spring",
                    "only": "query",
                    "help": "Created in spring (meteorological)",
                },
                {
                    "name": ":summer",
                    "only": "query",
                    "help": "Created in summer (meteorological)",
                },
                {
                    "name": ":winter",
                    "only": "query",
                    "help": "Created in winter (meteorological)",
                },
                {"name": "hello", "count": 2},
                {"name": "world", "count": 1},
                {"name": ":monday", "only": "query", "help": "Created on Monday"},
            ),
        },
    ),
    (
        {"query": "!"},
        422,
        {
            "detail": [
                {
                    "char_index": 1,
                    "loc": ["query", "query"],
                    "msg": "query cannot be parsed",
                    "type": "parse_error.query",
                }
            ]
        },
    ),
    (
        {"query": "world :RANDOM"},
        422,
        # TODO: make error message more specific and standardized
        {"detail": ":RANDOM in query is not allowed here"},
    ),
]


@pytest.mark.parametrize(("params", "status_code", "response_json"), RELATED_TESTCASES)
def test_api_suggestions(
    test_client, memory_storage_backend, params, status_code, response_json
):
    response = test_client.get("/api/v2/suggestions", params=params)
    assert response.status_code == status_code
    assert response.json() == response_json
