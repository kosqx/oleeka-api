import pytest  # type: ignore

from tests.helpers import Matches


def response_note(text=None, tags=None, attrs=None):
    return {
        "nid": Matches.nid(),
        "text": text or [],
        "tags": tags or [],
        "attrs": {
            "created": Matches.datetime_str(),
            "modified": Matches.datetime_str(),
            **(attrs or {}),
        },
        "files": [],
    }


RESTORE_NOTE = {
    "nid": "20211212T220555881054R431040",
    "text": ["line 1\r\n", "line 2"],
    "tags": ["foo"],
    "attrs": {
        "created": "2021-12-12T22:05:55Z",
        "key": "value",
        "modified": "2021-12-12T22:05:55Z",
    },
    "files": [],
}


API_NOTES_POST_TESTCASES = [
    pytest.param(
        {"note": {}},
        200,
        {"note": response_note()},
        [response_note()],
        id="empty",
    ),
    pytest.param(
        {"note": {"tags": [1, 2], "attrs": {12: 144}}},
        200,
        {"note": response_note(tags=["1", "2"], attrs={"12": "144"})},
        [response_note(tags=["1", "2"], attrs={"12": "144"})],
        id="cast_types",
    ),
    pytest.param(
        {"note": {"input": "key='val"}},
        422,
        {
            "detail": [
                {
                    "loc": ["body", "note", "input"],
                    "msg": "input cannot be parsed",
                    "type": "parse_error.input",
                    "char_index": 3,
                }
            ],
        },
        [],
        id="invalid_input",
    ),
    pytest.param(
        {"note": {"tags": [":r2"]}},
        200,
        {"note": response_note(attrs={"rating": "2"})},
        [response_note(attrs={"rating": "2"})],
        id="preprocess_rating_tags",
    ),
    pytest.param(
        {"note": {"input": ":r2"}},
        200,
        {"note": response_note(attrs={"rating": "2"})},
        [response_note(attrs={"rating": "2"})],
        id="preprocess_rating_input",
    ),
    pytest.param(
        {"note": dict(RESTORE_NOTE, restore=True)},
        200,
        {"note": RESTORE_NOTE},
        [RESTORE_NOTE],
        id="restore",
    ),
    pytest.param(
        {"note": {"input": ":r1"}, "notes": [{"tags": ["foo"]}, {"text": "message"}]},
        200,
        {"note": response_note(attrs={"rating": "1"})},
        [
            response_note(attrs={"rating": "1"}),
            response_note(tags=["foo"]),
            response_note(text=["message"]),
        ],
        id="multiple",
    ),
]


@pytest.mark.parametrize(
    ("body_json", "status", "response_json", "stored"),
    API_NOTES_POST_TESTCASES,
)
def test_api_notes_post(
    test_client, storage_backend_empty, body_json, status, response_json, stored
):
    response = test_client.post("/api/v2/notes", json=body_json)
    assert response.status_code == status
    assert response.json() == response_json

    response_get = test_client.get("/api/v2/notes")
    assert response_get.json()["notes"] == stored
