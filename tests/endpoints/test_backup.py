import json
import io
import re
import tarfile
from typing import Any, List

import pytest  # type: ignore

from tests.data import NOTE_1_JSON, NOTE_2_JSON, NOTE_3_JSON


PATTERN_BASE = r"^attachment;filename=backup_\d+-\d+-\d+T\d+:\d+:\d+Z_20y"
REGEX_NDJSON = re.compile(PATTERN_BASE + r"\.ndjson$")
REGEX_TARGZ = re.compile(PATTERN_BASE + r"\.tar\.gz$")


@pytest.mark.parametrize(
    ("params", "suffix"),
    [
        ({}, "Z.tar.gz"),
        ({"age": "48h"}, "Z_48h.tar.gz"),
        ({"file_format": "tar.gz"}, "Z.tar.gz"),
        ({"file_format": "tar.gz", "age": "2w"}, "Z_2w.tar.gz"),
        ({"file_format": "ndjson"}, "Z.ndjson"),
        ({"file_format": "ndjson", "age": "30d"}, "Z_30d.ndjson"),
    ],
)
def test_api_backup_download_params(
    test_client, memory_storage_backend, params, suffix
):
    response = test_client.get("/api/v2/backup/download", params=params)
    assert response.status_code == 200
    assert response.headers["content-disposition"].endswith(suffix)


def load_ndjson(content: bytes) -> List[Any]:
    return [json.loads(line) for line in content.decode("utf8").splitlines()]


def test_api_backup_download(test_client, memory_storage_backend):
    response = test_client.get(
        "/api/v2/backup/download", params={"age": "20y", "file_format": "ndjson"}
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/x-ndjson"
    assert response.headers["x-oleeka-notes-count"] == "3"
    assert REGEX_NDJSON.match(response.headers["content-disposition"]) is not None
    assert load_ndjson(response.content) == [NOTE_3_JSON, NOTE_2_JSON, NOTE_1_JSON]


def load_targz(content: bytes) -> List[Any]:
    file = tarfile.open(fileobj=io.BytesIO(content))
    result = []
    while True:
        info = file.next()
        if info is None:
            break
        else:
            file_extracted = file.extractfile(info)
            if file_extracted is not None:
                file_data = file_extracted.read()
                meta = {"name": info.name, "mtime": info.mtime}
                data = json.loads(file_data)
                result.append({"meta": meta, "data": data})
    return result


def test_api_backup_download_targz(test_client, memory_storage_backend):
    response = test_client.get(
        "/api/v2/backup/download", params={"age": "20y", "file_format": "tar.gz"}
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/gzip"
    assert response.headers["x-oleeka-notes-count"] == "3"
    assert REGEX_TARGZ.match(response.headers["content-disposition"]) is not None
    assert load_targz(response.content) == [
        {
            "meta": {
                "name": "2022-01/20201113T182454852122R334524",
                "mtime": 1641111894,
            },
            "data": NOTE_3_JSON,
        },
        {
            "meta": {
                "name": "2020-11/20201113T175554625048R173053",
                "mtime": 1605286554,
            },
            "data": NOTE_2_JSON,
        },
        {
            "meta": {
                "name": "2020-11/20201113T175543290880R684769",
                "mtime": 1605286543,
            },
            "data": NOTE_1_JSON,
        },
    ]
