import pytest  # type: ignore

from tests.data import NOTE_1_JSON, NOTE_2_JSON, NOTE_2_JSON_NO_DATA, NOTE_3_JSON


def build_note_json(*, nid):
    return {"nid": nid, "text": [], "tags": [], "attrs": {}, "files": []}


NOTE_SELECT_FIELDS_TESTCASES = [
    (
        {},
        200,
        {"notes": [NOTE_3_JSON, NOTE_2_JSON_NO_DATA, NOTE_1_JSON]},
    ),
    (
        {"skip": "0", "limit": "2"},
        200,
        {"notes": [NOTE_3_JSON, NOTE_2_JSON_NO_DATA]},
    ),
    (
        {"skip": "2", "limit": "2"},
        200,
        {"notes": [NOTE_1_JSON]},
    ),
    (
        {"select_fields": ""},
        200,
        {"notes": [NOTE_3_JSON, NOTE_2_JSON_NO_DATA, NOTE_1_JSON]},
    ),
    (
        {"select_fields": "nid"},
        200,
        {
            "notes": [
                build_note_json(nid=NOTE_3_JSON["nid"]),
                build_note_json(nid=NOTE_2_JSON["nid"]),
                build_note_json(nid=NOTE_1_JSON["nid"]),
            ]
        },
    ),
    (
        {"select_fields": "nid, text, tags, attrs, files, files_data"},
        200,
        {"notes": [NOTE_3_JSON, NOTE_2_JSON, NOTE_1_JSON]},
    ),
    # fetch_total support
    (
        {"fetch_total": "yes"},
        200,
        {"notes": [NOTE_3_JSON, NOTE_2_JSON_NO_DATA, NOTE_1_JSON], "total": 3},
    ),
    (
        {"fetch_total": "yes", "query": "hello"},
        200,
        {"notes": [NOTE_2_JSON_NO_DATA, NOTE_1_JSON], "total": 2},
    ),
    (
        {"fetch_total": "yes", "skip": "2"},
        200,
        {"notes": [NOTE_1_JSON], "total": 3},
    ),
    (
        {"fetch_total": "yes", "limit": "2"},
        200,
        {"notes": [NOTE_3_JSON, NOTE_2_JSON_NO_DATA], "total": 3},
    ),
    pytest.param(
        {"skip": "abc", "limit": "xyz"},
        422,
        {
            "detail": [
                {
                    "loc": ["query", "skip"],
                    "msg": "value is not a valid integer",
                    "type": "type_error.integer",
                },
                {
                    "loc": ["query", "limit"],
                    "msg": "value is not a valid integer",
                    "type": "type_error.integer",
                },
            ],
        },
        id="skip & limit are not numbers",
    ),
    pytest.param(
        {"skip": "-2", "limit": "-1"},
        422,
        {
            "detail": [
                {
                    "loc": ["query", "skip"],
                    "msg": "ensure this value is greater than or equal to 0",
                    "type": "value_error.number.not_ge",
                    "ctx": {"limit_value": 0},
                },
                {
                    "loc": ["query", "limit"],
                    "msg": "ensure this value is greater than or equal to 0",
                    "type": "value_error.number.not_ge",
                    "ctx": {"limit_value": 0},
                },
            ]
        },
        id="skip & limit are too small",
    ),
    pytest.param(
        {"query": "key="},
        422,
        {
            "detail": [
                {
                    "loc": ["query", "query"],
                    "msg": "query cannot be parsed",
                    "type": "parse_error.query",
                    "char_index": 3,
                },
            ],
        },
        id="invalid query",
    ),
    pytest.param(
        {"query": '["tag"]'},
        422,
        {
            "detail": [
                {
                    "loc": ["query", "query"],
                    "msg": "tag missing 1 required positional argument: 'tag'",
                    "type": "parse_error.query",
                    "value": None,
                },
            ],
        },
        id="query invalid struct",
    ),
]


@pytest.mark.parametrize(
    ("params", "status", "response_json"), NOTE_SELECT_FIELDS_TESTCASES
)
def test_api_notes_get(
    test_client, memory_storage_backend, params, status, response_json
):
    response = test_client.get("/api/v2/notes", params=params)
    assert response.status_code == status
    assert response.json() == response_json


@pytest.mark.parametrize(
    ("params",),
    [
        pytest.param(
            {"query": "hello", "limit": 1, "random": "yes"},
            id="random passed as a GET argument",
        ),
        pytest.param(
            {"query": "hello :RANDOM", "limit": 1},
            id="random passed in query",
        ),
        pytest.param(
            {"query": '["and", [["tag", "hello"], ["tag", ":RANDOM"]]]', "limit": 1},
            id="random passed in struct query",
        ),
        pytest.param(
            {"query": '["and", [["all"], ["nested", ":RANDOM hello"]]]', "limit": 1},
            id="random passed in struct query inside nested query",
        ),
    ],
)
def test_api_notes_get_random(test_client, memory_storage_backend, params):
    result_nids = set()
    for i in range(30):
        response = test_client.get("/api/v2/notes", params=params)
        assert response.status_code == 200
        assert response.json()["random"] is True
        notes = response.json()["notes"]
        assert len(notes) == 1
        result_nids.add(notes[0]["nid"])
        if len(result_nids) == 2:
            break
    assert {NOTE_2_JSON["nid"], NOTE_1_JSON["nid"]} == result_nids
