import hashlib

import pytest  # type: ignore

from oleeka_api.api.auth import check_auth


def sha1(text: str) -> str:
    return hashlib.sha1(text.encode("utf-8")).hexdigest()


@pytest.mark.parametrize(
    ("user_auth", "expected"),
    [
        ({}, False),
        ({"auths": []}, False),
        ({"auths": [{}]}, False),
        ({"auths": [{"password": ""}]}, False),
        ({"auths": [{"password": "md5$"}]}, False),
        ({"auths": [{"password": "sha1$abc"}]}, False),
        ({"auths": [{"password": f"sha1$abc${sha1('foo')}"}]}, False),
        ({"auths": [{"password": f"sha1$abc${sha1('abcpass')}"}]}, True),
    ],
)
def test_check_auth(user_auth, expected):
    assert check_auth(user_auth, "pass") == expected
