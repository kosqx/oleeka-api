import pytest  # type: ignore

from oleeka_api.api.models import split_chunks, encode_data, decode_data


@pytest.mark.parametrize(
    ("text", "length", "expected"),
    [
        ("", 3, []),
        ("a", 3, ["a"]),
        ("ab", 3, ["ab"]),
        ("abc", 3, ["abc"]),
        ("abcd", 3, ["abc", "d"]),
        ("abcde", 3, ["abc", "de"]),
        ("abcdef", 3, ["abc", "def"]),
        ("abcdefg", 3, ["abc", "def", "g"]),
    ],
)
def test_file_encoding_split_chunks(text, length, expected):
    assert split_chunks(text, length) == expected


DATA_ENCODING_TESTCASES = [
    (None, "omitted", None),
    (b"abc", "text", ["abc"]),
    (b"a b\tc", "text", ["a b\tc"]),
    (b"a b\ac", "base64", ["YSBiB2M="]),
    (b"\x00\x01\x02", "base64", ["AAEC"]),
]
DATA_ONLY_DECODE_TESTCASES = [
    (b"abc", "text", "abc"),
    (b"a b\tc", "text", "a b\tc"),
    (b"a b\ac", "base64", "YSBiB2M="),
    (b"\x00\x01\x02", "base64", "AAEC"),
]


@pytest.mark.parametrize(("in_data", "encoding", "data"), DATA_ENCODING_TESTCASES)
def test_encode_data(in_data, encoding, data):
    assert encode_data(in_data) == (encoding, data)


@pytest.mark.parametrize(
    ("in_data", "encoding", "data"),
    DATA_ENCODING_TESTCASES + DATA_ONLY_DECODE_TESTCASES,  # type: ignore
)
def test_decode_data(in_data, encoding, data):
    result = decode_data(encoding, data)
    assert result == in_data
    assert isinstance(result, bytes) or result is None
