import base64
import dataclasses
import os
import re
from contextlib import contextmanager
from typing import Any, Callable, List, Set, Optional, Type, Union, Pattern, get_args


from oleeka_api.query.terms import Term, Tag, Attr, Mark, Nested, ALL_TERM_CLASSES


_MatchesCondition = Union[Type[Any], Callable[[Any], bool], Pattern]


class Matches:
    def __init__(self, *conditions: _MatchesCondition):
        self._conditions = conditions

    @classmethod
    def _match(cls, condition: _MatchesCondition, value: Any) -> bool:
        if type(condition) == type:
            return isinstance(value, condition)
        elif isinstance(condition, re.Pattern):
            return isinstance(value, str) and condition.match(value) is not None
        elif callable(condition):
            return condition(value)
        else:
            return False

    def __eq__(self, other):
        return all(self._match(cond, other) for cond in self._conditions)

    @classmethod
    def regex(cls, pattern: str) -> "Matches":
        return cls(re.compile(pattern))

    @classmethod
    def contains(cls, *items: Any) -> "Matches":
        def contains_all_items(values):
            for value in values:
                if value not in items:
                    print("Matches.contains additional:", value)
            return all(item in values for item in items)

        return cls(contains_all_items)

    @classmethod
    def datetime_str(cls) -> "Matches":
        return cls(re.compile(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$"))

    @classmethod
    def nid(cls) -> "Matches":
        return cls(re.compile(r"^\d{8}T\d{12}R\d{6}$"))

    @classmethod
    def fid(cls) -> "Matches":
        return cls(re.compile(r"^F[0-9TR]+$"))


@contextmanager
def set_envvar(name: str, value):
    old_value = os.environ.get(name)
    os.environ[name] = value
    try:
        yield value
    finally:
        if old_value is None:
            del os.environ[name]
        else:
            os.environ[name] = old_value


def to_base64(value: str) -> str:
    return base64.b64encode(value.encode("utf-8")).decode("ascii")


def get_dataclass_field(dataclass, field_name: str) -> dataclasses.Field:
    return {field.name: field for field in dataclasses.fields(dataclass)}[field_name]


def get_literal_values(literal) -> Set[Any]:
    return set(get_args(literal))


def get_dataclass_field_literal_values(dataclass, field_name: str) -> Set[Any]:
    return get_literal_values(get_dataclass_field(dataclass, field_name).type)


def _get_terms(testcases, index: Optional[int] = None):
    return [testcase if index is None else testcase[index] for testcase in testcases]


def _describe_term_class(term: Type[Term]) -> List[str]:
    if issubclass(term, Tag):
        return [f"tag.{mode}" for mode in Tag.MODES]
    if issubclass(term, Attr):
        return [f"attr.{operator}" for operator in Attr.OPERATORS]
    if issubclass(term, Mark):
        features = [f"mark.{feature}" for feature in Mark.FEATURES]
        operators = [f"mark.{operator}" for operator in Mark.OPERATORS]
        return features + operators
    return [term.get_term_name()]


def _describe_term_instance(term: Term) -> List[str]:
    if isinstance(term, Tag):
        return [f"tag.{term.mode}"]
    if isinstance(term, Attr):
        return [f"attr.{term.operator}"]
    if isinstance(term, Mark):
        return [f"mark.{term.feature}", f"mark.{term.operator}"]
    return [term.get_term_name()]


def get_all_terms(term_classes, exclude_classes) -> Set[str]:
    classes = [cls for cls in term_classes if cls not in exclude_classes]
    return set(sum([_describe_term_class(i) for i in classes], []))


def get_all_db_terms() -> Set[str]:
    return get_all_terms(ALL_TERM_CLASSES, [Nested])


def get_all_parsable_terms() -> Set[str]:
    return get_all_terms(ALL_TERM_CLASSES, [Mark, Nested])


def get_covered_terms(testcases, index: Optional[int] = None) -> Set[str]:
    terms = _get_terms(testcases, index)
    return set(sum([_describe_term_instance(term) for term in terms], []))
