import pytest  # type: ignore

from oleeka_api.models import File, Note, SelectFields
from tests.data import NOTE_2, NOTE_2_NO_DATA


def test_file_from_data():
    assert File.from_data("hello.py", "print('Hello!')", fid="F1") == File(
        fid="F1",
        name="hello.py",
        hash="701815cb03468f5f52b9ffc9a7abe86c8996bea6",
        mime="text/x-python",
        size=15,
        meta={},
        data=b"print('Hello!')",
    )


NOTE_SELECT_FIELDS_TESTCASES = [
    (SelectFields.only_nid(), Note(nid=NOTE_2.nid)),
    (SelectFields.no_files(), NOTE_2.replace(files=[])),
    (SelectFields(), NOTE_2_NO_DATA),
    (SelectFields.everything(), NOTE_2),
    (
        SelectFields.only(attrs={"created", "link"}),
        Note(nid=NOTE_2.nid, attrs={"created": NOTE_2.attrs["created"]}),
    ),
]


@pytest.mark.parametrize(("select_fields", "expected"), NOTE_SELECT_FIELDS_TESTCASES)
def test_note_select_fields(select_fields, expected):
    assert NOTE_2.select_fields(select_fields) == expected


SELECT_FIELDS_FORMAT_TESTCASES = [
    (SelectFields.only_nid(), "nid"),
    (SelectFields.no_files(), "nid,text,tags,attrs"),
    (SelectFields(), "nid,text,tags,attrs,files"),
    (SelectFields.everything(), "nid,text,tags,attrs,files,files_data"),
    (SelectFields.only(attrs={"created", "link"}), "nid,attrs.created,attrs.link"),
]


@pytest.mark.parametrize(("select_fields", "formatted"), SELECT_FIELDS_FORMAT_TESTCASES)
def test_select_fields_format(select_fields: SelectFields, formatted: str):
    assert select_fields.format() == formatted


SELECT_FIELDS_ONLY_PARSE_TESTCASES = [
    (SelectFields(), ""),
    (SelectFields.no_files(), "nid, text, tags, attrs"),
    (SelectFields.no_files(), "attrs,nid,tags,text"),
    (SelectFields.no_files(), "nid,text,tags,attrs,something,extra"),
    (SelectFields.only(attrs={"link"}), "nid,attrs,attrs.link"),
]


@pytest.mark.parametrize(
    ("select_fields", "formatted"),
    SELECT_FIELDS_FORMAT_TESTCASES + SELECT_FIELDS_ONLY_PARSE_TESTCASES,
)
def test_select_fields_parse(select_fields: SelectFields, formatted: str):
    assert SelectFields.parse(formatted) == select_fields
