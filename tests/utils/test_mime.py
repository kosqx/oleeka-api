import pytest  # type: ignore

from oleeka_api.utils.mime import guess_mime, is_text


@pytest.mark.parametrize(
    ("filename", "filedata", "mime"),
    [
        # standard types
        ("foo.jpg", None, "image/jpeg"),
        ("foo.png", None, "image/png"),
        ("foo.zip", None, "application/zip"),
        # overriding standard types
        ("foo.ts", None, "application/x-typescript"),
        ("foo.tsx", None, "application/x-typescript"),
        # handling file data
        ("foo.ole", None, "application/octet-stream"),
        ("foo.ole", "foo", "text/plain"),
        ("foo.ole", b"foo", "text/plain"),
        ("foo.ole", b"f\0", "application/octet-stream"),
    ],
)
def test_guess_mime(filename, filedata, mime):
    assert guess_mime(filename, filedata) == mime


@pytest.mark.parametrize(
    ("data", "expected"),
    [
        (b"", True),
        (b"foo bar", True),
        (b"foo\abar", False),
        (b"\xc4\x85", True),
        (b"\xc4", False),
    ],
)
def test_is_text(data, expected):
    assert is_text(data) == expected
