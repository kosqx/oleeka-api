import pytest  # type: ignore

from oleeka_api.utils.http import (
    format_server_timing,
    is_http_token,
    ServerTimingMetric,
)


@pytest.mark.parametrize(
    ("text", "expected"),
    [
        (None, False),
        ("", False),
        ("valid-token", True),
        ("Valid-Token!", True),
        ("not token", False),
    ],
)
def test_is_http_token(text, expected):
    assert is_http_token(text) == expected


def test_format_server_timings():
    assert (
        format_server_timing(
            ServerTimingMetric("cpu", 1.234),
            ServerTimingMetric("db", 0.234567, description="info"),
            ServerTimingMetric("db", 0.123456789, description="long info"),
        )
        == 'cpu;dur=1234.000, db;dur=234.567;desc=info, db;dur=123.457;desc="long info"'
    )
