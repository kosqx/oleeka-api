import string

import pytest  # type: ignore

from oleeka_api.utils.text import get_random_string, get_random_hex
from oleeka_api.utils.text import is_number, cast_number


HEXDIGITS = "0123456789abcdef"


@pytest.mark.parametrize(
    ("digits", "lowercase", "uppercase", "expected"),
    [
        (True, False, False, string.digits),
        (True, True, False, string.digits + string.ascii_lowercase),
        (True, True, True, string.digits + string.ascii_letters),
    ],
)
def test_get_random_string(digits, lowercase, uppercase, expected):
    value = get_random_string(
        1000, digits=digits, lowercase=lowercase, uppercase=uppercase
    )
    assert len(value) == 1000
    assert set(value) == set(expected)


def test_get_random_hex():
    value = get_random_hex(1000)
    assert len(value) == 1000
    assert set(value) == set("0123456789abcdef")


NUMBER_TESTCASES = [
    ("", None, False),
    ("abc", None, False),
    ("123", 123, True),
    ("3.14", 3.14, True),
    ("3.0", 3.0, True),
    ("3.", 3.0, True),
    (".14", 0.14, True),
]


@pytest.mark.parametrize(
    ("value", "expected_cast", "expected_is_number"),
    NUMBER_TESTCASES,
)
def test_is_number(value, expected_cast, expected_is_number):
    assert is_number(value) == expected_is_number


@pytest.mark.parametrize(
    ("value", "expected_cast", "expected_is_number"),
    NUMBER_TESTCASES,
)
def test_cast_number(value, expected_cast, expected_is_number):
    result = cast_number(value)
    assert result == expected_cast
    assert type(result) is type(expected_cast)
