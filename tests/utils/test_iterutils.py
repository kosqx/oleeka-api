import pytest  # type: ignore

from oleeka_api.utils.iterutils import build_dict_of_lists, chunks


@pytest.mark.parametrize(
    ("sequence", "length", "expected"),
    [
        ("", 3, []),
        ("abcdef", 3, [["a", "b", "c"], ["d", "e", "f"]]),
        ("abcdefgh", 3, [["a", "b", "c"], ["d", "e", "f"], ["g", "h"]]),
        (["a", "b", "c", "d"], 3, [["a", "b", "c"], ["d"]]),
    ],
)
def test_chunks(sequence, length, expected):
    assert list(chunks(sequence, length)) == expected


@pytest.mark.parametrize(
    ("sequence", "expected"),
    [
        ([], {}),
        ([(1, 2)], {1: [2]}),
        ([(1, 2), (3, 4), (1, 6)], {1: [2, 6], 3: [4]}),
        (((abs(i), i) for i in range(-2, 3)), {0: [0], 1: [-1, 1], 2: [-2, 2]}),
    ],
)
def test_build_dict_of_lists(sequence, expected):
    assert build_dict_of_lists(sequence) == expected
