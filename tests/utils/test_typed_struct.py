import pytest  # type: ignore

import dataclasses
import datetime
import enum
import typing

from oleeka_api.utils.typed_struct import TypedStruct


@dataclasses.dataclass
class Person:
    name: str
    is_adult: bool = True


@dataclasses.dataclass
class Family:
    name: str
    people: typing.List[Person] = dataclasses.field(default_factory=list)


class Color(enum.Enum):
    RED = "r"
    GREEN = "g"
    BLUE = "b"


class Numbers(enum.Enum):
    ONE = 1
    TWO = 2


TYPED_STRUCT_TEST_CASES: typing.List[
    typing.Tuple[typing.Any, typing.Any, typing.Any]
] = [
    # None
    (type(None), None, None),
    # bool
    (bool, True, True),
    (bool, "", TypeError("")),
    # int
    (int, 123, 123),
    (int, 123.45, 123),
    # float
    (float, 123, 123.0),
    (float, 123.45, 123.45),
    # str
    (str, "foo", "foo"),
    # datetime
    (
        datetime.datetime,
        "2022-01-02T12:34:56",
        datetime.datetime(2022, 1, 2, 12, 34, 56),
    ),
    (
        datetime.datetime,
        "2022-01-02T12:34:56.789Z",
        datetime.datetime(2022, 1, 2, 12, 34, 56, 789_000),
    ),
    (
        datetime.datetime,
        "2022-01-02",
        datetime.datetime(2022, 1, 2),
    ),
    # Optional
    (typing.Optional[int], None, None),
    (typing.Optional[int], 123, 123),
    (typing.Optional[int], "a", TypeError("no type in Union matched")),
    # Enum
    (Color, "g", Color.GREEN),
    (Color, "c", ValueError("'c' is not a valid Color")),
    (Numbers, 2, Numbers.TWO),
    (Numbers, 0, ValueError("0 is not a valid Numbers")),
    # dataclass
    (
        Person,
        {},
        TypeError("Person.__init__() missing 1 required positional argument: 'name'"),
    ),
    (Person, {"name": "Tom"}, Person(name="Tom", is_adult=True)),
    (Person, {"name": "Tom", "is_adult": False}, Person(name="Tom", is_adult=False)),
    (
        Person,
        {},
        TypeError("Person.__init__() missing 1 required positional argument: 'name'"),
    ),
    (Person, {"name": 123, "is_adult": False}, TypeError("")),
    (
        Family,
        {"name": "branch", "people": [{"name": "A"}, {"name": "B"}]},
        Family(
            name="branch",
            people=[Person(name="A", is_adult=True), Person(name="B", is_adult=True)],
        ),
    ),
    # list
    (list[int], [1, 2, 3], [1, 2, 3]),
    (typing.List[int], [1, 2, 3], [1, 2, 3]),
    (list[int], [1, 2, "abc"], TypeError("")),
    # dict
    (dict[int, int], {1: 2, 3: 4}, {1: 2, 3: 4}),
    (typing.Dict[int, int], {1: 2, 3: 4}, {1: 2, 3: 4}),
    # tuple
    (typing.Tuple[int, str], (1, ""), (1, "")),
    (typing.Tuple[int, str], [1, ""], (1, "")),
    (typing.Tuple[int, str], ["", 1], TypeError("")),
    # Union
    (typing.Union[int, str], 123, 123),
    (typing.Union[int, str], "a", "a"),
    (typing.Union[int, str], [], TypeError("no type in Union matched")),
    # Literal
    (typing.Literal[1, 2], 1, 1),
    (typing.Literal[1, 2], 2, 2),
    (typing.Literal[1, 2], 3, TypeError("")),
    (typing.Union[typing.Literal[1], typing.Literal[2]], 2, 2),
]


@pytest.mark.parametrize(
    ("typ", "data", "expected"),
    TYPED_STRUCT_TEST_CASES,
)
def test_typed_struct_load(typ, data, expected):
    if isinstance(expected, Exception):
        with pytest.raises(type(expected)) as exc:
            TypedStruct(typ).load(data)
        assert exc.value.args == expected.args
    else:
        assert TypedStruct(typ).load(data) == expected
