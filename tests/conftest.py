import pytest  # type: ignore
from fastapi.testclient import TestClient

from oleeka_api.api.auth import create_access_token, Sha1Password
from oleeka_api.main import app
from oleeka_api.models import UserAccount
from oleeka_api.storage.base import StorageBackend
from oleeka_api.storage.memory.backend import MemoryStorageBackend

from tests.data import NOTE_1, NOTE_2, NOTE_3
from tests.helpers import set_envvar, to_base64


@pytest.fixture(scope="function")
def jwt_headers():
    token = create_access_token(data={"sub": "testuser"})
    return {"Authorization": f"Bearer {token}"}


@pytest.fixture(scope="function")
def test_client(jwt_headers):
    client = TestClient(app)
    client.headers.update(jwt_headers)
    return client


@pytest.fixture(scope="function")
def test_client_unauthorized():
    return TestClient(app)


@pytest.fixture(scope="function")
def test_client_basic_auth():
    auth = to_base64("testuser:XYZ123")
    client = TestClient(app)
    client.headers.update({"Authorization": f"Basic {auth}"})
    return client


async def create_backend(insert_notes: bool):
    backend = MemoryStorageBackend()
    user_1 = UserAccount(
        username="testuser",
        auth={
            "auths": [
                {"password": Sha1Password.generate("testpass")},
                {"token": "XYZ123"},
            ]
        },
    )
    if insert_notes:
        await backend.testing_insert(user_1, NOTE_1, NOTE_2, NOTE_3)
    else:
        await backend.testing_insert(user_1)

    return backend


@pytest.fixture(scope="function")
async def storage_backend_empty():
    with set_envvar(StorageBackend.URL_ENVVAR, "memory://"):
        yield await create_backend(False)


@pytest.fixture(scope="function")
async def memory_storage_backend():
    with set_envvar(StorageBackend.URL_ENVVAR, "memory://"):
        yield await create_backend(True)
