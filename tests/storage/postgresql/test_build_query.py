import pytest  # type: ignore

from oleeka_api.models import Query, SelectFields
from oleeka_api.query.terms import Text
from oleeka_api.storage.postgresql import build_query


@pytest.mark.parametrize(
    "table, fields, returning, query, values",
    [
        (
            "files_data",
            {"hash": "cafe", "data": b"123"},
            None,
            "INSERT INTO files_data (hash, data) VALUES ($1, $2)",
            ["cafe", b"123"],
        ),
        (
            "notes",
            {"text": "content", "tags": ["foo", "bar"], "attrs": {"kind": "bookmark"}},
            "id",
            "INSERT INTO notes (text, tags, attrs) VALUES ($1, $2, $3::hstore) RETURNING id",
            ["content", ["foo", "bar"], {"kind": "bookmark"}],
        ),
    ],
)
def test_get_insert_query(table, fields, returning, query, values):
    assert build_query.insert(table, fields, returning=returning) == (query, values)


@pytest.mark.parametrize(
    ("query", "expected"),
    [
        (Query(), "WHERE (0 = 1)"),
        (Query(query_terms=Text("foo")), "WHERE (notes.text LIKE '%foo%')"),
        (Query(username="adam"), "WHERE (0 = 1) AND (notes.username = 'adam')"),
        (
            Query(username="adam", query_terms=Text("foo")),
            "WHERE (notes.text LIKE '%foo%') AND (notes.username = 'adam')",
        ),
    ],
)
def test_build_where(query, expected):
    assert build_query._build_where(query) == expected


@pytest.mark.parametrize(
    ("select_fields", "expected"),
    [
        (SelectFields(), "id, nid, text, tags, attrs"),
        (SelectFields.only_nid(), "id, nid"),
        (SelectFields.no_files(), "id, nid, text, tags, attrs"),
        (SelectFields.everything(), "id, nid, text, tags, attrs"),
        (
            SelectFields.only(attrs={"created", "title"}),
            "id, nid, slice(attrs, ARRAY['created', 'title']) AS attrs",
        ),
        (
            SelectFields(attrs={"created", "title"}),
            "id, nid, text, tags, slice(attrs, ARRAY['created', 'title']) AS attrs",
        ),
    ],
)
def test_build_note_projection(select_fields, expected):
    assert build_query._build_note_projection(select_fields) == expected
