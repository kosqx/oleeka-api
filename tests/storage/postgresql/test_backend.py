from oleeka_api.storage.postgresql.backend import parse_postgresql_url


def test_parse_postgresql_url():
    url = "postgresql://user:pass@127.0.0.1:54321/dbname"
    assert parse_postgresql_url(url) == {
        "host": "127.0.0.1",
        "port": 54321,
        "user": "user",
        "password": "pass",
        "database": "dbname",
    }
