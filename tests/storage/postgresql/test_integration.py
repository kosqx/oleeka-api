import asyncio

import pytest  # type: ignore

from oleeka_api.models import UserAccount, PaginatedQuery
from oleeka_api.query.terms import All
from oleeka_api.storage.base import StorageBackend
from oleeka_api.storage.postgresql.backend import PostgresqlStorageBackend

from tests.data import NOTE_1, NOTE_2, NOTE_2_NO_DATA, NOTE_3
from tests.helpers import set_envvar


URL = "postgresql://{0}:{0}@127.0.0.1:5432/{0}".format("oleeka_unittest")


@pytest.fixture(scope="module")
def event_loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module")
async def pg_storage_backend(event_loop):
    with set_envvar(StorageBackend.URL_ENVVAR, URL):
        async with PostgresqlStorageBackend.acquire_backend() as backend:
            yield backend
    await PostgresqlStorageBackend.testing_deinit()


@pytest.mark.asyncio
async def test_postgresql_backend(pg_storage_backend: PostgresqlStorageBackend):
    query_all = PaginatedQuery(query_terms=All())

    user_1 = UserAccount(username="testuser")
    await pg_storage_backend.testing_insert(user_1, NOTE_1, NOTE_2, NOTE_3)

    assert await pg_storage_backend.get_user("non-user") is None
    assert await pg_storage_backend.get_user("testuser") == user_1

    all_notes = [NOTE_3, NOTE_2_NO_DATA, NOTE_1]
    assert await pg_storage_backend.get_notes(query_all) == all_notes

    assert await pg_storage_backend.get_notes(
        PaginatedQuery(query_terms=All(), skip=1, limit=1)
    ) == [NOTE_2_NO_DATA]
