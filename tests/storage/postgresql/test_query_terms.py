import pytest  # type: ignore

from oleeka_api.query.terms import Term, All, Nid, Tag, Text, Attr, Mark
from oleeka_api.query.terms import Not, And, Or, Nested
from oleeka_api.storage.postgresql.query_terms import to_sql

from tests.helpers import get_all_db_terms, get_covered_terms


NID_1 = "20201113T175543290880R684769"
NID_2 = "20201113T175554625048R173053"
TERM_TO_SQL_TESTCASES = [
    (Term(), "(0 = 1)"),
    (All(), "(1 = 1)"),
    (Nid([]), "(1 = 0)"),
    (Nid([NID_1]), f"(notes.nid IN ('{NID_1}'))"),
    (Nid([NID_1, NID_2]), f"(notes.nid IN ('{NID_1}', '{NID_2}'))"),
    (Text("foo"), "(notes.text LIKE '%foo%')"),
    # Tag
    (Tag("foo"), "(notes.tags @> ARRAY['foo']::text[])"),
    (Tag("foo", mode="exact"), "(notes.tags @> ARRAY['foo']::text[])"),
    (
        Tag("foo", mode="prefix"),
        "EXISTS (SELECT 1 FROM (SELECT unnest(notes.tags)) x(tag) WHERE x.tag LIKE 'foo%')",
    ),
    (
        Tag("foo", mode="suffix"),
        "EXISTS (SELECT 1 FROM (SELECT unnest(notes.tags)) x(tag) WHERE x.tag LIKE '%foo')",
    ),
    (
        Tag("foo", mode="infix"),
        "EXISTS (SELECT 1 FROM (SELECT unnest(notes.tags)) x(tag) WHERE x.tag LIKE '%foo%')",
    ),
    # Attr - text quoting
    (Attr("foo", "===", "I'd"), "(notes.attrs->'foo' LIKE 'I''d')"),
    (Attr("foo", "===", "'foo'"), "(notes.attrs->'foo' LIKE '''foo''')"),
    (Attr("foo", "=", "'foo'"), "(notes.attrs->'foo' ILIKE '%''foo''%')"),
    # Attr - all operators
    (Attr("foo", "?", ""), "(notes.attrs->'foo' IS NOT NULL)"),
    (Attr("foo", "=", "bar"), "(notes.attrs->'foo' ILIKE '%bar%')"),
    (Attr("foo", "==", "bar"), "(notes.attrs->'foo' ILIKE 'bar')"),
    (Attr("foo", "===", "bar"), "(notes.attrs->'foo' LIKE 'bar')"),
    (Attr("foo", "^=", "bar"), "(notes.attrs->'foo' ILIKE 'bar%')"),
    (Attr("foo", "^==", "bar"), "(notes.attrs->'foo' LIKE 'bar%')"),
    (Attr("foo", "$=", "bar"), "(notes.attrs->'foo' ILIKE '%bar')"),
    (Attr("foo", "$==", "bar"), "(notes.attrs->'foo' LIKE '%bar')"),
    (Attr("foo", "*=", "bar"), "(notes.attrs->'foo' ILIKE '%bar%')"),
    (Attr("foo", "*==", "bar"), "(notes.attrs->'foo' LIKE '%bar%')"),
    (Attr("foo", "+=", "bar"), "(notes.attrs->'foo' ~* '.*\\ybar\\y.*')"),
    (Attr("foo", "+==", "bar"), "(notes.attrs->'foo' ~ '.*\\ybar\\y.*')"),
    (
        Attr("foo", "<=", "bar"),
        "(UPPER(notes.attrs->'foo') COLLATE \"C\" <= UPPER('bar'))",
    ),
    (Attr("foo", "<==", "bar"), "(notes.attrs->'foo' COLLATE \"C\" <= 'bar')"),
    (
        Attr("foo", ">=", "bar"),
        "(UPPER(notes.attrs->'foo') COLLATE \"C\" >= UPPER('bar'))",
    ),
    (Attr("foo", ">==", "bar"), "(notes.attrs->'foo' COLLATE \"C\" >= 'bar')"),
    (Attr("foo", "<<=", "bar"), "(1 = 2)"),
    (
        Attr("foo", "<<=", "0.1"),
        "COALESCE(try_cast_float(notes.attrs->'foo') <= '0.1'::float8, FALSE)",
    ),
    (
        Attr("foo", "<<=", "023"),
        "COALESCE(try_cast_float(notes.attrs->'foo') <= '023'::float8, FALSE)",
    ),
    (Attr("foo", ">>=", "bar"), "(1 = 2)"),
    (
        Attr("foo", ">>=", "42."),
        "COALESCE(try_cast_float(notes.attrs->'foo') >= '42.'::float8, FALSE)",
    ),
    # Mark
    (Mark("tags_count", "==", 1), "(cardinality(notes.tags) = 1)"),
    (Mark("tags_count", "!=", 1), "(cardinality(notes.tags) != 1)"),
    (Mark("tags_count", "<", 1), "(cardinality(notes.tags) < 1)"),
    (Mark("tags_count", "<=", 1), "(cardinality(notes.tags) <= 1)"),
    (Mark("tags_count", ">", 1), "(cardinality(notes.tags) > 1)"),
    (Mark("tags_count", ">=", 1), "(cardinality(notes.tags) >= 1)"),
    (Mark("attrs_count", "==", 2), "(cardinality(akeys(notes.attrs)) = 2)"),
    (
        Mark("files_count", "==", 1),
        "((SELECT count(*) FROM files WHERE notes.id = note_id) = 1)",
    ),
    (
        Mark("files_size", "==", 1),
        "((SELECT coalesce(sum(size), 0) FROM files WHERE notes.id = note_id) = 1)",
    ),
    (
        Mark("modified_after", "<", 60),
        "(extract(epoch FROM (notes.attrs->'modified')::timestamp - "
        "(notes.attrs->'created')::timestamp) < 60)",
    ),
    (
        Mark("date_day_of_week", "==", 1),
        "(extract(isodow FROM (notes.attrs->'created')::timestamp) = 1)",
    ),
    (
        Mark("date_day_of_year", "==", 1),
        "(extract(doy FROM (notes.attrs->'created')::timestamp) = 1)",
    ),
    (
        Mark("date_week_of_year", "==", 1),
        "(extract(week FROM (notes.attrs->'created')::timestamp) = 1)",
    ),
    # logical connectives
    (Not(All()), "(NOT (1 = 1))"),
    (And([All(), Term()]), "((1 = 1) AND (0 = 1))"),
    (Or([All(), Term()]), "((1 = 1) OR (0 = 1))"),
]


@pytest.mark.parametrize(("term", "sql"), TERM_TO_SQL_TESTCASES)
def test_term_to_sql(term: Term, sql: str):
    assert to_sql(term) == sql


def test_postgresql_testcases_cover_all_terms():
    assert get_covered_terms(TERM_TO_SQL_TESTCASES, 0) == get_all_db_terms()


def test_term_to_sql_benchmark():
    term = And([Term(), All(), Text("foo"), Tag("bar")])
    term = Or([term, term, term, term])
    for i in range(1000):
        to_sql(term)


def test_term_to_sql_fails_for_nested():
    with pytest.raises(KeyError):
        to_sql(Nested("foo"))
