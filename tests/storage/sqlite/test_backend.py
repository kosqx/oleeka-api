import pytest  # type: ignore

from oleeka_api.query.terms import Term, All, Nid, Text, Tag, Attr, Not, And, Or
from oleeka_api.storage.base import StorageBackend
from oleeka_api.storage.sqlite.backend import SqliteStorageBackend
from oleeka_api.models import Note, UserAccount, PaginatedQuery, SelectFields

from tests.data import NOTE_1, NOTE_2, NOTE_2_NO_DATA, NOTE_3
from tests.helpers import set_envvar


@pytest.fixture(scope="function")
def sqlite_storage_backend():
    with set_envvar(StorageBackend.URL_ENVVAR, "sqlite://:memory:"):
        yield SqliteStorageBackend()


@pytest.mark.asyncio
async def test_sqlite_backend(sqlite_storage_backend: SqliteStorageBackend):
    query_all = PaginatedQuery(query_terms=All())
    assert await sqlite_storage_backend.get_user("testuser") is None
    assert await sqlite_storage_backend.get_notes(query_all) == []

    user_1 = UserAccount(username="testuser")
    await sqlite_storage_backend.testing_insert(user_1, NOTE_1, NOTE_2, NOTE_3)

    assert await sqlite_storage_backend.get_user("non-user") is None
    assert await sqlite_storage_backend.get_user("testuser") == user_1

    all_notes = [NOTE_3, NOTE_2_NO_DATA, NOTE_1]
    assert await sqlite_storage_backend.get_notes(query_all) == all_notes

    assert await sqlite_storage_backend.get_notes(
        PaginatedQuery(query_terms=All(), skip=1, limit=1)
    ) == [NOTE_2_NO_DATA]


GET_NOTES_TESTCASES = [
    (Term(), []),
    (All(), [NOTE_3, NOTE_2_NO_DATA, NOTE_1]),
    (Nid([NOTE_2.nid]), [NOTE_2_NO_DATA]),
    (Text("first"), [NOTE_1]),
    (Tag("hello"), [NOTE_2_NO_DATA, NOTE_1]),
    (Attr("link", "?", ""), [NOTE_3]),
    (Attr("created", "$=", ":54Z"), [NOTE_3, NOTE_2_NO_DATA]),
    (Not(Nid([NOTE_2.nid])), [NOTE_3, NOTE_1]),
    (And([Text("first"), Tag("hello")]), [NOTE_1]),
    (Or([Text("first"), Nid([NOTE_3.nid])]), [NOTE_3, NOTE_1]),
]


@pytest.mark.parametrize(("query_terms", "expected_notes"), GET_NOTES_TESTCASES)
@pytest.mark.asyncio
async def test_sqlite_backend_get_notes(
    sqlite_storage_backend, query_terms, expected_notes
):
    user_1 = UserAccount(username="testuser")
    await sqlite_storage_backend.testing_insert(user_1, NOTE_1, NOTE_2, NOTE_3)
    query = PaginatedQuery(query_terms=query_terms)

    assert await sqlite_storage_backend.get_notes(query) == expected_notes


GET_NOTES_SELECT_FIELDS_TESTCASES = [
    (SelectFields.only_nid(), [Note(nid=NOTE_2.nid)]),
    (SelectFields.no_files(), [NOTE_2.replace(files=[])]),
    (SelectFields(), [NOTE_2_NO_DATA]),
    (SelectFields.everything(), [NOTE_2]),
]


@pytest.mark.parametrize(
    ("select_fields", "expected"), GET_NOTES_SELECT_FIELDS_TESTCASES
)
@pytest.mark.asyncio
async def test_sqlite_select_fields(sqlite_storage_backend, select_fields, expected):
    await sqlite_storage_backend.testing_insert(NOTE_2)
    query = PaginatedQuery(query_terms=All(), select_fields=select_fields)

    assert await sqlite_storage_backend.get_notes(query) == expected
