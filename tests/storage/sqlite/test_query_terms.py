import pytest  # type: ignore

from oleeka_api.query.terms import Term, All, Nid, Tag, Text, Attr, Mark
from oleeka_api.query.terms import Not, And, Or, Nested
from oleeka_api.storage.sqlite.query_terms import to_sql

from tests.helpers import get_all_db_terms, get_covered_terms


NID_1 = "20201113T175543290880R684769"
NID_2 = "20201113T175554625048R173053"
TERM_TO_SQL_TESTCASES = [
    (Term(), "(0 = 1)"),
    (All(), "(1 = 1)"),
    (Nid([]), "(1 = 0)"),
    (Nid([NID_1]), f"(notes.nid IN ('{NID_1}'))"),
    (Nid([NID_1, NID_2]), f"(notes.nid IN ('{NID_1}', '{NID_2}'))"),
    (Text("foo"), "(notes.text LIKE '%foo%')"),
    # Tag
    (Tag("foo"), "(notes.tags LIKE '%\"foo\"%')"),
    (Tag("foo", mode="exact"), "(notes.tags LIKE '%\"foo\"%')"),
    (Tag("foo", mode="prefix"), "(notes.tags LIKE '%\"foo%')"),
    (Tag("foo", mode="suffix"), "(notes.tags LIKE '%foo\"%')"),
    (Tag("foo", mode="infix"), "(notes.tags LIKE '%foo%')"),
    # Attr - text quoting
    (Attr("foo", "===", "I'd"), "attr_match(notes.attrs, 'foo', '===', 'I''d')"),
    (Attr("foo", "===", "'foo'"), "attr_match(notes.attrs, 'foo', '===', '''foo''')"),
    (Attr("foo", "=", "'foo'"), "attr_match(notes.attrs, 'foo', '=', '''foo''')"),
    # Attr - all operators
    (Attr("foo", "?", ""), "attr_match(notes.attrs, 'foo', '?', '')"),
    (Attr("foo", "=", "bar"), "attr_match(notes.attrs, 'foo', '=', 'bar')"),
    (Attr("foo", "==", "bar"), "attr_match(notes.attrs, 'foo', '==', 'bar')"),
    (Attr("foo", "===", "bar"), "attr_match(notes.attrs, 'foo', '===', 'bar')"),
    (Attr("foo", "^=", "bar"), "attr_match(notes.attrs, 'foo', '^=', 'bar')"),
    (Attr("foo", "^==", "bar"), "attr_match(notes.attrs, 'foo', '^==', 'bar')"),
    (Attr("foo", "$=", "bar"), "attr_match(notes.attrs, 'foo', '$=', 'bar')"),
    (Attr("foo", "$==", "bar"), "attr_match(notes.attrs, 'foo', '$==', 'bar')"),
    (Attr("foo", "*=", "bar"), "attr_match(notes.attrs, 'foo', '*=', 'bar')"),
    (Attr("foo", "*==", "bar"), "attr_match(notes.attrs, 'foo', '*==', 'bar')"),
    (Attr("foo", "+=", "bar"), "attr_match(notes.attrs, 'foo', '+=', 'bar')"),
    (Attr("foo", "+==", "bar"), "attr_match(notes.attrs, 'foo', '+==', 'bar')"),
    (Attr("foo", "<=", "bar"), "attr_match(notes.attrs, 'foo', '<=', 'bar')"),
    (Attr("foo", "<==", "bar"), "attr_match(notes.attrs, 'foo', '<==', 'bar')"),
    (Attr("foo", ">=", "bar"), "attr_match(notes.attrs, 'foo', '>=', 'bar')"),
    (Attr("foo", ">==", "bar"), "attr_match(notes.attrs, 'foo', '>==', 'bar')"),
    (Attr("foo", "<<=", "bar"), "attr_match(notes.attrs, 'foo', '<<=', 'bar')"),
    (Attr("foo", "<<=", "0.1"), "attr_match(notes.attrs, 'foo', '<<=', '0.1')"),
    (Attr("foo", "<<=", "023"), "attr_match(notes.attrs, 'foo', '<<=', '023')"),
    (Attr("foo", ">>=", "bar"), "attr_match(notes.attrs, 'foo', '>>=', 'bar')"),
    (Attr("foo", ">>=", "42."), "attr_match(notes.attrs, 'foo', '>>=', '42.')"),
    # Mark
    (Mark("tags_count", "==", 1), "(json_array_length(notes.tags) == 1)"),
    (Mark("tags_count", "!=", 1), "(json_array_length(notes.tags) != 1)"),
    (Mark("tags_count", "<", 1), "(json_array_length(notes.tags) < 1)"),
    (Mark("tags_count", "<=", 1), "(json_array_length(notes.tags) <= 1)"),
    (Mark("tags_count", ">", 1), "(json_array_length(notes.tags) > 1)"),
    (Mark("tags_count", ">=", 1), "(json_array_length(notes.tags) >= 1)"),
    (
        Mark("attrs_count", "==", 2),
        "((SELECT count(*) FROM json_each(notes.attrs)) == 2)",
    ),
    (
        Mark("files_count", "==", 1),
        "((SELECT count(*) FROM files WHERE notes.id = note_id) == 1)",
    ),
    (
        Mark("files_size", "==", 1),
        "((SELECT coalesce(sum(size), 0) FROM files WHERE notes.id = note_id) == 1)",
    ),
    (
        Mark("modified_after", "<", 60),
        "((strftime('%s', json_extract(notes.attrs, '$.modified')) - "
        "strftime('%s', json_extract(notes.attrs, '$.created'))) < 60)",
    ),
    (
        Mark("date_day_of_week", "==", 1),
        "((7 - (7 - strftime('%w', json_extract(notes.attrs, '$.created'))) % 7) == 1)",
    ),
    (
        Mark("date_day_of_year", "==", 1),
        "((strftime('%j', json_extract(notes.attrs, '$.created')) * 1) == 1)",
    ),
    (
        Mark("date_week_of_year", "==", 1),
        "(mark_date_extract(notes.attrs, 'week_of_year') == 1)",
    ),
    # logical connectives
    (Not(All()), "(NOT (1 = 1))"),
    (And([All(), Term()]), "((1 = 1) AND (0 = 1))"),
    (Or([All(), Term()]), "((1 = 1) OR (0 = 1))"),
]


@pytest.mark.parametrize(("term", "sql"), TERM_TO_SQL_TESTCASES)
def test_term_to_sql(term: Term, sql: str):
    assert to_sql(term) == sql


def test_sqlite_testcases_cover_all_terms():
    assert get_covered_terms(TERM_TO_SQL_TESTCASES, 0) == get_all_db_terms()


def test_term_to_sql_fails_for_nested():
    with pytest.raises(KeyError):
        to_sql(Nested("foo"))
