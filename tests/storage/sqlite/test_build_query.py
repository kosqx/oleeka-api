import pytest  # type: ignore

from oleeka_api.models import Query, SelectFields
from oleeka_api.query.terms import Text
from oleeka_api.storage.sqlite import build_query


def test_get_insert_query():
    assert build_query.insert("files_data", {"hash": "cafe", "data": b"123"}) == (
        "INSERT INTO files_data (hash, data) VALUES (?, ?)",
        ("cafe", b"123"),
    )


def test_get_upsert_query():
    assert build_query.upsert(
        "files_data", {"hash": "cafe", "data": b"123"}, ["hash"], None
    ) == (
        "INSERT INTO files_data (hash, data) VALUES (?, ?)"
        " ON CONFLICT(hash) DO UPDATE SET data=excluded.data",
        ("cafe", b"123"),
    )


def test_get_upsert_query_returning():
    assert build_query.upsert(
        "files_data", {"hash": "cafe", "data": b"123"}, ["hash"], "id"
    ) == (
        "INSERT INTO files_data (hash, data) VALUES (?, ?)"
        " ON CONFLICT(hash) DO UPDATE SET data=excluded.data"
        " RETURNING id",
        ("cafe", b"123"),
    )


@pytest.mark.parametrize(
    ("select_fields", "expected"),
    [
        (SelectFields(), "id, nid, text, tags, attrs"),
        (SelectFields.only_nid(), "id, nid"),
        (SelectFields.no_files(), "id, nid, text, tags, attrs"),
        (SelectFields.everything(), "id, nid, text, tags, attrs"),
        (
            SelectFields.only(attrs={"created", "title"}),
            (
                "id, nid, "
                "json_object("
                "'created', json_extract(attrs, '$.\"created\"'), "
                "'title', json_extract(attrs, '$.\"title\"')"
                ") AS attrs"
            ),
        ),
        (
            SelectFields(attrs={"created", "title"}),
            (
                "id, nid, text, tags, "
                "json_object("
                "'created', json_extract(attrs, '$.\"created\"'), "
                "'title', json_extract(attrs, '$.\"title\"')"
                ") AS attrs"
            ),
        ),
    ],
)
def test_build_note_projection(select_fields, expected):
    assert build_query._build_note_projection(select_fields) == expected


@pytest.mark.parametrize(
    ("query", "expected"),
    [
        (Query(), "WHERE (0 = 1)"),
        (Query(query_terms=Text("foo")), "WHERE (notes.text LIKE '%foo%')"),
        (Query(username="adam"), "WHERE (0 = 1) AND (notes.username = 'adam')"),
        (
            Query(username="adam", query_terms=Text("foo")),
            "WHERE (notes.text LIKE '%foo%') AND (notes.username = 'adam')",
        ),
    ],
)
def test_build_where(query, expected):
    assert build_query._build_where(query) == expected


@pytest.mark.parametrize(
    ("username", "nids", "expected"),
    [
        (
            None,
            [],
            "DELETE FROM notes WHERE (0 = 1) RETURNING nid",
        ),
        (
            "foo",
            ["20201113T175543290880R684769", "20201113T182454852122R334524"],
            (
                "DELETE FROM notes WHERE "
                "(notes.nid IN ('20201113T175543290880R684769', '20201113T182454852122R334524'))"
                " AND "
                "(notes.username = 'foo') RETURNING nid"
            ),
        ),
    ],
)
def test_build_delete(username, nids, expected):
    assert build_query.delete_notes(username, nids) == expected
