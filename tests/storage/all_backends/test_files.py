import pytest  # type: ignore

from oleeka_api.logic.base import OleekaObjectMissingException
from oleeka_api.storage import Storage

from tests.data import NOTE_1, NOTE_2, USER_1
from tests.data import FILE_A, FILE_A_NO_DATA


@pytest.mark.parametrize(
    ("username", "nid", "fid", "fetch_data", "expected"),
    [
        (USER_1.username, NOTE_2.nid, FILE_A.fid, True, FILE_A),
        (USER_1.username, NOTE_2.nid, FILE_A.fid, False, FILE_A_NO_DATA),
    ],
)
@pytest.mark.asyncio
async def test_get_file(
    storage_instance: Storage, username, nid, fid, fetch_data, expected
):
    assert (
        await storage_instance.get_file(username, nid, fid, fetch_data=fetch_data)
        == expected
    )


@pytest.mark.parametrize(
    ("username", "nid", "fid"),
    [
        ("nonuser", NOTE_2.nid, FILE_A.fid),
        (USER_1.username, NOTE_1.nid, FILE_A.fid),
        (USER_1.username, NOTE_2.nid, "F144"),
    ],
)
@pytest.mark.asyncio
async def test_get_file_not_found(storage_instance: Storage, username, nid, fid):
    with pytest.raises(OleekaObjectMissingException) as exc:
        await storage_instance.get_file(username, nid, fid, fetch_data=False)
    assert exc.value.args == ("file not found", fid)
    assert exc.value.message == "file not found"
    assert exc.value.info == fid
