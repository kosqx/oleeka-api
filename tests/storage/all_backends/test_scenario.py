import pytest  # type: ignore

from oleeka_api.models import Note, File, PaginatedQuery, SelectFields
from oleeka_api.query.terms import All, Nid
from oleeka_api.storage import Storage


from tests.data import NOTE_1, NOTE_2, NOTE_3, USER_1


async def get_all_nids(storage_instance: Storage):
    notes = await storage_instance.get_notes(PaginatedQuery(query_terms=All()))
    return [note.nid for note in notes]


@pytest.mark.asyncio
async def test_storage_delete_notes(storage_instance: Storage):
    nid_1, nid_2, nid_3 = NOTE_1.nid, NOTE_2.nid, NOTE_3.nid
    nid_missing = "20210517T184619272389R346082"
    assert await get_all_nids(storage_instance) == [nid_3, nid_2, nid_1]

    deleted = await storage_instance.delete_notes(USER_1.username, [nid_2, nid_missing])
    assert deleted == {nid_2}

    assert await get_all_nids(storage_instance) == [nid_3, nid_1]


@pytest.mark.asyncio
async def test_storage_save_notes(storage_instance: Storage):
    NID = "20220511T175401674923R750611"
    ATTRS = {
        "created": "2022-05-11T17:54:01Z",
        "modified": "2022-05-11T17:54:01Z",
    }
    FID = "F20220511T175401674923R987654"
    # Create
    note = Note(nid=NID, attrs=ATTRS)
    await storage_instance.save_notes(USER_1.username, [note])

    assert await storage_instance.get_notes(
        PaginatedQuery(query_terms=Nid([note.nid]))
    ) == [
        Note(
            nid=NID,
            text="",
            tags=[],
            attrs=ATTRS,
            files=[],
        )
    ]

    # Update
    note_updated = note.replace(text="note text")
    await storage_instance.save_notes(USER_1.username, [note_updated])

    assert await storage_instance.get_notes(
        PaginatedQuery(query_terms=Nid([note.nid]))
    ) == [
        Note(
            nid=NID,
            text="note text",
            tags=[],
            attrs=ATTRS,
            files=[],
        )
    ]

    # Update - add file
    note_updated = note.replace(files=[File.from_data("a.txt", b"123", fid=FID)])
    await storage_instance.save_notes(USER_1.username, [note_updated])

    assert await storage_instance.get_notes(
        PaginatedQuery(
            query_terms=Nid([note.nid]), select_fields=SelectFields.everything()
        )
    ) == [
        Note(
            nid=NID,
            text="",
            tags=[],
            attrs=ATTRS,
            files=[File.from_data("a.txt", b"123", fid=FID)],
        )
    ]

    # Update - change file
    note_updated = note.replace(files=[File.from_data("b.txt", b"abc", fid=FID)])
    await storage_instance.save_notes(USER_1.username, [note_updated])

    notes = await storage_instance.get_notes(PaginatedQuery(query_terms=All()))
    for note2 in notes:
        print(note2)
    assert await storage_instance.get_notes(
        PaginatedQuery(
            query_terms=Nid([note.nid]), select_fields=SelectFields.everything()
        )
    ) == [
        Note(
            nid=NID,
            text="",
            tags=[],
            attrs=ATTRS,
            files=[File.from_data("b.txt", b"abc", fid=FID)],
        )
    ]
