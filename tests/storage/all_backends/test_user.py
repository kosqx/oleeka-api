import pytest  # type: ignore

from oleeka_api.models import UserAccount
from oleeka_api.storage import Storage


@pytest.mark.asyncio
async def test_user_scenario(storage_instance: Storage):
    user_account = await storage_instance.get_user("kris")
    assert user_account is None

    # insert new user
    await storage_instance.save_user(
        UserAccount(username="kris", email="kris@oleeka.com")
    )
    user_account = await storage_instance.get_user("kris")
    assert user_account == UserAccount(username="kris", email="kris@oleeka.com")

    # store existing user
    await storage_instance.save_user(
        UserAccount(username="kris", email="kris@oleeka.com", settings={"tz": "01:00"})
    )
    user_account = await storage_instance.get_user("kris")
    assert user_account == UserAccount(
        username="kris", email="kris@oleeka.com", settings={"tz": "01:00"}
    )
