import pytest  # type: ignore

from oleeka_api.models import Query, RelatedRequest, RelatedStats, SizeStats
from oleeka_api.query.terms import Term, All, Tag, Nid
from oleeka_api.storage import Storage

from tests.data import NOTE_3


@pytest.mark.parametrize(
    ("fetch_params", "query_terms", "expected_stats"),
    [
        (
            (True, True, True, True, None),
            Term(),
            RelatedStats(
                total=0,
                tags_empty=0,
                tags={},
                attrs={},
                sizes=SizeStats(
                    text_size=0,
                    tags_counts=0,
                    attrs_counts=0,
                    files_counts=0,
                    files_size=0,
                ),
            ),
        ),
        (
            (True, True, True, False, None),
            Tag("world"),
            RelatedStats(
                total=1,
                tags_empty=0,
                tags={"hello": 1, "world": 1},
                attrs={"created": 1, "modified": 1, "name": 1},
            ),
        ),
        # fetch_total, fetch_tags, fetch_attrs combinations
        (
            (False, False, False, False, None),
            All(),
            RelatedStats(
                total=None,
                tags_empty=None,
                tags=None,
                attrs=None,
            ),
        ),
        (
            (False, True, True, False, None),
            All(),
            RelatedStats(
                total=3,
                tags_empty=1,
                tags={"hello": 2, "world": 1},
                attrs={"created": 3, "modified": 3, "name": 2, "link": 1, "rating": 1},
            ),
        ),
        (
            (True, False, False, False, None),
            All(),
            RelatedStats(
                total=3,
                tags_empty=None,
                tags=None,
                attrs=None,
            ),
        ),
        (
            (True, False, True, False, None),
            All(),
            RelatedStats(
                total=3,
                tags_empty=None,
                tags=None,
                attrs={"created": 3, "modified": 3, "name": 2, "link": 1, "rating": 1},
            ),
        ),
        (
            (True, True, False, False, None),
            All(),
            RelatedStats(
                total=3,
                tags_empty=1,
                tags={"hello": 2, "world": 1},
                attrs=None,
            ),
        ),
        (
            (True, True, True, False, None),
            All(),
            RelatedStats(
                total=3,
                tags_empty=1,
                tags={"hello": 2, "world": 1},
                attrs={"created": 3, "modified": 3, "name": 2, "link": 1, "rating": 1},
            ),
        ),
        (
            (False, False, False, True, None),
            All(),
            RelatedStats(
                total=3,
                sizes=SizeStats(
                    text_size=0,
                    tags_counts=3,
                    attrs_counts=10,
                    files_counts=1,
                    files_size=19,
                ),
            ),
        ),
        (
            (False, False, False, True, None),
            Tag("world"),
            RelatedStats(
                total=1,
                sizes=SizeStats(
                    text_size=0,
                    tags_counts=2,
                    attrs_counts=3,
                    files_counts=1,
                    files_size=19,
                ),
            ),
        ),
        (
            (False, False, False, True, None),
            Nid([NOTE_3.nid]),
            RelatedStats(
                total=1,
                sizes=SizeStats(
                    text_size=0,
                    tags_counts=0,
                    attrs_counts=3,
                    files_counts=0,
                    files_size=0,
                ),
            ),
        ),
        (
            (False, False, False, False, "year"),
            All(),
            RelatedStats(
                total=3,
                dates={"2020": 2, "2022": 1},
            ),
        ),
        (
            (False, False, False, False, "month"),
            All(),
            RelatedStats(
                total=3,
                dates={"2020-11": 2, "2022-01": 1},
            ),
        ),
        (
            (False, False, False, False, "day"),
            All(),
            RelatedStats(
                total=3,
                dates={"2020-11-13": 2, "2022-01-02": 1},
            ),
        ),
        (
            (False, False, False, False, "hour"),
            All(),
            RelatedStats(
                total=3,
                dates={"2020-11-13T17": 2, "2022-01-02T07": 1},
            ),
        ),
    ],
)
@pytest.mark.asyncio
async def test_get_related(
    storage_instance: Storage, fetch_params, query_terms, expected_stats
):
    fetch_total, fetch_tags, fetch_attrs, fetch_sizes, fetch_dates = fetch_params
    request = RelatedRequest(
        Query(query_terms=query_terms),
        fetch_total=fetch_total,
        fetch_tags=fetch_tags,
        fetch_attrs=fetch_attrs,
        fetch_sizes=fetch_sizes,
        fetch_dates=fetch_dates,
    )
    related = await storage_instance.get_related(request)
    assert related == expected_stats
