import asyncio
from typing import cast, Any, Callable, Optional

import pytest  # type: ignore

from oleeka_api.storage import Storage
from oleeka_api.storage.base import StorageBackend

from tests.data import NOTE_1, NOTE_2, NOTE_3, USER_1
from tests.helpers import set_envvar


def dict_select_keys(dict_obj, *keys: str):
    return {key: dict_obj[key] for key in keys if key in dict_obj}


@pytest.fixture(scope="module")
def event_loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(
    scope="module",
    params=[
        "memory://",
        "mongodb://{0}:{0}@127.0.0.1:27017/{0}".format("oleeka_unittest"),
        "postgresql://{0}:{0}@127.0.0.1:5432/{0}".format("oleeka_unittest"),
        "sqlite://:memory:",
    ],
    ids=cast(Callable[[Any], Optional[object]], lambda url: url.split("://")[0]),
)
async def storage_instance(request, event_loop):
    with set_envvar(StorageBackend.URL_ENVVAR, request.param):
        async for storage in Storage.get_db():
            await storage.testing_insert(USER_1, NOTE_1, NOTE_2, NOTE_3)
            yield storage
            await storage._backend.__class__.testing_deinit()
