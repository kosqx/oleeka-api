import pytest  # type: ignore

from oleeka_api.models import Note, PaginatedQuery, SelectFields
from oleeka_api.query.terms import Term, All, Nid, Tag, Text, Attr, Mark, Not, And, Or
from oleeka_api.storage import Storage

from tests.data import NOTE_1, NOTE_2, NOTE_2_NO_DATA, NOTE_3, USER_1
from tests.helpers import get_all_db_terms, get_covered_terms


def dict_select_keys(dict_obj, *keys: str):
    return {key: dict_obj[key] for key in keys if key in dict_obj}


@pytest.mark.parametrize(
    ("paginated_query", "expected_notes"),
    [
        (PaginatedQuery(query_terms=Term()), []),
        # username
        (PaginatedQuery(query_terms=All(), username="not_user"), []),
        (
            PaginatedQuery(query_terms=All(), username=USER_1.username),
            [NOTE_3, NOTE_2_NO_DATA, NOTE_1],
        ),
        # limit & skip
        (PaginatedQuery(query_terms=All(), limit=1), [NOTE_3]),
        (
            PaginatedQuery(query_terms=All(), limit=1, skip=1),
            [NOTE_2_NO_DATA],
        ),
        (PaginatedQuery(query_terms=All(), limit=1, skip=2), [NOTE_1]),
        (PaginatedQuery(query_terms=All(), limit=1, skip=3), []),
        # select_fields
        (
            PaginatedQuery(query_terms=All(), select_fields=SelectFields()),
            [NOTE_3, NOTE_2_NO_DATA, NOTE_1],
        ),
        (
            PaginatedQuery(query_terms=All(), select_fields=SelectFields.only_nid()),
            [Note(nid=NOTE_3.nid), Note(nid=NOTE_2.nid), Note(nid=NOTE_1.nid)],
        ),
        (
            PaginatedQuery(query_terms=All(), select_fields=SelectFields.no_files()),
            [NOTE_3, NOTE_2.replace(files=[]), NOTE_1],
        ),
        (
            PaginatedQuery(query_terms=All(), select_fields=SelectFields.everything()),
            [NOTE_3, NOTE_2, NOTE_1],
        ),
        (
            PaginatedQuery(
                query_terms=All(),
                select_fields=SelectFields.only(attrs={"created", "link"}),
            ),
            [
                Note(
                    nid=NOTE_3.nid,
                    attrs=dict_select_keys(NOTE_3.attrs, "created", "link"),
                ),
                Note(
                    nid=NOTE_2.nid,
                    attrs=dict_select_keys(NOTE_2.attrs, "created", "link"),
                ),
                Note(
                    nid=NOTE_1.nid,
                    attrs=dict_select_keys(NOTE_1.attrs, "created", "link"),
                ),
            ],
        ),
    ],
)
@pytest.mark.asyncio
async def test_get_notes(
    storage_instance: Storage,
    paginated_query: PaginatedQuery,
    expected_notes,
):
    assert await storage_instance.get_notes(paginated_query) == expected_notes


def idfn(val):
    if isinstance(val, Term):
        try:
            return f"-{val.format()}-"
        except NotImplementedError:
            return f"{val.to_struct()}"
    if isinstance(val, list):
        return f"list{len(val)}"


QUERY_TERMS_TESTCASES = [
    (Term(), []),
    (All(), [NOTE_3, NOTE_2, NOTE_1]),
    # Nid
    (Nid([]), []),
    (Nid([NOTE_1.nid]), [NOTE_1]),
    (Nid([NOTE_1.nid, NOTE_2.nid]), [NOTE_2, NOTE_1]),
    # Text
    (Text("note"), [NOTE_3, NOTE_2, NOTE_1]),
    (Text("second"), [NOTE_2]),
    # Tag
    (Tag("hello"), [NOTE_2, NOTE_1]),
    (Tag("he", mode="prefix"), [NOTE_2, NOTE_1]),
    (Tag("ello", mode="suffix"), [NOTE_2, NOTE_1]),
    (Tag("ell", mode="infix"), [NOTE_2, NOTE_1]),
    (Tag("ell", mode="exact"), []),
    # Attr
    (Attr("link", "?", ""), [NOTE_3]),
    (Attr("name", "=", "abc foo123 xyz"), [NOTE_2, NOTE_1]),
    (Attr("name", "=", "OO12"), [NOTE_2, NOTE_1]),
    (Attr("name", "==", "abc foo123 xyz"), [NOTE_2, NOTE_1]),
    (Attr("name", "==", "ABC FOO123 XYZ"), [NOTE_2, NOTE_1]),
    (Attr("name", "==", "foo123"), []),
    (Attr("name", "===", "abc foo123 xyz"), [NOTE_1]),
    (Attr("name", "===", "ABC FOO123 XYZ"), [NOTE_2]),
    (Attr("name", "===", "FOO123"), []),
    (Attr("name", "^=", "abc"), [NOTE_2, NOTE_1]),
    (Attr("name", "^=", "foo"), []),
    (Attr("name", "^==", "abc"), [NOTE_1]),
    (Attr("name", "^==", "ABC"), [NOTE_2]),
    (Attr("name", "$=", "xyz"), [NOTE_2, NOTE_1]),
    (Attr("name", "$=", "foo"), []),
    (Attr("name", "$==", "xyz"), [NOTE_1]),
    (Attr("name", "$==", "XYZ"), [NOTE_2]),
    (Attr("name", "$==", "foo"), []),
    (Attr("name", "*=", "foo123"), [NOTE_2, NOTE_1]),
    (Attr("name", "*=", "FOO123"), [NOTE_2, NOTE_1]),
    (Attr("name", "*=", "oo12"), [NOTE_2, NOTE_1]),
    (Attr("name", "*==", "foo123"), [NOTE_1]),
    (Attr("name", "*==", "FOO123"), [NOTE_2]),
    (Attr("name", "*==", "OO12"), [NOTE_2]),
    (Attr("name", "+=", "foo123"), [NOTE_2, NOTE_1]),
    (Attr("name", "+=", "oo12"), []),
    (Attr("name", "+==", "foo123"), [NOTE_1]),
    (Attr("name", "+==", "FOO123"), [NOTE_2]),
    (Attr("name", "+==", "OO12"), []),
    (Attr("created", "<=", NOTE_2.attrs["created"]), [NOTE_2, NOTE_1]),
    (Attr("created", "<==", NOTE_2.attrs["created"]), [NOTE_2, NOTE_1]),
    (Attr("created", ">=", NOTE_2.attrs["created"]), [NOTE_3, NOTE_2]),
    (Attr("created", ">==", NOTE_2.attrs["created"]), [NOTE_3, NOTE_2]),
    (Attr("name", "<=", "abd"), [NOTE_2, NOTE_1]),
    (Attr("name", "<==", "ABD"), [NOTE_2]),
    (Attr("name", ">=", "abb"), [NOTE_2, NOTE_1]),
    (Attr("name", ">==", "ABB"), [NOTE_2, NOTE_1]),
    (Attr("link", "<<=", "0"), []),
    (Attr("link", ">>=", "0"), []),
    (Attr("rating", "<<=", "abc"), []),
    (Attr("rating", ">>=", "abc"), []),
    (Attr("rating", "<<=", "3"), [NOTE_1]),
    (Attr("rating", "<<=", "2.5"), [NOTE_1]),
    (Attr("rating", "<<=", "2"), [NOTE_1]),
    (Attr("rating", "<<=", "1.5"), []),
    (Attr("rating", "<<=", "1"), []),
    (Attr("rating", ">>=", "3"), []),
    (Attr("rating", ">>=", "2.5"), []),
    (Attr("rating", ">>=", "2"), [NOTE_1]),
    (Attr("rating", ">>=", "1.5"), [NOTE_1]),
    (Attr("rating", ">>=", "1"), [NOTE_1]),
    # Mark tags_count
    (Mark("tags_count", "==", 0), [NOTE_3]),
    (Mark("tags_count", "==", 1), [NOTE_1]),
    (Mark("tags_count", "==", 2), [NOTE_2]),
    (Mark("tags_count", "==", 3), []),
    (Mark("tags_count", "!=", 1), [NOTE_3, NOTE_2]),
    (Mark("tags_count", "<", 1), [NOTE_3]),
    (Mark("tags_count", "<=", 1), [NOTE_3, NOTE_1]),
    (Mark("tags_count", ">", 1), [NOTE_2]),
    (Mark("tags_count", ">=", 1), [NOTE_2, NOTE_1]),
    # Mark attrs_count
    (Mark("attrs_count", "==", 3), [NOTE_3, NOTE_2]),
    (Mark("attrs_count", "<", 3), []),
    (Mark("attrs_count", ">", 3), [NOTE_1]),
    # Mark files_count
    (Mark("files_count", "==", 1), [NOTE_2]),
    (Mark("files_count", "<", 1), [NOTE_3, NOTE_1]),
    (Mark("files_count", ">", 1), []),
    # Mark files_size
    (Mark("files_size", "==", 0), [NOTE_3, NOTE_1]),
    (Mark("files_size", "==", 19), [NOTE_2]),
    (Mark("files_size", "<", 19), [NOTE_3, NOTE_1]),
    (Mark("files_size", ">", 19), []),
    # Mark modified_after
    (Mark("modified_after", "==", 0), [NOTE_2, NOTE_1]),
    (Mark("modified_after", "==", 7200), [NOTE_3]),
    # Mark date_day_of_week
    (Mark("date_day_of_week", "==", 0), []),
    (Mark("date_day_of_week", "==", 1), []),
    (Mark("date_day_of_week", "==", 2), []),
    (Mark("date_day_of_week", "==", 3), []),
    (Mark("date_day_of_week", "==", 4), []),
    (Mark("date_day_of_week", "==", 5), [NOTE_2, NOTE_1]),
    (Mark("date_day_of_week", "==", 6), []),
    (Mark("date_day_of_week", "==", 7), [NOTE_3]),
    (Mark("date_day_of_week", "==", 8), []),
    # Mark date_day_of_year
    (Mark("date_day_of_year", "==", 2), [NOTE_3]),
    (Mark("date_day_of_year", "==", 318), [NOTE_2, NOTE_1]),
    # Mark date_week_of_year
    (Mark("date_week_of_year", "==", 52), [NOTE_3]),
    (Mark("date_week_of_year", "==", 46), [NOTE_2, NOTE_1]),
    # logical connectives
    (Not(Text("second")), [NOTE_3, NOTE_1]),
    (And([Tag("hello"), Tag("world")]), [NOTE_2]),
    (Or([Text("first"), Text("third")]), [NOTE_3, NOTE_1]),
]


@pytest.mark.parametrize(
    ("query_terms", "expected_notes"), QUERY_TERMS_TESTCASES, ids=idfn
)
@pytest.mark.asyncio
async def test_query_terms(storage_instance: Storage, query_terms, expected_notes):
    query = PaginatedQuery(
        query_terms=query_terms,
        select_fields=SelectFields.everything(),
    )
    assert await storage_instance.get_notes(query) == expected_notes


def test_storage_backends_testcases_cover_all_terms():
    assert get_covered_terms(QUERY_TERMS_TESTCASES, 0) == get_all_db_terms()


@pytest.mark.asyncio
async def test_random_query(storage_instance: Storage):
    query = PaginatedQuery(query_terms=Tag("hello"), skip=10, limit=1, random=True)
    result_nids = set()
    for i in range(30):
        notes = await storage_instance.get_notes(query)
        assert len(notes) == 1
        result_nids.add(notes[0].nid)
        if len(result_nids) == 2:
            break
    assert {NOTE_2.nid, NOTE_1.nid} == result_nids
