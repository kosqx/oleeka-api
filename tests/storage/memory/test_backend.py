import pytest  # type: ignore

from oleeka_api.models import Note, UserAccount, PaginatedQuery
from oleeka_api.query.terms import All
from oleeka_api.storage.memory.backend import MemoryStorageBackend


NOTE_1 = Note(
    nid="20201113T175543290880R684769",
    attrs={"created": "2020-11-13T17:55:43Z"},
    text="first note",
)
NOTE_2 = Note(
    nid="20201113T175554625048R173053",
    attrs={"created": "2020-11-13T17:55:54Z"},
    text="second note",
)
NOTE_3 = Note(
    nid="20201113T182454852122R334524",
    attrs={"created": "2020-11-13T18:24:54Z"},
    text="third note",
)
USER_1 = UserAccount(username="testuser")


async def get_backend() -> MemoryStorageBackend:
    backend = MemoryStorageBackend()
    await backend.testing_insert(NOTE_1, USER_1, NOTE_2)
    return backend


@pytest.mark.asyncio
async def test_insert_for_tests():
    backend = MemoryStorageBackend()
    await backend.testing_insert(NOTE_1, USER_1, NOTE_2)

    assert backend._db == {
        "note::20201113T175543290880R684769": NOTE_1,
        "user:testuser": USER_1,
        "note:testuser:20201113T175554625048R173053": NOTE_2,
    }


@pytest.mark.asyncio
async def test_iter_by_kind():
    backend = await get_backend()
    assert list(backend._iter_by_kind("note")) == [NOTE_1, NOTE_2]
    assert list(backend._iter_by_kind("note", "testuser")) == [NOTE_2]
    assert list(backend._iter_by_kind("user")) == [USER_1]


@pytest.mark.parametrize(
    ("username", "user_account"),
    [("non_user", None), ("testuser", USER_1)],
)
@pytest.mark.asyncio
async def test_get_user(username, user_account):
    backend = await get_backend()
    assert await backend.get_user(username) == user_account


@pytest.mark.parametrize(
    ("paginated_query", "notes"),
    [
        (PaginatedQuery(), []),
        (PaginatedQuery(query_terms=All()), [NOTE_2, NOTE_1]),
        (PaginatedQuery(username="testuser", query_terms=All()), [NOTE_2]),
        (PaginatedQuery(query_terms=All(), skip=1), [NOTE_1]),
        (PaginatedQuery(query_terms=All(), skip=2), []),
        (PaginatedQuery(query_terms=All(), limit=2), [NOTE_2, NOTE_1]),
        (PaginatedQuery(query_terms=All(), limit=1), [NOTE_2]),
        (PaginatedQuery(query_terms=All(), limit=0), []),
    ],
)
@pytest.mark.asyncio
async def test_get_notes_paginated_query(paginated_query, notes):
    backend = await get_backend()
    assert await backend.get_notes(paginated_query) == notes


@pytest.mark.asyncio
async def test_save_note():
    backend = await get_backend()
    query_all = PaginatedQuery(query_terms=All())
    query_user = PaginatedQuery(username="testuser", query_terms=All())

    assert await backend.get_notes(query_all) == [NOTE_2, NOTE_1]
    assert await backend.get_notes(query_user) == [NOTE_2]

    assert await backend.save_notes("testuser", [NOTE_3]) is None

    assert await backend.get_notes(query_all) == [NOTE_3, NOTE_2, NOTE_1]
    assert await backend.get_notes(query_user) == [NOTE_3, NOTE_2]
