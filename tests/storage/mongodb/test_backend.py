from oleeka_api.storage.mongodb.backend import parse_mongodb_url


def test_parse_mongodb_url():
    url = "mongodb://user:pass@127.0.0.1:54321/dbname"
    assert parse_mongodb_url(url) == ("mongodb://user:pass@127.0.0.1:54321", "dbname")
