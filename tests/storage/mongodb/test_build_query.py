import pytest  # type: ignore

import re

from oleeka_api.models import Query, PaginatedQuery, SelectFields
from oleeka_api.query.terms import Text
from oleeka_api.storage.mongodb import build_query


@pytest.mark.parametrize(
    ("query", "expected"),
    [
        (Query(), {"$match": {"_id": ""}}),
        (Query(query_terms=Text("foo")), {"$match": {"text": re.compile("foo")}}),
        (
            Query(username="adam"),
            {"$match": {"$and": [{"_id": ""}, {"username": "adam"}]}},
        ),
        (
            Query(username="adam", query_terms=Text("foo")),
            {"$match": {"$and": [{"text": re.compile("foo")}, {"username": "adam"}]}},
        ),
    ],
)
def test_build_where(query, expected):
    assert build_query._build_match(query) == expected


@pytest.mark.parametrize(
    ("select_fields", "expected"),
    [
        (
            SelectFields(),
            [
                {"$project": {"_id": 1, "text": 1, "tags": 1, "files": 1, "attrs": 1}},
                {"$project": {"files.data": 0}},
            ],
        ),
        (
            SelectFields.only_nid(),
            [
                {"$project": {"_id": 1}},
            ],
        ),
        (
            SelectFields.no_files(),
            [
                {"$project": {"_id": 1, "text": 1, "tags": 1, "attrs": 1}},
            ],
        ),
        (
            SelectFields.everything(),
            [{"$project": {"_id": 1, "text": 1, "tags": 1, "files": 1, "attrs": 1}}],
        ),
        (
            SelectFields.only(attrs={"created", "title.dot"}),
            [
                {"$project": {"_id": 1, "attrs.created": 1, "attrs.title~2e~dot": 1}},
            ],
        ),
        (
            SelectFields(attrs={"created", "title"}),
            [
                {
                    "$project": {
                        "_id": 1,
                        "text": 1,
                        "tags": 1,
                        "files": 1,
                        "attrs.created": 1,
                        "attrs.title": 1,
                    }
                },
                {"$project": {"files.data": 0}},
            ],
        ),
    ],
)
def test_build_note_projection(select_fields, expected):
    assert build_query._build_note_projection(select_fields) == expected


@pytest.mark.parametrize(
    ("paginated_query", "expected"),
    [
        (
            PaginatedQuery(),
            [
                {"$match": {"_id": ""}},
                {"$sort": {"created": -1}},
                {"$skip": 0},
                {"$limit": 10},
                {"$project": {"_id": 1, "text": 1, "tags": 1, "files": 1, "attrs": 1}},
                {"$project": {"files.data": 0}},
            ],
        ),
        (
            PaginatedQuery(
                username="testuser",
                query_terms=Text("abc"),
                skip=40,
                limit=20,
                select_fields=SelectFields.only_nid(),
            ),
            [
                {
                    "$match": {
                        "$and": [{"text": re.compile("abc")}, {"username": "testuser"}]
                    }
                },
                {"$sort": {"created": -1}},
                {"$skip": 40},
                {"$limit": 20},
                {"$project": {"_id": 1}},
            ],
        ),
        (
            PaginatedQuery(
                username="usr",
                query_terms=Text("a"),
                skip=40,
                limit=20,
                random=True,
                select_fields=SelectFields.only_nid(),
            ),
            [
                {"$match": {"$and": [{"text": re.compile("a")}, {"username": "usr"}]}},
                {"$sample": {"size": 20}},
                {"$sort": {"created": -1}},
                {"$project": {"_id": 1}},
            ],
        ),
    ],
)
def test_get_notes(paginated_query, expected):
    assert build_query.get_notes(paginated_query) == expected
