from datetime import datetime
from typing import Any, Dict, List, Tuple

import pytest  # type: ignore

from oleeka_api.models import Note, RelatedStats, SizeStats
from oleeka_api.storage.mongodb import encoding

from tests.data import FILE_A, NOTE_2


NOTE_ENCODING_TESTCASES: List[Tuple[Note, Dict[str, Any], Dict[str, Any]]] = [
    (
        NOTE_2,
        {
            "username": "user",
            "created": datetime(2020, 11, 13, 17, 55, 54),
            "_id": "20201113T175554625048R173053",
            "text": "second note",
            "tags": ["hello", "world"],
            "attrs": {
                "created": "2020-11-13T17:55:54Z",
                "modified": "2020-11-13T17:55:54Z",
                "name": "ABC FOO123 XYZ",
            },
            "files": [
                {
                    "fid": "F1",
                    "size": 19,
                    "hash": "d78e0e124680dd28828f96c508ae755e82060436",
                    "name": "content.md",
                    "mime": "text/markdown",
                    "meta": {},
                    "data": None,
                }
            ],
            "_sizes": {
                "text_size": 11,
                "tags_counts": 2,
                "attrs_counts": 3,
                "files_counts": 1,
                "files_size": 19,
            },
        },
        {"F1": b"# Title\n\nParagraph\n"},
    ),
]
NOTE_ONLY_DECODE_TESTCASES: List[Tuple[Note, Dict[str, Any], Dict[str, Any]]] = [
    (
        Note(nid="20201113T175543290880R684769", text="", tags=[], attrs={}, files=[]),
        {
            "username": "user",
            "created": datetime(2020, 11, 13, 17, 55, 43),
            "_id": "20201113T175543290880R684769",
        },
        {},
    ),
]


@pytest.mark.parametrize(
    ("note", "record", "files_data_mapping"),
    NOTE_ENCODING_TESTCASES,
)
def test_encode_note(note, record, files_data_mapping):
    assert encoding.encode_note(note, "user") == record


@pytest.mark.parametrize(
    ("note", "record", "files_data_mapping"),
    NOTE_ENCODING_TESTCASES + NOTE_ONLY_DECODE_TESTCASES,
)
def test_decode_note(note, record, files_data_mapping):
    assert encoding.decode_note(record, files_data_mapping) == note


RECORD_NESTED = {
    "size": 19,
    "hash": "d78e0e124680dd28828f96c508ae755e82060436",
    "name": "content.md",
    "mime": "text/markdown",
    "meta": {},
    "data": None,
    "fid": "F1",
}
RECORD_STANDALONE = {
    "size": 19,
    "hash": "d78e0e124680dd28828f96c508ae755e82060436",
    "name": "content.md",
    "mime": "text/markdown",
    "meta": {},
    "data": b"# Title\n\nParagraph\n",
    "_id": "F1",
    "username": "user",
    "nid": "123",
}


def test_encode_file_nested():
    assert encoding.encode_file_nested(FILE_A) == RECORD_NESTED


def test_encode_file_standalone():
    assert encoding.encode_file_standalone(FILE_A, "user", "123") == RECORD_STANDALONE


def test_decode_file_nested():
    files_data_mapping = {"F1": b"# Title\n\nParagraph\n"}
    assert encoding.decode_file_nested(RECORD_NESTED, files_data_mapping) == FILE_A


def test_decode_file_standalone():
    assert encoding.decode_file_standalone(RECORD_STANDALONE) == FILE_A


@pytest.mark.parametrize(
    ("record", "expected"),
    [
        ({}, RelatedStats()),
        (
            {
                "total": [],
                "tags": [],
                "attrs": [],
                "sizes": [],
                "dates": [],
            },
            RelatedStats(
                total=0,
                tags_empty=0,
                tags={},
                attrs={},
                dates={},
                sizes=SizeStats(
                    text_size=0,
                    tags_counts=0,
                    attrs_counts=0,
                    files_counts=0,
                    files_size=0,
                ),
            ),
        ),
        (
            {
                "total": [{"count": 3}],
                "tags": [
                    {"_id": "hello", "count": 2},
                    {"_id": "world", "count": 1},
                    {"_id": None, "count": 1},
                ],
                "attrs": [
                    {"_id": "created", "count": 3},
                    {"_id": "modified", "count": 3},
                    {"_id": "name", "count": 2},
                    {"_id": "link", "count": 1},
                    {"_id": "rating", "count": 1},
                ],
                "sizes": [
                    {
                        "_id": None,
                        "tags_counts": 3,
                        "attrs_counts": 10,
                        "files_counts": 1,
                        "files_size": 19,
                    }
                ],
                "dates": [
                    {"_id": "2020-11-13", "count": 3},
                ],
            },
            RelatedStats(
                total=3,
                tags_empty=1,
                tags={"hello": 2, "world": 1},
                attrs={"created": 3, "modified": 3, "name": 2, "link": 1, "rating": 1},
                dates={"2020-11-13": 3},
                sizes=SizeStats(
                    text_size=0,
                    tags_counts=3,
                    attrs_counts=10,
                    files_counts=1,
                    files_size=19,
                ),
            ),
        ),
    ],
)
def test_decode_related_stats(record, expected):
    assert encoding.decode_related_stats(record) == expected
