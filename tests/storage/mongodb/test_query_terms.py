import pytest  # type: ignore

import re

from oleeka_api.query.terms import Term, All, Nid, Tag, Text, Attr, Mark
from oleeka_api.query.terms import Not, And, Or, Nested
from oleeka_api.storage.mongodb.query_terms import to_condition

from tests.helpers import get_all_db_terms, get_covered_terms


NID_1 = "20201113T175543290880R684769"
NID_2 = "20201113T175554625048R173053"
TERM_TO_CONDITION_TESTCASES = [
    (Term(), {"_id": ""}),
    (All(), {}),
    (Nid([]), {"_id": {"$in": []}}),
    (Nid([NID_1]), {"_id": {"$in": [NID_1]}}),
    (Nid([NID_1, NID_2]), {"_id": {"$in": [NID_1, NID_2]}}),
    (Text("foo"), {"text": re.compile("foo")}),
    # Tag
    (Tag("foo"), {"tags": "foo"}),
    (Tag("foo", mode="exact"), {"tags": "foo"}),
    (Tag("foo", mode="prefix"), {"tags": re.compile("^foo.*$")}),
    (Tag("foo", mode="suffix"), {"tags": re.compile("^.*foo$")}),
    (Tag("foo", mode="infix"), {"tags": re.compile("^.*foo.*$")}),
    # Attr - text quoting
    (Attr("f.o", "^==", "foo...?"), {"attrs.f~2e~o": re.compile("^foo\\.\\.\\.\\?")}),
    # Attr - all operators
    (Attr("foo", "?", ""), {"attrs.foo": {"$exists": True}}),
    (Attr("foo", "=", "bar"), {"attrs.foo": re.compile("bar", re.IGNORECASE)}),
    (Attr("foo", "==", "bar"), {"attrs.foo": re.compile("^bar$", re.IGNORECASE)}),
    (Attr("foo", "===", "bar"), {"attrs.foo": "bar"}),
    (Attr("foo", "^=", "bar"), {"attrs.foo": re.compile("^bar", re.IGNORECASE)}),
    (Attr("foo", "^==", "bar"), {"attrs.foo": re.compile("^bar")}),
    (Attr("foo", "$=", "bar"), {"attrs.foo": re.compile("bar$", re.IGNORECASE)}),
    (Attr("foo", "$==", "bar"), {"attrs.foo": re.compile("bar$")}),
    (Attr("foo", "*=", "bar"), {"attrs.foo": re.compile("bar", re.IGNORECASE)}),
    (Attr("foo", "*==", "bar"), {"attrs.foo": re.compile("bar")}),
    (Attr("foo", "+=", "bar"), {"attrs.foo": re.compile("\\bbar\\b", re.IGNORECASE)}),
    (Attr("foo", "+==", "bar"), {"attrs.foo": re.compile("\\bbar\\b")}),
    (
        Attr("foo", "<=", "bar"),
        {
            "$expr": {
                "$and": [
                    {"$eq": [{"$type": "$attrs.foo"}, "string"]},
                    {"$lte": [{"$toUpper": "$attrs.foo"}, "BAR"]},
                ]
            }
        },
    ),
    (Attr("foo", "<==", "bar"), {"attrs.foo": {"$lte": "bar"}}),
    (
        Attr("foo", ">=", "bar"),
        {
            "$expr": {
                "$and": [
                    {"$eq": [{"$type": "$attrs.foo"}, "string"]},
                    {"$gte": [{"$toUpper": "$attrs.foo"}, "BAR"]},
                ]
            }
        },
    ),
    (Attr("foo", ">==", "bar"), {"attrs.foo": {"$gte": "bar"}}),
    (Attr("foo", "<<=", "bar"), {"_id": ""}),
    (Attr("foo", "<<=", "0.1"), {"attrs.foo|num": {"$lte": 0.1}}),
    (Attr("foo", "<<=", "023"), {"attrs.foo|num": {"$lte": 23.0}}),
    (Attr("foo", ">>=", "bar"), {"_id": ""}),
    (Attr("foo", ">>=", "42."), {"attrs.foo|num": {"$gte": 42.0}}),
    # Mark
    (Mark("tags_count", "==", 1), {"$expr": {"$eq": [{"$size": "$tags"}, 1]}}),
    (Mark("tags_count", "!=", 1), {"$expr": {"$ne": [{"$size": "$tags"}, 1]}}),
    (Mark("tags_count", "<", 1), {"$expr": {"$lt": [{"$size": "$tags"}, 1]}}),
    (Mark("tags_count", "<=", 1), {"$expr": {"$lte": [{"$size": "$tags"}, 1]}}),
    (Mark("tags_count", ">", 1), {"$expr": {"$gt": [{"$size": "$tags"}, 1]}}),
    (Mark("tags_count", ">=", 1), {"$expr": {"$gte": [{"$size": "$tags"}, 1]}}),
    (
        Mark("attrs_count", "==", 2),
        {"$expr": {"$eq": [{"$size": {"$objectToArray": "$attrs"}}, 2]}},
    ),
    (Mark("files_count", "==", 1), {"$expr": {"$eq": [{"$size": "$files"}, 1]}}),
    (Mark("files_size", "==", 1), {"$expr": {"$eq": [{"$sum": "$files.size"}, 1]}}),
    (
        Mark("modified_after", "<", 60),
        {
            "$expr": {
                "$lt": [
                    {
                        "$divide": [
                            {
                                "$subtract": [
                                    {"$toDate": "$attrs.modified"},
                                    {"$toDate": "$attrs.created"},
                                ]
                            },
                            1000,
                        ]
                    },
                    60,
                ]
            }
        },
    ),
    (
        Mark("date_day_of_week", "==", 1),
        {
            "$expr": {
                "$eq": [{"$isoDayOfWeek": {"date": {"$toDate": "$attrs.created"}}}, 1]
            }
        },
    ),
    (
        Mark("date_day_of_year", "==", 1),
        {
            "$expr": {
                "$eq": [{"$dayOfYear": {"date": {"$toDate": "$attrs.created"}}}, 1]
            }
        },
    ),
    (
        Mark("date_week_of_year", "==", 1),
        {"$expr": {"$eq": [{"$isoWeek": {"date": {"$toDate": "$attrs.created"}}}, 1]}},
    ),
    # logical connectives
    (Not(All()), {"$nor": [{}]}),
    (And([All(), Term()]), {"$and": [{}, {"_id": ""}]}),
    (Or([All(), Term()]), {"$or": [{}, {"_id": ""}]}),
]


@pytest.mark.parametrize(("term", "condition"), TERM_TO_CONDITION_TESTCASES)
def test_term_to_condition(term: Term, condition):
    assert to_condition(term) == condition


def test_mongodb_testcases_cover_all_terms():
    assert get_covered_terms(TERM_TO_CONDITION_TESTCASES, 0) == get_all_db_terms()


def test_term_to_condition_fails_for_nested():
    with pytest.raises(KeyError):
        to_condition(Nested("foo"))
