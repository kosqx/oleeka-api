import pytest  # type: ignore

from oleeka_api.modifier.actions import AddText, AddTag, RemoveTag, ResetTags, SetAttr
from oleeka_api.modifier.base import InputParseException
from oleeka_api.modifier.parser import parse_input_string


@pytest.mark.parametrize(
    ("input_string", "expected"),
    [
        ("", []),
        ("'single quoted'", [AddText("single quoted")]),
        ('"double quoted"', [AddText("double quoted")]),
        (" foo ", [AddTag("foo")]),
        ("foo bar", [AddTag("foo"), AddTag("bar")]),
        ("foo!", [RemoveTag("foo")]),
        ("foo !", [RemoveTag("foo")]),
        ("foo ! bar", [RemoveTag("foo"), AddTag("bar")]),
        ("foo bar!", [AddTag("foo"), RemoveTag("bar")]),
        ("val = 7", [SetAttr("val", "7")]),
        ("val = 'single quoted'", [SetAttr("val", "single quoted")]),
        ('val = "double quoted"', [SetAttr("val", "double quoted")]),
        ("$val = 7", [SetAttr("val", "7")]),
        ("$val = 'a->b'", [SetAttr("val", "a->b")]),
        (
            "val = abc foo 'text value'",
            [SetAttr("val", "abc"), AddTag("foo"), AddText("text value")],
        ),
        ("=", [ResetTags()]),
        ("==", [ResetTags()]),
        ("===", [ResetTags()]),
        ("=foo", [ResetTags(), AddTag("foo")]),
        ("==foo", [ResetTags(), AddTag("foo")]),
        ("===foo", [ResetTags(), AddTag("foo")]),
        (" == foo", [ResetTags(), AddTag("foo")]),
        ("[]", []),
        ('[["add_tag", "foo"]]', [AddTag("foo")]),
        (
            (
                "["
                '["add_text", "abc"], '
                '["add_tag", "foo"], '
                '["remove_tag", "bar"], '
                '["set_attr", "k", "v"]'
                "]"
            ),
            [AddText("abc"), AddTag("foo"), RemoveTag("bar"), SetAttr("k", "v")],
        ),
    ],
)
def test_parse_input_string(input_string, expected):
    assert parse_input_string(input_string) == expected


@pytest.mark.parametrize(
    ("input_string", "message", "info"),
    [
        ("!", "input cannot be parsed", 0),
        ("  !  ", "input cannot be parsed", 2),
        ("$ key = 7", "input cannot be parsed", 0),
        ("key = ", "input cannot be parsed", 4),
        (" key = ", "input cannot be parsed", 5),
        ("key='value", "input cannot be parsed", 3),
        ("'abc", "input cannot be parsed", 0),
        ('key="value', "input cannot be parsed", 3),
        ('"abc', "input cannot be parsed", 0),
        ('"abc"!', "input cannot be parsed", 5),
        ("a=b!", "input cannot be parsed", 3),
        ("foo==", "input cannot be parsed", 3),
        ("foo==bar", "input cannot be parsed", 3),
        ("[", "input is not valid JSON", None),
        ('[("tag", "foo")]', "input is not valid JSON", None),
        ("[144]", "invalid input struct", 144),
        ("[[]]", "invalid input struct", []),
        ('[["foo"]]', "invalid input struct", ["foo"]),
        ('[["add_tag"]]', "invalid input struct", ["add_tag"]),
        ('[["add_tag", 123]]', "invalid input struct", ["add_tag", 123]),
        ('[["set_attr", "key"]]', "invalid input struct", ["set_attr", "key"]),
        (
            '[["set_attr", "key", 144]]',
            "invalid input struct",
            ["set_attr", "key", 144],
        ),
    ],
)
def test_parse_input_string_raises(input_string, message, info):
    with pytest.raises(InputParseException) as exc:
        parse_input_string(input_string)
    assert exc.value.args == (message, info)
    assert exc.value.message == message
    assert exc.value.info == info
