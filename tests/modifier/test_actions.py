import pytest  # type: ignore

from oleeka_api.modifier import actions


ACTION_TESTCASES = [
    (actions.Action(), ["noop"]),
    (actions.AddText("foo"), ["add_text", "foo"]),
    (actions.AddTag("foo"), ["add_tag", "foo"]),
    (actions.RemoveTag("foo"), ["remove_tag", "foo"]),
    (actions.ResetTags(), ["reset_tags"]),
    (actions.SetAttr("foo", "value"), ["set_attr", "foo", "value"]),
]


@pytest.mark.parametrize(("action", "struct"), ACTION_TESTCASES)
def test_action_to_struct(action, struct):
    assert action.to_struct() == struct


@pytest.mark.parametrize(("action", "struct"), ACTION_TESTCASES)
def test_action_from_struct(action, struct):
    assert actions.Action.from_struct(struct) == action
