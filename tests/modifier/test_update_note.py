import pytest  # type: ignore

from oleeka_api.models import Note
from oleeka_api.modifier.actions import AddText, AddTag, RemoveTag, ResetTags, SetAttr
from oleeka_api.modifier.update_note import (
    apply_actions,
    update_note_with_input,
)


@pytest.mark.parametrize(
    ("note", "actions", "expected"),
    [
        (Note(), [], Note()),
        # AddText
        (Note(), [AddText("foo")], Note(text="foo")),
        (Note(), [AddText("foo"), AddText("bar")], Note(text="foo\nbar")),
        (Note(text="abc"), [AddText("foo")], Note(text="abc\nfoo")),
        (Note(text="abc\n"), [AddText("foo")], Note(text="abc\nfoo")),
        (Note(text="abc\r"), [AddText("foo")], Note(text="abc\rfoo")),
        (Note(text="abc\r\n"), [AddText("foo")], Note(text="abc\r\nfoo")),
        # AddTag
        (Note(), [AddTag("foo")], Note(tags=["foo"])),
        (Note(tags=["abc"]), [AddTag("foo")], Note(tags=["abc", "foo"])),
        (Note(), [AddTag("foo"), AddTag("bar")], Note(tags=["foo", "bar"])),
        (Note(), [AddTag("foo"), AddTag("foo")], Note(tags=["foo"])),
        # RemoveTag
        (Note(), [RemoveTag("foo")], Note(tags=[])),
        (Note(tags=["abc"]), [RemoveTag("foo")], Note(tags=["abc"])),
        (Note(tags=["abc"]), [RemoveTag("abc")], Note(tags=[])),
        (Note(tags=["abc", "foo"]), [RemoveTag("abc")], Note(tags=["foo"])),
        (Note(tags=["abc", "foo", "abc"]), [RemoveTag("abc")], Note(tags=["foo"])),
        # ResetTags
        (Note(), [ResetTags()], Note(tags=[])),
        (Note(tags=["abc", "foo"]), [ResetTags()], Note(tags=[])),
        # SetAttr
        (Note(), [SetAttr("val", "7")], Note(attrs={"val": "7"})),
        (Note(attrs={"val": "X"}), [SetAttr("val", "7")], Note(attrs={"val": "7"})),
        (Note(attrs={"val": "X"}), [SetAttr("val", "")], Note(attrs={})),
        # all
        (
            Note(text="abc\n", tags=["tagA"], attrs={"kind": "note"}),
            [AddText("foo"), AddTag("foo"), SetAttr("pi", "3.14")],
            Note(
                text="abc\nfoo",
                tags=["tagA", "foo"],
                attrs={"kind": "note", "pi": "3.14"},
            ),
        ),
    ],
)
def test_apply_actions(note, actions, expected):
    assert apply_actions(note, actions) == expected


def test_update_note_with_input():
    assert update_note_with_input(
        note=Note(), input_string="val = abc foo 'text value'"
    ) == Note(
        text="text value",
        tags=["foo"],
        attrs={"val": "abc"},
    )
