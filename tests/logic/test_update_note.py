from typing import cast

import pytest  # type: ignore
from freezegun import freeze_time

from oleeka_api.api.models import NoteInputModel
from oleeka_api.logic.base import OleekaConflictException
from oleeka_api.logic.update_note import merge_note
from oleeka_api.models import File, Note

from oleeka_api.logic.note import generate_nid

from tests.helpers import Matches


NID_1 = generate_nid()
RESTORE_NOTE_INPUT = {
    "nid": "123",
    "text": ["abc\n", "xyz"],
    "tags": ["foo", "bar"],
    "attrs": {
        "created": "2020-11-08T20:11:06Z",
        "modified": "2020-11-08T20:12:16Z",
        "kind": "bookmark",
    },
    "files": [
        {
            "fid": "F16",
            "name": "foo.txt",
            "mime": "text/plain",
            "data": "foo bar",
            "encoding": "text",
        },
    ],
    "restore": True,
}
RESTORE_NOTE_EXPECTED = Note(
    nid=cast(str, Matches.nid()),
    text="abc\nxyz",
    tags=["foo", "bar"],
    attrs={
        "created": "2020-11-08T20:11:06Z",
        "modified": "2020-11-08T20:12:16Z",
        "kind": "bookmark",
    },
    files=[
        File(
            fid="F16",
            name="foo.txt",
            hash="3773dea65156909838fa6c22825cafe090ff8030",
            mime="text/plain",
            size=7,
            meta={},
            data=b"foo bar",
        ),
    ],
)


@pytest.mark.parametrize(
    ("note", "note_input", "expected"),
    [
        pytest.param(
            Note(),
            {},
            Note(
                nid=cast(str, Matches.nid()),
                attrs={
                    "created": "2021-12-21T12:34:56Z",
                    "modified": "2021-12-21T12:34:56Z",
                },
            ),
            id="phantom_update_with_empty_note",
        ),
        pytest.param(
            Note(
                nid=NID_1,
                attrs={
                    "created": "2021-01-25T19:17:50Z",
                    "modified": "2021-01-25T19:17:50Z",
                },
            ),
            {},
            Note(
                nid=NID_1,
                attrs={
                    "created": "2021-01-25T19:17:50Z",
                    "modified": "2021-12-21T12:34:56Z",
                },
            ),
            id="phantom_update",
        ),
        pytest.param(
            None,
            {},
            Note(
                nid=cast(str, Matches.nid()),
                attrs={
                    "created": "2021-12-21T12:34:56Z",
                    "modified": "2021-12-21T12:34:56Z",
                },
            ),
            id="create_new_note",
        ),
        pytest.param(
            None,
            {"input": "tagA attr=value 'note text'"},
            Note(
                nid=cast(str, Matches.nid()),
                text="note text",
                tags=["tagA"],
                attrs={
                    "created": "2021-12-21T12:34:56Z",
                    "modified": "2021-12-21T12:34:56Z",
                    "attr": "value",
                },
            ),
            id="input",
        ),
        pytest.param(
            Note(
                nid=NID_1,
                attrs={
                    "created": "2021-01-25T19:17:40Z",
                    "modified": "2021-01-25T19:17:50Z",
                },
            ),
            {"attrs": {"modified": "2021-01-25T19:17:50Z"}},
            Note(
                nid=NID_1,
                attrs={
                    "created": "2021-01-25T19:17:40Z",
                    "modified": "2021-12-21T12:34:56Z",
                },
            ),
            id="compare_modified_success",
        ),
        pytest.param(
            Note(
                nid=NID_1,
                attrs={
                    "created": "2021-01-25T19:17:50Z",
                    "modified": "2021-01-25T19:17:50Z",
                },
            ),
            {"attrs": {"modified": "2021-01-25T19:17:20Z"}},
            OleekaConflictException(
                message="note has been updated in meantime",
                info={
                    "request": "2021-01-25T19:17:20Z",
                    "storage": "2021-01-25T19:17:50Z",
                },
            ),
            id="compare_modified_conflict",
        ),
        pytest.param(
            None,
            {
                "nid": "123",
                "text": ["abc", " ", "xyz"],
                "tags": ["foo", "bar"],
                "attrs": {"kind": "bookmark", "created": "2020-11-08T20:11:06Z"},
                "files": [
                    {"name": "note", "data": "<a>foo</a>", "mime": "application/xml"},
                    {
                        "name": "red_dot.png",
                        "data": (
                            "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbybl"
                            "AAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO"
                            "9TXL0Y4OHwAAAABJRU5ErkJggg=="
                        ),
                        "encoding": "base64",
                        "meta": {
                            "image_format": "PNG",
                            "image_height": 5,
                            "image_width": 5,
                        },
                    },
                ],
            },
            Note(
                nid=cast(str, Matches.nid()),
                text="abc xyz",
                tags=["foo", "bar"],
                attrs={
                    "created": "2020-11-08T20:11:06Z",
                    "modified": "2021-12-21T12:34:56Z",
                    "kind": "bookmark",
                },
                files=[
                    File(
                        fid=cast(str, Matches.fid()),
                        data=b"<a>foo</a>",
                        hash="9dbed8af4a914d701b02c1dafb7440eac868434d",
                        meta={},
                        mime="application/xml",
                        name="note",
                        size=10,
                    ),
                    File(
                        fid=cast(str, Matches.fid()),
                        data=(
                            b"\x89\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00"
                            b"\x0d\x49\x48\x44\x52\x00\x00\x00\x05\x00\x00"
                            b"\x00\x05\x08\x06\x00\x00\x00\x8d\x6f\x26\xe5"
                            b"\x00\x00\x00\x1c\x49\x44\x41\x54\x08\xd7\x63"
                            b"\xf8\xff\xff\x3f\xc3\x7f\x06\x20\x05\xc3\x20"
                            b"\x12\x84\xd0\x31\xf1\x82\x58\xcd\x04\x00\x0e"
                            b"\xf5\x35\xcb\xd1\x8e\x0e\x1f\x00\x00\x00\x00"
                            b"\x49\x45\x4e\x44\xae\x42\x60\x82"
                        ),
                        hash="af6a4603e039cca2f6823d287f6c87e561aa6e68",
                        meta={
                            "image_format": "PNG",
                            "image_height": "5",
                            "image_width": "5",
                        },
                        mime="image/png",
                        name="red_dot.png",
                        size=85,
                    ),
                ],
            ),
            id="create_with_files",
        ),
        pytest.param(
            Note(
                files=[
                    File.from_data(name="a_noop.txt", data="a", fid="F001"),
                    File.from_data(name="b_data.txt", data="b", fid="F002"),
                    File.from_data(name="c_name.txt", data="c", fid="F003"),
                    File.from_data(name="d_mime.txt", data="d", fid="F004"),
                    File.from_data(name="e_meta.txt", data="e", fid="F005"),
                    File.from_data(name="f_junk.txt", data="f", fid="F006"),
                ]
            ),
            {
                "files": [
                    {"fid": "F002", "data": "B"},
                    {"fid": "F003", "name": "c_call.txt"},
                    {"fid": "F004", "mime": "text/html"},
                    {"fid": "F005", "meta": {"foo": "bar"}},
                    {"fid": "F006", "delete": True},
                    {"name": "g_anew.txt", "data": "g"},
                ],
            },
            Note(
                nid=cast(str, Matches.nid()),
                attrs={
                    "created": "2021-12-21T12:34:56Z",
                    "modified": "2021-12-21T12:34:56Z",
                },
                files=[
                    File.from_data(name="a_noop.txt", data="a", fid="F001"),
                    File.from_data(name="b_data.txt", data="B", fid="F002"),
                    File.from_data(name="c_call.txt", data="c", fid="F003"),
                    File.from_data(
                        name="d_mime.txt", data="d", fid="F004", mime="text/html"
                    ),
                    File.from_data(
                        name="e_meta.txt", data="e", fid="F005", meta={"foo": "bar"}
                    ),
                    File.from_data(
                        name="g_anew.txt", data="g", fid=cast(str, Matches.fid())
                    ),
                ],
            ),
            id="files_operations",
        ),
        pytest.param(
            None,
            RESTORE_NOTE_INPUT,
            RESTORE_NOTE_EXPECTED,
            id="restore",
        ),
        pytest.param(
            Note(text="xyz"),
            RESTORE_NOTE_INPUT,
            RESTORE_NOTE_EXPECTED,
            id="restore_note_exists",
        ),
    ],
)
@freeze_time("2021-12-21T12:34:56")
def test_merge_note(note, note_input, expected):
    note_input_obj = NoteInputModel.parse_obj(note_input)

    if isinstance(expected, Exception):
        with pytest.raises(OleekaConflictException) as exc:
            merge_note(note, note_input_obj)
        assert exc.value.args == expected.args
    else:
        assert merge_note(note, note_input_obj) == expected
