import pytest  # type: ignore

from oleeka_api.logic.plugin import Plugin, PluginManager
from oleeka_api.models import Note
from oleeka_api.query.terms import Tag, Attr, Nid, Not, And, Or


from tests.data import USER_1


@pytest.mark.parametrize(
    ("query", "expected"),
    [
        (Tag("foo"), Tag("foo")),
        (Tag(":bar"), Attr("kind", "===", "bar")),
        (Not(Tag(":bar")), Not(Attr("kind", "===", "bar"))),
        (Not(Not(Tag(":bar"))), Not(Not(Attr("kind", "===", "bar")))),
        (And([Tag("foo"), Tag(":bar")]), And([Tag("foo"), Attr("kind", "===", "bar")])),
        (Or([Tag("foo"), Tag(":bar")]), Or([Tag("foo"), Attr("kind", "===", "bar")])),
        (Tag("pet"), Or([Tag("cat"), Tag("dog")])),
        (
            Or([Tag("pet"), Tag("fish")]),
            Or([Or([Tag("cat"), Tag("dog")]), Tag("fish")]),
        ),
        (
            Attr(":bar", "===", "abc,def"),
            Or([Attr("bar", "===", "abc"), Attr("bar", "===", "def")]),
        ),
        (
            Not(Attr(":bar", "===", "abc,def")),
            Not(Or([Attr("bar", "===", "abc"), Attr("bar", "===", "def")])),
        ),
    ],
)
def test_replace_query_term(query, expected):
    bar_plugin = Plugin("bar")
    bar_plugin.replace_query_term(Tag(":bar"), Attr("kind", "===", "bar"))

    @bar_plugin.modify_query_term(Attr)
    def in_bar(attr):
        if isinstance(attr, Attr) and attr.name == ":bar":
            return Or([Attr("bar", "===", i) for i in attr.value.split(",")])
        return attr

    pet_plugin = Plugin("pet")
    pet_plugin.replace_query_term(Tag("pet"), Or([Tag("cat"), Tag("dog")]))

    plugin_manager = PluginManager([bar_plugin, pet_plugin])

    assert plugin_manager.preprocess_query(query) == expected


SEQUENCE_TESTCASES = [
    (Tag("foo"), Tag("foo")),
    (Tag("p2"), Tag("p2t")),
    (Attr("p1a", "===", "foo"), Attr("p1a", "===", "FOO")),
    (Tag(":p1"), Or([Attr("p1a", "===", "FOO"), Tag("p2t", "exact")])),
    # wrapped in operator
    (Not(Tag("p2")), Not(Tag("p2t"))),
    (Not(Attr("p1a", "===", "foo")), Not(Attr("p1a", "===", "FOO"))),
    (Not(Tag(":p1")), Not(Or([Attr("p1a", "===", "FOO"), Tag("p2t", "exact")]))),
    # unhashable types
    (Nid([]), Nid([])),
]


@pytest.mark.parametrize(("query", "expected"), SEQUENCE_TESTCASES)
def test_replace_query_term_sequence(query, expected):
    p1_plugin = Plugin("p1")

    @p1_plugin.modify_query_term(Tag)
    def first(tag):
        if isinstance(tag, Tag) and tag.tag == ":p1":
            return Or([Attr("p1a", "===", "foo"), Tag("p2")])
        return tag

    @p1_plugin.modify_query_term(Attr)
    def second(attr):
        if isinstance(attr, Attr) and attr.name == "p1a":
            return Attr("p1a", "===", attr.value.upper())
        return attr

    p2_plugin = Plugin("p1")

    @p2_plugin.modify_query_term(Tag)
    def third(tag):
        if isinstance(tag, Tag) and tag.tag == "p2":
            return Tag("p2t")
        return tag

    plugin_manager = PluginManager([p1_plugin, p2_plugin])

    assert plugin_manager.preprocess_query(query) == expected


@pytest.mark.parametrize(("query", "expected"), SEQUENCE_TESTCASES)
def test_replace_query_tag_attr_sequence(query, expected):
    p1_plugin = Plugin("p1")

    @p1_plugin.modify_query_tag(":p1")
    def first(tag):
        return Or([Attr("p1a", "===", "foo"), Tag("p2")])

    @p1_plugin.modify_query_attr("p1a")
    def second(attr):
        return Attr("p1a", "===", attr.value.upper())

    p2_plugin = Plugin("p1")

    @p2_plugin.modify_query_tag("p2")
    def third(tag):
        return Tag("p2t")

    plugin_manager = PluginManager([p1_plugin, p2_plugin])

    assert plugin_manager.preprocess_query(query) == expected


@pytest.mark.parametrize(
    ("note_obj", "expected_calls"),
    [
        (Note(tags=[]), {"fish"}),
        (Note(tags=["cat"]), {"fish", "cat"}),
        (Note(tags=["dog"]), {"fish", "dog"}),
        (Note(tags=["dog", "cat"]), {"fish", "dog", "cat"}),
        (Note(tags=["where", "is", "the"]), {"fish"}),
    ],
)
def test_modify_note_filter_by_query(note_obj, expected_calls):
    calls = set()
    pet_plugin = Plugin("pet")

    @pet_plugin.modify_note(Tag("cat"))
    def cat_fn(note, user_account):
        calls.add("cat")
        return note

    @pet_plugin.modify_note(Tag("dog"))
    def dog_fn(note, user_account):
        calls.add("dog")
        return note

    @pet_plugin.modify_note()
    def fish_fn(note, user_account):
        calls.add("fish")
        return note

    plugin_manager = PluginManager([pet_plugin])
    assert plugin_manager.preprocess_note(note_obj, USER_1) == note_obj
    assert calls == expected_calls
