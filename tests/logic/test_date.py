import pytest  # type: ignore

from oleeka_api.logic.date import parse_timedelta


PARSE_TIMEDELTA_TESTCASES = [
    ("1", 1),
    ("1s", 1),
    ("1m", 1 * 60),
    ("1h", 1 * 60 * 60),
    ("1d", 1 * 60 * 60 * 24),
    ("1w", 1 * 60 * 60 * 24 * 7),
    ("", 0),
    ("s", 1),
    ("m", 1 * 60),
    ("h", 1 * 60 * 60),
    ("d", 1 * 60 * 60 * 24),
    ("w", 1 * 60 * 60 * 24 * 7),
    ("1h2m3s", 1 * 60 * 60 + 2 * 60 + 3),
    ("-1h2m3s", -(1 * 60 * 60 + 2 * 60 + 3)),
]


@pytest.mark.parametrize(("spec", "diff"), PARSE_TIMEDELTA_TESTCASES)
def test_parse_timedelta(spec, diff):
    assert parse_timedelta(spec) == diff
