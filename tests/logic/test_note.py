import re
from datetime import datetime

import pytest  # type: ignore

from oleeka_api.logic.note import validate_datetime_str, validate_nid, generate_nid


@pytest.mark.parametrize(
    ("nid", "is_valid"),
    [
        (None, False),
        (123, False),
        ("", False),
        ("20201107T213505223490R865116", True),
        (b"20201107T213505223490R865116", False),
        (" 20201107T213505223490R865116 ", False),
        ("20201107T213505223490R865116 ", False),
        ("20201107T213505223R865116", False),
        ("20201107t213505223490r865116", False),
    ],
)
def test_validate_nid(nid, is_valid):
    assert validate_nid(nid) == is_valid


DATE = datetime(2020, 1, 2, 12, 34, 56, 987_654)


@pytest.mark.parametrize(
    ("now", "rand", "pattern"),
    [
        (None, None, r"^\d{8}T\d{12}R\d{6}$"),
        (None, 144, r"^\d{8}T\d{12}R000144$"),
        (DATE, None, r"^20200102T123456987654R\d{6}$"),
        (DATE, 144, r"^20200102T123456987654R000144$"),
    ],
)
def test_generate_nid(now, rand, pattern):
    nid = generate_nid(now, rand)
    assert validate_nid(nid)
    assert re.match(pattern, nid) is not None


@pytest.mark.parametrize(
    ("dt", "is_valid"),
    [
        (None, False),
        (DATE, False),
        ("20200102T123456", False),
        ("2020-01-02T12:34:56", False),
        ("2020-01-02T12:34:56Z", True),
        ("2020-01-02T12:34:56Zfoo", False),
    ],
)
def test_validate_datetime_str(dt, is_valid):
    assert validate_datetime_str(dt) == is_valid
