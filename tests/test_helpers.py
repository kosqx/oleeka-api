from typing import cast

import pytest  # type: ignore

from tests.helpers import Matches, _MatchesCondition


@pytest.mark.parametrize(
    ("matcher", "value", "expected"),
    [
        # no conditions
        (Matches(), None, True),
        (Matches(), 3, True),
        # type only
        (Matches(int), None, False),
        (Matches(int), "3", False),
        (Matches(int), 3, True),
        # function
        (Matches(lambda x: len(x) > 2), "a", False),
        (Matches(lambda x: len(x) > 2), "ab", False),
        (Matches(lambda x: len(x) > 2), "abc", True),
        (Matches(lambda x: len(x) > 2), [1], False),
        (Matches(lambda x: len(x) > 2), [1, 2], False),
        (Matches(lambda x: len(x) > 2), [1, 2, 3], True),
        # regex pattern
        (Matches.regex("^[a-c]$"), "a", True),
        (Matches.regex("^[a-c]$"), "aa", False),
        (Matches.regex("^[a-c]$"), "d", False),
        # multiple conditions
        (Matches(lambda x: len(x) > 2, list), "abc", False),
        (Matches(lambda x: len(x) > 2, list), [1, 2, 3], True),
        # invalid conditions
        (Matches(cast(_MatchesCondition, 5)), 5, False),
    ],
)
def test_matches(matcher, value, expected):
    assert (matcher == value) == expected
