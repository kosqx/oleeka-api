import pytest  # type: ignore

from oleeka_api.api.models import SuggestionModel
from oleeka_api.models import Note
from oleeka_api.service.suggestions import merge_suggestions


@pytest.mark.parametrize(
    ("related_tags", "query_help", "edit_help", "taginfo_notes", "expected"),
    [
        ({}, {}, {}, [], []),
        (
            {"foo": 1},
            {":summer": "Created in summer (meteorological)"},
            {":age": "Set time offset"},
            [],
            [
                SuggestionModel(
                    name=":age=",
                    only="edit",
                    help="Set time offset",
                ),
                SuggestionModel(
                    name=":summer",
                    only="query",
                    help="Created in summer (meteorological)",
                ),
                SuggestionModel(name="foo", count=1),
            ],
        ),
        (
            {"foo": 1, "bar": 2},
            {},
            {},
            [
                Note(attrs={"tag": "bar", "title": "bar tag info"}),
                Note(attrs={"tag": "baz", "title": "baz tag info"}),
            ],
            [
                SuggestionModel(name="bar", count=2, help="bar tag info"),
                SuggestionModel(name="baz", help="baz tag info"),
                SuggestionModel(name="foo", count=1),
            ],
        ),
    ],
)
def test_merge_suggestions(
    related_tags, query_help, edit_help, taginfo_notes, expected
):
    assert (
        merge_suggestions(related_tags, query_help, edit_help, taginfo_notes)
        == expected
    )
