import pytest  # type: ignore

from oleeka_api.models import Note
from oleeka_api.query.terms import Term, All, Nid, Tag, Text, Attr, Mark, Not, And, Or
from oleeka_api.query.terms import _format_exception_message, StructParseException

from tests.data import FILE_A
from tests.helpers import (
    get_all_db_terms,
    get_covered_terms,
    get_dataclass_field_literal_values,
)


NID_1 = "20201113T175543290880R684769"
NID_2 = "20201113T175554625048R173053"

TERM_TESTCASES = [
    (Term(), ["none"], "NONE"),
    (All(), ["all"], "ALL"),
    (Nid([]), ["nid", []], 'NID=""'),
    (Nid([NID_1, NID_2]), ["nid", [NID_1, NID_2]], f'NID="{NID_1},{NID_2}"'),
    (Text("foo bar"), ["text", "foo bar"], '"foo bar"'),
    (Text('foo"bar'), ["text", 'foo"bar'], '"foo\\"bar"'),
    (Tag("a", mode="exact"), ["tag", "a", "exact"], "a"),
    (Tag("b", mode="prefix"), ["tag", "b", "prefix"], "b*"),
    (Tag("c", mode="suffix"), ["tag", "c", "suffix"], "*c"),
    (Tag("d", mode="infix"), ["tag", "d", "infix"], "*d*"),
    (
        Attr("href", "*=", "oleeka.com"),
        ["attr", "href", "*=", "oleeka.com"],
        "href*=oleeka.com",
    ),
    (
        Attr("title", "=", "with-dash"),
        ["attr", "title", "=", "with-dash"],
        "title=with-dash",
    ),
    (
        Attr("title", "=", "with space"),
        ["attr", "title", "=", "with space"],
        'title="with space"',
    ),
    (
        Attr("title", "=", 'with "quote"'),
        ["attr", "title", "=", 'with "quote"'],
        'title="with \\"quote\\""',
    ),
    (Not(Tag("foo")), ["not", ["tag", "foo", "exact"]], "!foo"),
    (
        And([Text("foo"), Text("bar")]),
        ["and", [["text", "foo"], ["text", "bar"]]],
        '"foo" AND "bar"',
    ),
    (
        Or([Text("foo"), Text("bar")]),
        ["or", [["text", "foo"], ["text", "bar"]]],
        '"foo" OR "bar"',
    ),
    (
        And([All(), Or([Text("foo"), Text("bar")])]),
        ["and", [["all"], ["or", [["text", "foo"], ["text", "bar"]]]]],
        'ALL AND ("foo" OR "bar")',
    ),
]


@pytest.mark.parametrize(("term", "struct", "query"), TERM_TESTCASES)
def test_term_to_struct(term: Term, struct, query):
    assert term.to_struct() == struct


@pytest.mark.parametrize(("term", "struct", "query"), TERM_TESTCASES)
def test_term_from_struct(term: Term, struct, query):
    assert Term.from_struct(struct) == term


@pytest.mark.parametrize(("term", "struct", "query"), TERM_TESTCASES)
def test_term_format(term: Term, struct, query):
    assert term.format() == query


@pytest.mark.parametrize(
    ("message", "klass", "expected"),
    [
        pytest.param(
            "__init__() takes from 2 to 3 positional arguments but 4 were given",
            Tag,
            "tag takes from 2 to 3 positional arguments but 4 were given",
            id="pre-python-3.10",
        ),
        pytest.param(
            "Tag.__init__() takes from 2 to 3 positional arguments but 4 were given",
            Tag,
            "tag takes from 2 to 3 positional arguments but 4 were given",
            id="python-3.10",
        ),
    ],
)
def test_format_exception_message(message, klass, expected):
    assert _format_exception_message(message, klass) == expected


@pytest.mark.parametrize(
    ("struct", "message", "info"),
    [
        ({}, "struct query must be a list", None),
        ([], "struct query must be an nonempty list", None),
        (["foo"], "struct query 'foo' is not a valid term", "foo"),
        (["all", "extra"], "all takes 1 positional argument but 2 were given", None),
        (["none", "extra"], "none takes 0 positional arguments but 1 were given", None),
        (["nid", [NID_1 + "foo"]], f"'{NID_1}foo' is not a valid nid", NID_1 + "foo"),
        (["tag"], "tag missing 1 required positional argument: 'tag'", None),
        (["tag", 123], "struct query parameters 123 is not a string", 123),
        (
            ["tag", "123", "exact", "extra"],
            "tag takes from 2 to 3 positional arguments but 4 were given",
            None,
        ),
        (["tag", "123", "like"], "tag mode 'like' is not a valid one", "like"),
        (
            ["attr", "key", "=*=", "value"],
            "attr operator '=*=' is not a valid one",
            "=*=",
        ),
        (["not"], "not takes 1 positional arguments but 0 were given", None),
        (["and"], "and takes 1 positional arguments but 0 were given", None),
        (["or"], "or takes 1 positional arguments but 0 were given", None),
        (
            ["not", ["text", "foo"], "extra"],
            "not takes 1 positional arguments but 2 were given",
            None,
        ),
        (
            ["and", [["text", "foo"]], "extra"],
            "and takes 1 positional arguments but 2 were given",
            None,
        ),
        (
            ["or", [["text", "foo"]], "extra"],
            "or takes 1 positional arguments but 2 were given",
            None,
        ),
    ],
)
def test_term_from_struct_exception(struct, message, info):
    with pytest.raises(StructParseException) as exc:
        Term.from_struct(struct)
    assert exc.value.args == (message, info)
    assert exc.value.message == message
    assert exc.value.info == info


def note_created(created):
    return Note(attrs={"created": created + "T12:34:56Z"})


def note_dates(created, modified):
    return Note(attrs={"created": created, "modified": modified})


TERM_MATCH_TESTCASES = [
    (Term(), Note(), False),
    (All(), Note(), True),
    # Nid
    (Nid([]), Note(), False),
    (Nid([NID_1]), Note(nid=NID_1), True),
    (Nid([NID_1]), Note(nid=NID_2), False),
    (Nid([NID_1, NID_2]), Note(nid=NID_2), True),
    # Tag
    (Tag("a", mode="exact"), Note(), False),
    (Tag("a", mode="exact"), Note(tags=["foo"]), False),
    (Tag("a", mode="exact"), Note(tags=["foo", "a"]), True),
    (Tag("b", mode="prefix"), Note(tags=["foo"]), False),
    (Tag("b", mode="prefix"), Note(tags=["fob"]), False),
    (Tag("b", mode="prefix"), Note(tags=["bar"]), True),
    (Tag("c", mode="suffix"), Note(tags=["foo"]), False),
    (Tag("c", mode="suffix"), Note(tags=["coo"]), False),
    (Tag("c", mode="suffix"), Note(tags=["foc"]), True),
    (Tag("d", mode="infix"), Note(tags=["abc"]), False),
    (Tag("d", mode="infix"), Note(tags=["bcd"]), True),
    (Tag("d", mode="infix"), Note(tags=["cde"]), True),
    (Tag("d", mode="infix"), Note(tags=["def"]), True),
    (Tag("d", mode="infix"), Note(tags=["efg"]), False),
    # Attr
    (Attr("k", "?", ""), Note(attrs={}), False),
    (Attr("k", "?", ""), Note(attrs={"k": "abc"}), True),
    (Attr.exists("k"), Note(attrs={}), False),
    (Attr.exists("k"), Note(attrs={"k": "abc"}), True),
    (Attr("k", "=", "contain"), Note(attrs={}), False),
    (Attr("k", "=", "contain"), Note(attrs={"k": "foo"}), False),
    (Attr("k", "=", "contain"), Note(attrs={"k": "contain"}), True),
    (Attr("k", "=", "contain"), Note(attrs={"k": "CoNtAiN"}), True),
    (Attr("k", "=", "contain"), Note(attrs={"k": "TextContained"}), True),
    (Attr("k", "==", "exact"), Note(attrs={}), False),
    (Attr("k", "==", "exact"), Note(attrs={"k": "inexact"}), False),
    (Attr("k", "==", "exact"), Note(attrs={"k": "Exact"}), True),
    (Attr("k", "==", "exact"), Note(attrs={"k": "exact"}), True),
    (Attr("k", "===", "exact"), Note(attrs={"k": "inexact"}), False),
    (Attr("k", "===", "exact"), Note(attrs={"k": "Exact"}), False),
    (Attr("k", "===", "exact"), Note(attrs={"k": "exact"}), True),
    (Attr("k", "^=", "pre"), Note(attrs={}), False),
    (Attr("k", "^=", "pre"), Note(attrs={"k": "express"}), False),
    (Attr("k", "^=", "pre"), Note(attrs={"k": "prefix"}), True),
    (Attr("k", "^=", "pre"), Note(attrs={"k": "PREfix"}), True),
    (Attr("k", "^==", "pre"), Note(attrs={"k": "prefix"}), True),
    (Attr("k", "^==", "pre"), Note(attrs={"k": "PREfix"}), False),
    (Attr("k", "$=", "post"), Note(attrs={}), False),
    (Attr("k", "$=", "post"), Note(attrs={"k": "postfix"}), False),
    (Attr("k", "$=", "post"), Note(attrs={"k": "outpost"}), True),
    (Attr("k", "$=", "post"), Note(attrs={"k": "outPOST"}), True),
    (Attr("k", "$==", "post"), Note(attrs={"k": "outpost"}), True),
    (Attr("k", "$==", "post"), Note(attrs={"k": "outPOST"}), False),
    (Attr("k", "*=", "in"), Note(attrs={}), False),
    (Attr("k", "*=", "in"), Note(attrs={"k": "foo"}), False),
    (Attr("k", "*=", "in"), Note(attrs={"k": "infix"}), True),
    (Attr("k", "*=", "in"), Note(attrs={"k": "wine"}), True),
    (Attr("k", "*=", "in"), Note(attrs={"k": "rubin"}), True),
    (Attr("k", "*=", "in"), Note(attrs={"k": "wINe"}), True),
    (Attr("k", "*==", "in"), Note(attrs={"k": "wine"}), True),
    (Attr("k", "*==", "in"), Note(attrs={"k": "wINe"}), False),
    (Attr("l", "+=", "web"), Note(attrs={"l": "web"}), True),
    (Attr("l", "+=", "web"), Note(attrs={"l": "http://web-site.com"}), True),
    (Attr("l", "+=", "web"), Note(attrs={"l": "http://WEB-site.com"}), True),
    (Attr("l", "+=", "web"), Note(attrs={"l": "http://WEB.site.com"}), True),
    (Attr("l", "+=", "web"), Note(attrs={"l": "http://WEBsite.com"}), False),
    (Attr("l", "+=", "web"), Note(attrs={"l": "http://cobWEB.com"}), False),
    (Attr("l", "+=", "a.c"), Note(attrs={"l": "a.c"}), True),
    (Attr("l", "+=", "a.c"), Note(attrs={"l": "abc"}), False),  # regex escape
    (Attr("l", "+==", "web"), Note(attrs={"l": "http://web.site.com"}), True),
    (Attr("l", "+==", "web"), Note(attrs={"l": "http://WEB.com"}), False),
    (Attr("l", "+==", "web"), Note(attrs={"l": "http://cobweb.com"}), False),
    (Attr("l", "+==", "a.c"), Note(attrs={"l": "a.c"}), True),
    (Attr("l", "+==", "a.c"), Note(attrs={"l": "A.C"}), False),
    (Attr("l", "+==", "a.c"), Note(attrs={"l": "abc"}), False),  # regex escape
    (Attr("k", "<=", "foo"), Note(attrs={"k": "bar"}), True),
    (Attr("k", "<=", "foo"), Note(attrs={"k": "qux"}), False),
    (Attr("k", "<==", "foo"), Note(attrs={"k": "bar"}), True),
    (Attr("k", "<==", "foo"), Note(attrs={"k": "qux"}), False),
    (Attr("k", ">=", "foo"), Note(attrs={"k": "bar"}), False),
    (Attr("k", ">=", "foo"), Note(attrs={"k": "qux"}), True),
    (Attr("k", ">==", "foo"), Note(attrs={"k": "bar"}), False),
    (Attr("k", ">==", "foo"), Note(attrs={"k": "qux"}), True),
    # Attr numeric comparisons (<<=, >>=)
    (Attr("k", "<<=", "abc"), Note(attrs={}), False),
    (Attr("k", "<<=", "abc"), Note(attrs={"k": "xyz"}), False),
    (Attr("k", "<<=", "144"), Note(attrs={"k": "xyz"}), False),
    (Attr("k", "<<=", "abc"), Note(attrs={"k": "144"}), False),
    (Attr("k", "<<=", "144"), Note(attrs={"k": "144"}), True),
    (Attr("k", "<<=", "15"), Note(attrs={"k": "144"}), False),
    (Attr("k", "<<=", "1200"), Note(attrs={"k": "144"}), True),
    (Attr("k", ">>=", "abc"), Note(attrs={}), False),
    (Attr("k", ">>=", "abc"), Note(attrs={"k": "xyz"}), False),
    (Attr("k", ">>=", "144"), Note(attrs={"k": "xyz"}), False),
    (Attr("k", ">>=", "abc"), Note(attrs={"k": "144"}), False),
    (Attr("k", ">>=", "144"), Note(attrs={"k": "144"}), True),
    (Attr("k", ">>=", "15"), Note(attrs={"k": "144"}), True),
    (Attr("k", ">>=", "1200"), Note(attrs={"k": "144"}), False),
    # Mark tags_count
    (Mark("tags_count", "==", 1), Note(tags=[]), False),
    (Mark("tags_count", "==", 1), Note(tags=["a"]), True),
    (Mark("tags_count", "==", 1), Note(tags=["a", "b"]), False),
    (Mark("tags_count", "!=", 1), Note(tags=[]), True),
    (Mark("tags_count", "!=", 1), Note(tags=["a"]), False),
    (Mark("tags_count", "!=", 1), Note(tags=["a", "b"]), True),
    (Mark("tags_count", "<", 1), Note(tags=[]), True),
    (Mark("tags_count", "<", 1), Note(tags=["a"]), False),
    (Mark("tags_count", "<", 1), Note(tags=["a", "b"]), False),
    (Mark("tags_count", "<=", 1), Note(tags=[]), True),
    (Mark("tags_count", "<=", 1), Note(tags=["a"]), True),
    (Mark("tags_count", "<=", 1), Note(tags=["a", "b"]), False),
    (Mark("tags_count", ">", 1), Note(tags=[]), False),
    (Mark("tags_count", ">", 1), Note(tags=["a"]), False),
    (Mark("tags_count", ">", 1), Note(tags=["a", "b"]), True),
    (Mark("tags_count", ">=", 1), Note(tags=[]), False),
    (Mark("tags_count", ">=", 1), Note(tags=["a"]), True),
    (Mark("tags_count", ">=", 1), Note(tags=["a", "b"]), True),
    # Mark attrs_count
    (Mark("attrs_count", "==", 0), Note(attrs={}), True),
    (Mark("attrs_count", "==", 0), Note(attrs={"a": "b"}), False),
    (Mark("attrs_count", "==", 1), Note(attrs={"a": "b"}), True),
    # Mark files_count
    (Mark("files_count", "==", 0), Note(files=[]), True),
    (Mark("files_count", "==", 0), Note(files=[FILE_A]), False),
    (Mark("files_count", "==", 1), Note(files=[FILE_A]), True),
    # Mark files_size
    (Mark("files_size", "==", 0), Note(files=[]), True),
    (Mark("files_size", "==", 0), Note(files=[FILE_A]), False),
    (Mark("files_size", "==", 19), Note(files=[FILE_A]), True),
    # Mark modified_after
    (
        Mark("modified_after", "==", 0),
        note_dates("2022-06-06T12:34:56Z", "2022-06-06T12:34:56Z"),
        True,
    ),
    (
        Mark("modified_after", "==", 3600),
        note_dates("2022-06-06T12:34:56Z", "2022-06-06T13:34:56Z"),
        True,
    ),
    # Mark date_day_of_week
    (Mark("date_day_of_week", "==", 1), note_created("2020-01-06"), True),
    (Mark("date_day_of_week", "==", 7), note_created("2020-01-12"), True),
    (Mark("date_day_of_week", "==", 1), note_created("2020-01-13"), True),
    # Mark date_day_of_year
    (Mark("date_day_of_year", "==", 1), note_created("2020-01-01"), True),
    (Mark("date_day_of_year", "==", 31), note_created("2020-01-31"), True),
    (Mark("date_day_of_year", "==", 366), note_created("2020-12-31"), True),
    # Mark date_week_of_year
    (Mark("date_week_of_year", "==", 1), note_created("2020-01-01"), True),
    (Mark("date_week_of_year", "==", 53), note_created("2021-01-01"), True),
    (Mark("date_week_of_year", "==", 52), note_created("2022-01-01"), True),
    (Mark("date_week_of_year", "==", 22), note_created("2022-06-05"), True),
    (Mark("date_week_of_year", "==", 23), note_created("2022-06-06"), True),
    # Text
    (Text("foo"), Note(), False),
    (Text("foo"), Note(text="bar baz"), False),
    (Text("foo"), Note(text="bar foo baz"), True),
    # Not
    (Not(Tag("a")), Note(), True),
    (Not(Tag("a")), Note(tags=["a"]), False),
    # And
    (And([]), Note(), True),
    (And([]), Note(tags=["a"]), True),
    (And([Tag("a")]), Note(), False),
    (And([Tag("a")]), Note(tags=["a"]), True),
    (And([Tag("a"), Tag("b")]), Note(), False),
    (And([Tag("a"), Tag("b")]), Note(tags=["a"]), False),
    (And([Tag("a"), Tag("b")]), Note(tags=["a", "b"]), True),
    (And([Tag("a"), Tag("b")]), Note(tags=["b"]), False),
    # Or
    (Or([]), Note(), False),
    (Or([]), Note(tags=["a"]), False),
    (Or([Tag("a")]), Note(), False),
    (Or([Tag("a")]), Note(tags=["a"]), True),
    (Or([Tag("a"), Tag("b")]), Note(), False),
    (Or([Tag("a"), Tag("b")]), Note(tags=["a"]), True),
    (Or([Tag("a"), Tag("b")]), Note(tags=["a", "b"]), True),
    (Or([Tag("a"), Tag("b")]), Note(tags=["b"]), True),
    (Or([Tag("a"), Tag("b")]), Note(tags=["c"]), False),
]


@pytest.mark.parametrize(("term", "note", "is_match"), TERM_MATCH_TESTCASES)
def test_term_match(term: Term, note, is_match):
    assert term.match(note) == is_match


def test_attr_operators_literal_match():
    assert set(Attr.OPERATORS) == get_dataclass_field_literal_values(Attr, "operator")


def test_tag_modes_literal_match():
    assert set(Tag.MODES) == get_dataclass_field_literal_values(Tag, "mode")


def test_term_testcases_cover_all_terms():
    assert get_covered_terms(TERM_MATCH_TESTCASES, 0) == get_all_db_terms()


@pytest.mark.parametrize(
    ("term", "expected"),
    [
        (Term(), Term()),
        (Nid([]), Term()),
        (Nid(["20201113T175543290880R684769"]), Nid(["20201113T175543290880R684769"])),
        (Text("foo"), Text("foo")),
        # Not
        (Tag("a"), Tag("a")),
        (Not(Tag("a")), Not(Tag("a"))),
        (Not(Not(Tag("a"))), Tag("a")),
        (Not(Not(Not(Tag("a")))), Not(Tag("a"))),
        (Not(Not(Not(Not(Tag("a"))))), Tag("a")),
        # And
        (And([]), All()),
        (And([Tag("a")]), Tag("a")),
        (And([Tag("a"), Tag("b")]), And([Tag("a"), Tag("b")])),
        # Or
        (Or([]), Term()),
        (Or([Tag("a")]), Tag("a")),
        (Or([Tag("a"), Tag("b")]), Or([Tag("a"), Tag("b")])),
        # mixing
        (And([Or([Tag("a")])]), Tag("a")),
        (Or([And([Tag("a")])]), Tag("a")),
        (And([Not(Not(Tag("a")))]), Tag("a")),
        (Or([Not(Not(Tag("a")))]), Tag("a")),
    ],
)
def test_term_simplify(term: Term, expected: Term):
    assert term.simplify() == expected
