import pytest  # type: ignore

from oleeka_api.query.parser import parse_string, QueryParseException
from oleeka_api.query.terms import Term, All, Nid, Text, Tag, Attr, Not, And, Or

from tests.helpers import get_all_parsable_terms, get_covered_terms


NID_1 = "20201113T175543290880R684769"
NID_2 = "20201113T175554625048R173053"

PARSE_STRING_TESTCASES = [
    ("", All()),
    (" NONE ", Term()),
    (" ALL ", All()),
    (" ALLegory ", Tag("ALLegory")),
    (" none ", Tag("none")),
    (" All ", Tag("All")),
    (f"NID={NID_1}", Nid([NID_1])),
    (f"NID='{NID_1},{NID_2}'", Nid([NID_1, NID_2])),
    # Text
    (" 'foo bar' ", Text("foo bar")),
    (' "foo bar" ', Text("foo bar")),
    (" 'foo bar \\'quote\\'' ", Text("foo bar 'quote'")),
    (" 'foo bar \"quote\"' ", Text('foo bar "quote"')),
    (" \"foo bar 'quote'\" ", Text("foo bar 'quote'")),
    (' "foo bar \\"quote\\"" ', Text('foo bar "quote"')),
    # Tag
    (" abc ", Tag("abc")),
    (" abc* ", Tag("abc", mode="prefix")),
    (" *abc ", Tag("abc", mode="suffix")),
    (" *abc* ", Tag("abc", mode="infix")),
    # Attr
    (" abc = foo ", Attr("abc", "=", "foo")),
    ("abc=foo", Attr("abc", "=", "foo")),
    (" abc = 'foo bar' ", Attr("abc", "=", "foo bar")),
    ("abc='foo bar'", Attr("abc", "=", "foo bar")),
    ("abc='let\\'s go'", Attr("abc", "=", "let's go")),
    # Attr with `$` prefix
    (" $abc = foo ", Attr("abc", "=", "foo")),
    ("$abc=foo", Attr("abc", "=", "foo")),
    (" $abc = 'foo bar' ", Attr("abc", "=", "foo bar")),
    ("$abc='foo bar'", Attr("abc", "=", "foo bar")),
    ("$abc='let\\'s go'", Attr("abc", "=", "let's go")),
    (" $abc ? ", Attr("abc", "?", "")),
    # Attr operators
    ("abc?", Attr("abc", "?", "")),
    (" abc ? ", Attr("abc", "?", "")),
    ("$abc", Attr("abc", "?", "")),
    ("abc=foo", Attr("abc", "=", "foo")),
    ("abc==foo", Attr("abc", "==", "foo")),
    ("abc===foo", Attr("abc", "===", "foo")),
    ("abc^=foo", Attr("abc", "^=", "foo")),
    ("abc^==foo", Attr("abc", "^==", "foo")),
    ("abc$=foo", Attr("abc", "$=", "foo")),
    ("abc$==foo", Attr("abc", "$==", "foo")),
    ("abc*=foo", Attr("abc", "*=", "foo")),
    ("abc*==foo", Attr("abc", "*==", "foo")),
    ("abc+=foo", Attr("abc", "+=", "foo")),
    ("abc+==foo", Attr("abc", "+==", "foo")),
    ("abc<=foo", Attr("abc", "<=", "foo")),
    ("abc<==foo", Attr("abc", "<==", "foo")),
    ("abc>=foo", Attr("abc", ">=", "foo")),
    ("abc>==foo", Attr("abc", ">==", "foo")),
    ("abc<<=144", Attr("abc", "<<=", "144")),
    ("abc>>=2.7", Attr("abc", ">>=", "2.7")),
    # Not
    ("NOT xyz", Not(Tag("xyz"))),
    ("NOT(xyz)", Not(Tag("xyz"))),
    ("!xyz", Not(Tag("xyz"))),
    ("! xyz", Not(Tag("xyz"))),
    # And
    ("abc def ghi", And([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc AND def AND ghi", And([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc & def & ghi", And([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc && def && ghi", And([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc&def&ghi", And([Tag("abc"), Tag("def"), Tag("ghi")])),
    # Or
    ("abc OR def OR ghi", Or([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc | def | ghi", Or([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc || def || ghi", Or([Tag("abc"), Tag("def"), Tag("ghi")])),
    ("abc|def|ghi", Or([Tag("abc"), Tag("def"), Tag("ghi")])),
    # Operator precedence
    ("abc OR def ghi", Or([Tag("abc"), And([Tag("def"), Tag("ghi")])])),
    ("(abc OR def) ghi", And([Or([Tag("abc"), Tag("def")]), Tag("ghi")])),
    # Unicode tests
    ("żółć нота 한글", And([Tag("żółć"), Tag("нота"), Tag("한글")])),
    ("żółć=нота 한글", And([Attr("żółć", "=", "нота"), Tag("한글")])),
    ("$żółć=нота 한글", And([Attr("żółć", "=", "нота"), Tag("한글")])),
]


@pytest.mark.parametrize(("query", "term"), PARSE_STRING_TESTCASES)
def test_parse_string(query, term):
    assert parse_string(query) == term


def test_parser_testcases_cover_all_terms():
    assert get_covered_terms(PARSE_STRING_TESTCASES, 1) == get_all_parsable_terms()


@pytest.mark.parametrize(
    ("query", "char_index"),
    [
        ("abc=", 3),
        ("abc = ", 4),
        (" abc = ", 5),
        ("@abc ", 0),
        ("$abc=", 4),
        ("$abc = ", 5),
        ("$ abc = foo", 1),
        ("$ abc ", 1),
        ("abc AND", 4),
        (f"NID={NID_1},{NID_2}", 32),
        (f"NID*='{NID_1},{NID_2}'", None),
    ],
)
def test_parse_string_raises(query, char_index):
    with pytest.raises(QueryParseException) as exc:
        parse_string(query)
    assert exc.value.args == ("query cannot be parsed", char_index)
    assert exc.value.message == "query cannot be parsed"
    assert exc.value.info == char_index
