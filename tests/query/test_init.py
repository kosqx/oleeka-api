import pytest  # type: ignore

from oleeka_api.models import PaginatedQuery
from oleeka_api.query import parse_query, postprocess_query
from oleeka_api.query.base import QueryParseException
from oleeka_api.query.terms import All, Text, Tag, Attr, Not, And, Or, Nested
from oleeka_api.query.terms import StructParseException


@pytest.mark.parametrize(
    ("query", "term"),
    [
        (None, All()),
        ('["all"]', All()),
        ('["text", "foo"]', Text("foo")),
        ("foo", Tag("foo")),
        ("foo OR bar", Or([Tag("foo"), Tag("bar")])),
        # Nested
        ('["nested", "foo bar"]', Nested("foo bar")),
        (
            '["and", [["attr", "kind", "===", "photo"], ["nested", "user input"]]]',
            And([Attr("kind", "===", "photo"), Nested("user input")]),
        ),
    ],
)
def test_parse_query(query, term):
    assert parse_query(query) == term


@pytest.mark.parametrize(
    ("input_string", "message", "info"),
    [
        ("!", "query cannot be parsed", 1),
        ("foo OR", "query cannot be parsed", 4),
        ("[", "input is not valid JSON", None),
        ('["tag"]', "tag missing 1 required positional argument: 'tag'", None),
    ],
)
def test_parse_query_raises(input_string, message, info):
    with pytest.raises((QueryParseException, StructParseException)) as exc:
        parse_query(input_string)
    assert exc.value.args == (message, info)
    assert exc.value.message == message
    assert exc.value.info == info


@pytest.mark.parametrize(
    ("query", "expected"),
    [
        pytest.param(PaginatedQuery(), PaginatedQuery(), id="empty"),
        pytest.param(
            PaginatedQuery(query_terms=Tag(":RANDOM")),
            PaginatedQuery(query_terms=All(), random=True),
            id="just :RANDOM",
        ),
        pytest.param(
            PaginatedQuery(query_terms=Not(Tag("foo"))),
            PaginatedQuery(query_terms=Not(Tag("foo"))),
            id="Not",
        ),
        pytest.param(
            PaginatedQuery(query_terms=Not(Tag(":RANDOM"))),
            PaginatedQuery(query_terms=All(), random=True),
            id="Not(:RANDOM)",
        ),
        pytest.param(
            PaginatedQuery(query_terms=And([Tag("foo"), Tag("bar")]), random=True),
            PaginatedQuery(query_terms=And([Tag("foo"), Tag("bar")]), random=True),
            id="preserves PaginatedQuery.random",
        ),
        pytest.param(
            PaginatedQuery(query_terms=And([Tag("foo"), Tag("bar"), Tag(":RANDOM")])),
            PaginatedQuery(query_terms=And([Tag("foo"), Tag("bar")]), random=True),
            id="And(:RANDOM)",
        ),
        pytest.param(
            PaginatedQuery(query_terms=Or([Tag("foo"), Tag("bar"), Tag(":RANDOM")])),
            PaginatedQuery(query_terms=Or([Tag("foo"), Tag("bar")]), random=True),
            id="Or(:RANDOM)",
        ),
        pytest.param(
            PaginatedQuery(
                query_terms=Or([Tag("foo"), And([Tag("bar"), Tag(":RANDOM")])])
            ),
            PaginatedQuery(
                query_terms=Or([Tag("foo"), And([Tag("bar")])]), random=True
            ),
            id="finds :RANDOM even deep in the tree",
        ),
        pytest.param(
            PaginatedQuery(query_terms=Nested("foo bar")),
            PaginatedQuery(query_terms=And([Tag("foo"), Tag("bar")])),
            id="parses Nested",
        ),
        pytest.param(
            PaginatedQuery(query_terms=And([Tag("baz"), Nested("foo :RANDOM")])),
            PaginatedQuery(
                query_terms=And([Tag("baz"), And([Tag("foo")])]), random=True
            ),
            id="parses Nested and finds :RANDOM",
        ),
    ],
)
def test_postprocess_query(query, expected):
    assert postprocess_query(query) == expected
